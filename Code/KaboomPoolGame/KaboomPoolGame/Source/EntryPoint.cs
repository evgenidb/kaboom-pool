﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.Factories;
using KaboomPoolGame.Source.Controllers.Factories.GameObjectFactories;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Controls;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Enums;
using KaboomPoolGame.Source.Models.Controls.InputCommands.KeyboardCommands;
using KaboomPoolGame.Source.Models.Controls.InputCommands.MouseCommands;
using KaboomPoolGame.Source.Models.Controls.InputCommands.SpecialInputCommands;
using KaboomPoolGame.Source.Models.Options;
using KaboomPoolGame.Source.Models.Repositories;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.FileReaderWriters;
using KaboomPoolGame.Source.Utils.Loaders;
using KaboomPoolGame.Source.Utils.Loaders.LevelLoaders;
using KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.Parsers;
using KaboomPoolGame.Source.Utils.MathUtils;
using KaboomPoolGame.Source.Utils.PhysicsUtils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace KaboomPoolGame.Source
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class EntryPoint
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            InitializeOptions(args);

            using (var game = new KaboomPool())
            {
                InitializeServices(game);
                InitializeControls();
                game.Run();
            }
        }


        #region Initializers
        private static void InitializeOptions(string[] args)
        {
            // Game Options
            var options = new GameOptions();
            // TODO: Put any non-default game options here
            GameServices.GameOptions = options;


            // Menu Options
            var menuOptions = new MenuOptions();
            // TODO: Put any non-default menu options here
            GameServices.MenuOptions = menuOptions;


            // Menu Options
            var playOptions = new PlayOptions();
            // TODO: Put any non-default play options here
            playOptions.CellSize = new Vector2(20, 20);
            GameServices.PlayOptions = playOptions;


            // Debug Options
            var debugOptions = new DebugOptions();
            // TODO: Put any non-default debug options here
            debugOptions.IsDebugMode = false;
            GameServices.DebugOptions = debugOptions;
        }

        private static void InitializeServices(KaboomPool game)
        {
            GameServices.Initialize(game);

            GameServices.Random = new Random();

            GameServices.GameStateManager = new GameStateManager();
            GameServices.SpritesManager = new SpriteManager();

            GameServices.Input = new InputManager();

            GameServices.Utils = new GameUtil();
            GameServices.Physics = new PhysicsUtil();
            GameServices.Math = new MathUtil();
            GameServices.Geometry = new GeometryUtil();

            GameServices.UIFactory = new UIFactory();
            GameServices.EntityFactories =
                new EntityFactoriesService()
                {
                    BombFactory = new BombFactory(),
                    ObjectFactory = new ObjectFactory(),
                    PlayerFactory = new PlayerFactory(),
                    TargetFactory = new TargetFactory(),
                    TerrainFactory = new TerrainFactory(),
                    WallFactory = new WallFactory(),
                    EffectsFactory = new EffectsFactory(),
                };

            GameServices.ContentLoader = new ContentLoader();

            var dataLoaders = GetDataLoaders();
            GameServices.DataLoader =
                new DataLoader(dataLoaders);

            //GameServices.LevelLoader = new TestLevelLoader();
            GameServices.LevelLoader = new JsonFileLevelLoader(
                levelsPath: "Levels",
                metaFileName: "Levels Meta",
                parser: new JsonLevelParser());

            GameServices.LevelRepository = new LevelRepository(
                areLevelsLazyLoaded: true);

            GameServices.SpritesheetRepository =
                new SpritesheetRepository();
        }

        private static void InitializeControls()
        {
            // Game Controls
            var gameControls = new GameControls();

            GameServices.GameOptions.Controls =
                gameControls;


            // Menu Controls
            var menuControls = new MenuControls();

            menuControls.NextMenuItem =
                new AlternativeInputCommands(
                    new BasicKeyCommand(Keys.Down),
                    new BasicKeyCommand(Keys.S));

            menuControls.PreviousMenuItem =
                new AlternativeInputCommands(
                    new BasicKeyCommand(Keys.Up),
                    new BasicKeyCommand(Keys.W));

            menuControls.ClickMenuItem =
                new AlternativeInputCommands(
                    new BasicKeyCommand(Keys.Enter),
                    new BasicKeyCommand(Keys.Space));

            menuControls.ResumeGame =
                new BasicKeyCommand(Keys.Escape);

            GameServices.MenuOptions.Controls =
                menuControls;


            // Play Controls
            var playControls = new PlayControls();

            playControls.MoveUp =
                new AlternativeInputCommands(
                    new HoldKeyCommand(Keys.Up),
                    new HoldKeyCommand(Keys.W));

            playControls.MoveDown =
                new AlternativeInputCommands(
                    new HoldKeyCommand(Keys.Down),
                    new HoldKeyCommand(Keys.S));

            playControls.MoveLeft =
                new AlternativeInputCommands(
                    new HoldKeyCommand(Keys.Left),
                    new HoldKeyCommand(Keys.A));

            playControls.MoveRight =
                new AlternativeInputCommands(
                    new HoldKeyCommand(Keys.Right),
                    new HoldKeyCommand(Keys.D));

            playControls.PlaceBomb =
                new BasicKeyCommand(key: Keys.Space);

            playControls.PauseMenu =
                new BasicKeyCommand(Keys.Escape);

            GameServices.PlayOptions.Controls =
                playControls;


            // Debug Controls
            var debugControls = new DebugControls();
            debugControls.ActivateDebugMode =
                new AlternativeInputCommands(
                    new ComboKeysCommand(
                        keys: new[] { Keys.LeftControl, Keys.F2 },
                        isStrict: true,
                        singleCheck: true),
                    new ComboKeysCommand(
                        keys: new[] { Keys.RightControl, Keys.F2 },
                        isStrict: true,
                        singleCheck: true));

            debugControls.DeactivateDebugMode =
                new AlternativeInputCommands(
                    new ComboKeysCommand(
                        keys: new[] { Keys.LeftControl, Keys.F3 },
                        isStrict: true,
                        singleCheck: true),
                    new ComboKeysCommand(
                        keys: new[] { Keys.RightControl, Keys.F3 },
                        isStrict: true,
                        singleCheck: true));

            debugControls.WinLevel =
                new AlternativeInputCommands(
                    new ComboKeysCommand(
                        keys: new[] { Keys.LeftControl, Keys.W },
                        isStrict: true,
                        singleCheck: true),
                    new ComboKeysCommand(
                        keys: new[] { Keys.RightControl, Keys.W },
                        isStrict: true,
                        singleCheck: true));

            debugControls.LoseLevel =
                new AlternativeInputCommands(
                    new ComboKeysCommand(
                        keys: new[] { Keys.LeftControl, Keys.L },
                        isStrict: true,
                        singleCheck: true),
                    new ComboKeysCommand(
                        keys: new[] { Keys.RightControl, Keys.L },
                        isStrict: true,
                        singleCheck: true));

            debugControls.ReloadLevelData =
                new AlternativeInputCommands(
                    new ComboKeysCommand(
                        keys: new[] { Keys.LeftControl, Keys.R },
                        isStrict: true,
                        singleCheck: true),
                    new ComboKeysCommand(
                        keys: new[] { Keys.RightControl, Keys.R },
                        isStrict: true,
                        singleCheck: true));

            debugControls.ReloadAllLevelsData =
                new AlternativeInputCommands(
                    new ComboKeysCommand(
                        keys: new[] { Keys.LeftControl, Keys.R, Keys.A },
                        isStrict: true,
                        singleCheck: true),
                    new ComboKeysCommand(
                        keys: new[] { Keys.RightControl, Keys.R, Keys.A },
                        isStrict: true,
                        singleCheck: true));

            GameServices.DebugOptions.Controls =
                debugControls;
        }
        #endregion


        #region Additional Initializers
        private static IDictionary<string, dynamic> GetDataLoaders()
        {
            var readers = new Dictionary<string, dynamic>
            {
                ["json"] = new JsonFileReader(),
                ["spritesheet"] = new JsonFileSpritesheetReader(),
            };

            return readers;
        }
        #endregion
    }
#endif
}
