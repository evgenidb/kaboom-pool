﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Utils.MathUtils.Shapes;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.GameObjects
{
    public abstract class GameObject
    {
        #region Properties
        public bool MustRemove { get; set; } = false;
        public string Name { get; }
        public Vector2 Position { get; set; }
        public Vector2 Size { get; }
        public double Rotation { get; set; }
        public Vector2 Scale { get; set; }

        public Vector2 Origin { get; }

        public IShape CollisionShape { get; set; }

        private HashSet<string> AllowedPublicProperties { get; } =
            new HashSet<string>();
        #endregion

        #region Constructors
        public GameObject(string name, Vector2 position, Vector2 size, double rotation = 0, Vector2? scale = null)
        {
            Name = name;
            Position = position;
            Size = size;
            Rotation = rotation;
            
            Scale = scale ?? Vector2.One;

            Origin = Size / 2;

            AddAllowedPublicProperties(new[]{
                nameof(MustRemove),
                nameof(Position),
                nameof(Rotation),
                nameof(Scale),
            });
        }
        #endregion

        #region Public Methods
        public bool CanGetProperty(string name)
        {
            return AllowedPublicProperties.Contains(name);
        }

        public T GetProperty<T>(string name)
        {
            if (CanGetProperty(name))
            {
                Type thisType = this.GetType();

                var property = thisType.GetProperty(name);

                var value = property.GetValue(this);

                return (T)value;
            }
            else
            {
                throw new InvalidOperationException(
                    message: $"Cannot get property {name}");
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj is GameObject)
            {
                var gameObject = obj as GameObject;
                return Name == gameObject.Name &&
                    Position == gameObject.Position;
            }
            else
            {
                var exceptionMessage = string.Join(" ",
                    "Cannot compare",
                    $"{ nameof(GameObject)} object",
                    $"with non-{nameof(GameObject)} object.");

                throw new InvalidOperationException(
                    exceptionMessage);
            }
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return $"{Name}: {Position.ToString()}";
        }

        public static bool operator ==(GameObject objA, GameObject objB)
        {
            return objA.Equals(objB);
        }

        public static bool operator !=(GameObject objA, GameObject objB)
        {
            return !(objA.Equals(objB));
        }
        #endregion

        #region Protected Methods
        protected void AddAllowedPublicProperties(IEnumerable<string> names)
        {
            AllowedPublicProperties.UnionWith(names);
        }
        #endregion
    }
}
