﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.Sprites;

namespace KaboomPoolGame.Source.Models.GameObjects
{
    public interface IRenderableFlyweightModel
    {
        Layer Layer { get; }
        IDictionary<string, ISpriteData> Sprites { get; }

        IEnumerable<string> GetSpriteNames();
    }
}
