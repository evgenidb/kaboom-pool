﻿using System;
using System.Collections.Generic;
using System.Linq;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Enums;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.MouseCommands
{
    public class BasicMouseCommand : IInputCommand
    {
        #region Fields
        private RequiredCommandState requiredState;
        #endregion

        #region Properties
        private MouseButton Button { get; }
        private RequiredCommandState RequiredState
        {
            get { return requiredState; }
            set
            {
                var validStates = new[]
                {
                    RequiredCommandState.Press,
                    RequiredCommandState.Release,
                    RequiredCommandState.Up,
                };
                if (validStates.Contains(value))
                {
                    requiredState = value;
                }
                else
                {
                    var statesString = string.Join(", ", validStates);
                    var exceptionMessage = string.Join(" ",
                        "Invalid Key state.",
                        "Valid states are:",
                        $"{statesString}.");

                    throw new ArgumentOutOfRangeException(
                        message: exceptionMessage,
                        paramName: nameof(RequiredState),
                        actualValue: value);
                }
            }
        }
        #endregion

        #region Constructors
        public BasicMouseCommand(MouseButton button, RequiredCommandState requiredState = RequiredCommandState.Release)
        {
            Button = button;
            RequiredState = requiredState;
        }
        #endregion

        #region Public Methods

        public bool Check(InputManager input, CheckInputArgs args = null)
        {
            // TODO: Think on how to smartly check
            // for any Mouse Button.

            switch (RequiredState)
            {
                case RequiredCommandState.Up:
                    return input.IsLeftMouseButtonUp();
                case RequiredCommandState.Press:
                    return input.IsLeftMouseButtonPressed();
                case RequiredCommandState.Release:
                    return input.IsLeftMouseButtonReleased();
                default:
                    break;
            }

            return false;
        }

        public bool Check(InputManager input, IEnumerable<CheckInputArgs> argsCollection)
        {
            return Check(input);
        }

        public override string ToString()
        {
            return $"{RequiredState.ToString()} {Button.ToString()}";
        }
        #endregion
    }
}
