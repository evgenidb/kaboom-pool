﻿using System;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments
{
    public class CircleAreaArgs : CheckInputArgs
    {
        #region Fields
        private double radius;
        #endregion

        #region Properties
        public Vector2 Center { get; set; }
        public double Radius
        {
            get { return radius; }
            set
            {
                if (value >= 0)
                {
                    radius = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        message: "Circle's radius must be a positive number.",
                        paramName: nameof(Radius),
                        actualValue: value);
                }
            }
        }
        #endregion

        #region Constructors
        public CircleAreaArgs(Vector2 center, double radius)
        {
            Center = center;
            Radius = radius;
        }
        #endregion
    }
}
