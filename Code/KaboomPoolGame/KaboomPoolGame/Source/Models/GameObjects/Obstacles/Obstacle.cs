﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.GameObjects.Obstacles
{
    public class Obstacle : GameObject, IRenderableModel, IDestructable
    {
        #region Properties
        public Layer Layer { get; } = Layer.Obstacle;

        public ISpriteData Sprite { get; private set; }
        
        public bool IsDestructable =>
            DestructableBy.Count > 0;
        
        public HashSet<string> DestructableBy { get; } =
            new HashSet<string>();

        public double RequiredMinDestructionForce { get; set; } = 0;
        #endregion

        #region Constructors
        public Obstacle(string name, Vector2 position, Vector2 size, ISpriteData sprite, double rotation = 0, Vector2? scale = default(Vector2?))
            : base(name, position, size, rotation, scale)
        {
            Sprite = sprite;

            AddAllowedPublicProperties(new[] {
                nameof(IsDestructable),
                nameof(DestructableBy)
            });
        }
        #endregion

        #region Public Methods
        public IEnumerable<string> GetSpriteNames()
        {
            return Sprite.TextureNames;
        }
        #endregion
    }
}
