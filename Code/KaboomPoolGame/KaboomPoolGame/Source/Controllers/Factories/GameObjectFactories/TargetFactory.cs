﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Models.GameObjects.Terrain;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.Factories.GameObjectFactories
{
    public class TargetFactory
    {
        public Target MakeTarget()
        {
            var sprite = new SpriteData("Ball", "Terrain/Target");

            var model = new Target(
                position: Vector2.Zero,
                size: new Vector2(20, 20),
                sprite: sprite);

            return model;
        }
    }
}
