﻿using KaboomPoolGame.Source.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Controllers
{
    public abstract class BaseController<TModel, TView>
        where TView : IView<TModel>
    {
        #region Properties
        protected TModel Model { get; private set; }
        protected TView View { get; private set; }
        #endregion

        #region Constructors
        public BaseController(TModel model, TView view)
        {
            Model = model;
            View = view;
            View.Initialize(Model);
        }
        #endregion

        #region Public Methods
        public void Initialize()
        {
            InitializeImplementation();
        }

        public void LoadContent()
        {
            View.LoadContent();
        }

        public void UnloadContent()
        {
            View.UnloadContent();
        }

        public virtual void Update(GameTime gameTime) { }

        public void Draw(SpriteBatch spriteBatch)
        {
            View.Draw(spriteBatch);
        }
        #endregion

        #region Protected Methods
        protected virtual void InitializeImplementation() { }
        #endregion
    }
}
