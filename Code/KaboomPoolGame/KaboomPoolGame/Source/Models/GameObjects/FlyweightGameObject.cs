﻿using System;
using System.Collections.Generic;

namespace KaboomPoolGame.Source.Models.GameObjects
{
    public abstract class FlyweightGameObject
    {
        #region Properties
        public string Name { get; }

        private HashSet<string> AllowedPublicProperties { get; } =
            new HashSet<string>();
        #endregion

        #region Constructors
        public FlyweightGameObject(string name)
        {
            Name = name;
        }
        #endregion

        #region Public Methods
        public bool CanGetProperty(string name)
        {
            return AllowedPublicProperties.Contains(name);
        }

        public T GetProperty<T>(string name)
        {
            if (CanGetProperty(name))
            {
                Type thisType = this.GetType();

                var property = thisType.GetProperty(name);

                var value = property.GetValue(this);

                return (T)value;
            }
            else
            {
                throw new InvalidOperationException(
                    message: $"Cannot get property {name}");
            }
        }
        #endregion

        #region Protected Methods
        protected void AddAllowedPublicProperties(IEnumerable<string> names)
        {
            AllowedPublicProperties.UnionWith(names);
        }
        #endregion
    }
}
