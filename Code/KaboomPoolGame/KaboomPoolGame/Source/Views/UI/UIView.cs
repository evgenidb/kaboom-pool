﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.Loaders;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Views.UI
{
    public abstract class UIView<TModel> : IView<TModel>
    {
        #region Fields
        private TModel model;
        #endregion

        #region Properties
        protected TModel Model
        {
            get
            {
                return model;
            }
            set
            {
                if (value != null)
                {
                    model = value;
                }
                else
                {
                    throw new ArgumentNullException(
                        message: "The Model cannot be null",
                        paramName: nameof(Model));
                }
            }
        }
        public bool IsContentLoaded { get; protected set; } = false;
        #endregion


        #region Constructors
        public UIView() { }
        #endregion

        #region Public Methods
        // Initialize
        public void Initialize(TModel model)
        {
            Model = model;
            if (IsContentLoaded)
            {
                UnloadContent();
                LoadContent();
            }
        }

        // Load Content
        public void LoadContent()
        {
            var contentLoader = GameServices.ContentLoader;

            LoadContentImplementation(contentLoader);

            IsContentLoaded = true;
        }

        protected abstract void LoadContentImplementation(ContentLoader contentLoader);

        // Unload Content
        public void UnloadContent()
        {
            UnloadContentImplementation();

            IsContentLoaded = false;
        }

        protected abstract void UnloadContentImplementation();

        // Draw
        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsContentLoaded)
            {
                DrawImplementation(spriteBatch);
            }
        }

        protected abstract void DrawImplementation(SpriteBatch spriteBatch);
        #endregion
    }
}
