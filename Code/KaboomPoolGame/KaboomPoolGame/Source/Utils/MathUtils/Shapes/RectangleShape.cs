﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Utils.MathUtils.Shapes
{
    public class RectangleShape : IShape
    {
        #region Properties
        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Rotation { get; set; }
        #endregion

        #region Constructors
        public RectangleShape(double x, double y, double width, double height)
            : this(x, y, width, height, 0)
        { }

        public RectangleShape(double x, double y, double width, double height, double rotation)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            Rotation = rotation;
        }

        public RectangleShape(Rectangle rect)
            : this(rect, 0)
        { }

        public RectangleShape(Rectangle rect, double rotation)
            : this(rect.Center.X, rect.Center.Y, rect.Width, rect.Height, rotation)
        { }
        #endregion

        #region Public Methods
        public bool CollidesWith(IShape other)
        {
            if (other is RectangleShape)
            {
                var otherRectangle =
                    other as RectangleShape;

                // Either:
                // 1) other rect vertex inside rect
                // 2) sides intersect
                // 3) rect inside other rect

                var geometry = GameServices.Geometry;

                var otherHalfWidth = otherRectangle.Width / 2;
                var otherHalfHeight = otherRectangle.Height / 2;

                var otherRectVertices = new[]
                {
                    geometry.RotatePoint(
                        point: new Vector2((float)(otherRectangle.X - otherHalfWidth), (float)(Y - otherHalfHeight)),
                        origin: new Vector2((float)otherRectangle.X, (float)otherRectangle.Y),
                        angle: otherRectangle.Rotation),
                    geometry.RotatePoint(
                        point: new Vector2((float)(otherRectangle.X - otherHalfWidth), (float)(otherRectangle.Y + otherHalfHeight)),
                        origin: new Vector2((float)otherRectangle.X, (float)otherRectangle.Y),
                        angle: otherRectangle.Rotation),
                    geometry.RotatePoint(
                        point: new Vector2((float)(otherRectangle.X + otherHalfWidth), (float)(otherRectangle.Y + otherHalfHeight)),
                        origin: new Vector2((float)otherRectangle.X, (float)otherRectangle.Y),
                        angle: otherRectangle.Rotation),
                    geometry.RotatePoint(
                        point: new Vector2((float)(otherRectangle.X + otherHalfWidth), (float)(otherRectangle.Y - otherHalfHeight)),
                        origin: new Vector2((float)otherRectangle.X, (float)otherRectangle.Y),
                        angle: otherRectangle.Rotation),
                };
                // 1)
                foreach (var otherRectVertex in otherRectVertices)
                {
                    var isInside = geometry.IsPointInsideRectangle(
                        point: otherRectVertex,
                        rectangleCenter: new Vector2((float)X, (float)Y),
                        width: Width,
                        height: Height,
                        rotation: Rotation);

                    if (isInside)
                    {
                        return true;
                    }
                }


                // 2)
                var halfWidth = Width / 2;
                var halfHeight = Height / 2;

                var thisRectVertices = new[]
                {
                    geometry.RotatePoint(
                        point: new Vector2((float)(X - halfWidth), (float)(Y - halfHeight)),
                        origin: new Vector2((float)X, (float)Y),
                        angle: Rotation),
                    geometry.RotatePoint(
                        point: new Vector2((float)(X + halfWidth), (float)(Y - halfHeight)),
                        origin: new Vector2((float)X, (float)Y),
                        angle: Rotation),
                    geometry.RotatePoint(
                        point: new Vector2((float)(X + halfWidth), (float)(Y + halfHeight)),
                        origin: new Vector2((float)X, (float)Y),
                        angle: Rotation),
                    geometry.RotatePoint(
                        point: new Vector2((float)(X - halfWidth), (float)(Y + halfHeight)),
                        origin: new Vector2((float)X, (float)Y),
                        angle: Rotation),
                };

                var doSidesCrossSideA =
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[0],
                        segment1B: thisRectVertices[1],
                        segment2A: otherRectVertices[0],
                        segment2B: otherRectVertices[1]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[0],
                        segment1B: thisRectVertices[1],
                        segment2A: otherRectVertices[1],
                        segment2B: otherRectVertices[2]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[0],
                        segment1B: thisRectVertices[1],
                        segment2A: otherRectVertices[2],
                        segment2B: otherRectVertices[3]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[0],
                        segment1B: thisRectVertices[1],
                        segment2A: otherRectVertices[3],
                        segment2B: otherRectVertices[0]);

                var doSidesCrossSideB =
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[1],
                        segment1B: thisRectVertices[2],
                        segment2A: otherRectVertices[0],
                        segment2B: otherRectVertices[1]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[1],
                        segment1B: thisRectVertices[2],
                        segment2A: otherRectVertices[1],
                        segment2B: otherRectVertices[2]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[1],
                        segment1B: thisRectVertices[2],
                        segment2A: otherRectVertices[2],
                        segment2B: otherRectVertices[3]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[1],
                        segment1B: thisRectVertices[2],
                        segment2A: otherRectVertices[3],
                        segment2B: otherRectVertices[0]);

                var doSidesCrossSideC =
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[2],
                        segment1B: thisRectVertices[3],
                        segment2A: otherRectVertices[0],
                        segment2B: otherRectVertices[1]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[2],
                        segment1B: thisRectVertices[3],
                        segment2A: otherRectVertices[1],
                        segment2B: otherRectVertices[2]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[2],
                        segment1B: thisRectVertices[3],
                        segment2A: otherRectVertices[2],
                        segment2B: otherRectVertices[3]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[2],
                        segment1B: thisRectVertices[3],
                        segment2A: otherRectVertices[3],
                        segment2B: otherRectVertices[0]);

                var doSidesCrossSideD =
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[3],
                        segment1B: thisRectVertices[0],
                        segment2A: otherRectVertices[0],
                        segment2B: otherRectVertices[1]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[3],
                        segment1B: thisRectVertices[0],
                        segment2A: otherRectVertices[1],
                        segment2B: otherRectVertices[2]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[3],
                        segment1B: thisRectVertices[0],
                        segment2A: otherRectVertices[2],
                        segment2B: otherRectVertices[3]) ||
                    geometry.DoSegmentsIntersect(
                        segment1A: thisRectVertices[3],
                        segment1B: thisRectVertices[0],
                        segment2A: otherRectVertices[3],
                        segment2B: otherRectVertices[0]);

                var doSidesCross =
                    doSidesCrossSideA ||
                    doSidesCrossSideB ||
                    doSidesCrossSideC ||
                    doSidesCrossSideD;

                if (doSidesCross)
                {
                    return true;
                }
                

                // 3)
                if (this.IsInsideOf(otherRectangle))
                {
                    return true;
                }

                // No Collision
                return false;
            }
            else if (other is CircleShape)
            {
                var otherCircle =
                    other as CircleShape;

                return otherCircle.CollidesWith(this);
            }
            else if (other is PointShape)
            {
                var otherPoint =
                    other as PointShape;

                return otherPoint.IsInsideOf(this);
            }
            else if (other is NullShape)
            {
                return false;
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    message: "Unhandled Shape.",
                    paramName: nameof(other),
                    actualValue: other);
            }
        }

        public bool IsInsideOf(IShape other)
        {
            if (other is RectangleShape)
            {
                // All vertices should be
                // inside the other rectangle

                var otherRectangle =
                    other as RectangleShape;

                var otherRectangleCenter = new Vector2(
                    x: (float)otherRectangle.X,
                    y: (float)otherRectangle.Y);

                var halfWidth = Width / 2;
                var halfHeight = Height / 2;

                var geometry = GameServices.Geometry;

                var rectangleVertices = new[]
                {
                    new Vector2(
                        x: (float)(X - halfWidth),
                        y: (float)(Y - halfHeight)),
                    new Vector2(
                        x: (float)(X + halfWidth),
                        y: (float)(Y - halfHeight)),
                    new Vector2(
                        x: (float)(X + halfWidth),
                        y: (float)(Y + halfHeight)),
                    new Vector2(
                        x: (float)(X - halfWidth),
                        y: (float)(Y + halfHeight)),
                };

                var properlyRotatedVertices = new List<Vector2>();
                foreach (var vertex in rectangleVertices)
                {
                    var rotatedVertex = geometry.RotatePoint(
                        point: vertex,
                        origin: new Vector2((float)X, (float)Y),
                        angle: Rotation);

                    properlyRotatedVertices.Add(rotatedVertex);
                }

                foreach (var vertex in properlyRotatedVertices)
                {
                    var isPointInside =
                        geometry.IsPointInsideRectangle(
                            point: vertex,
                            rectangleCenter: otherRectangleCenter,
                            width: otherRectangle.Width,
                            height: otherRectangle.Height,
                            rotation: otherRectangle.Rotation);

                    if (!isPointInside)
                    {
                        return false;
                    }
                }

                return true;
            }
            else if (other is CircleShape)
            {
                // All rectangle vertices must
                // lie inside the circle
                var otherCircle =
                    other as CircleShape;

                var geometry = GameServices.Geometry;

                var rotatedCircleCenter =
                    geometry.RotatePoint(
                        point: new Vector2(
                            x: (float)otherCircle.X,
                            y: (float)otherCircle.Y),
                        origin: new Vector2(
                            x: (float)X,
                            y: (float)Y),
                        angle: -Rotation);

                var halfWidth = Width / 2;
                var halfHeight = Height / 2;

                var rectangleVecrtices = new[]
                {
                    new Vector2(
                        x: (float)(X - halfWidth),
                        y: (float)(Y - halfHeight)),
                    new Vector2(
                        x: (float)(X + halfWidth),
                        y: (float)(Y - halfHeight)),
                    new Vector2(
                        x: (float)(X + halfWidth),
                        y: (float)(Y + halfHeight)),
                    new Vector2(
                        x: (float)(X - halfWidth),
                        y: (float)(Y + halfHeight)),
                };

                foreach (var vertex in rectangleVecrtices)
                {
                    var isInside =
                        geometry.IsPointInsideCircle(
                            point: vertex,
                            circleCenter: rotatedCircleCenter,
                            radius: otherCircle.Radius);

                    if (!isInside)
                    {
                        return false;
                    }
                }

                return true;
            }
            else if (other is PointShape)
            {
                return false;
            }
            else if (other is NullShape)
            {
                return false;
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    message: "Unhandled Shape.",
                    paramName: nameof(other),
                    actualValue: other);
            }
        }
        #endregion
    }
}
