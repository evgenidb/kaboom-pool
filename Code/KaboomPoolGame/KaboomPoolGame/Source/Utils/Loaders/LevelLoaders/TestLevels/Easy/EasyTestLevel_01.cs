﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Models;
using KaboomPoolGame.Source.Models.GameObjects.Terrain;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.TestLevels.Easy
{
    public class EasyTestLevel_01 : ITestLevel
    {
        #region Properties
        public string Name { get; } = "Easy Level 01";

        public LevelData Data { get; }
        #endregion

        #region Constructors
        public EasyTestLevel_01()
        {
            //var level = new LevelData(
            //    difficulty: "Easy",
            //    name: Name);

            //level.InitialBombCount = 3;
            //level.MapSize = new Point(20, 20);
            //level.CellSize = new Point(20, 20);

            //var grass = new TerrainType("Grass", "Terrain/Grass");
            //grass.AddLocation("Terrain/Grass", new CellLocationData(0, 0).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(1, 1).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(1, 0).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(0, 1).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(10, 10).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(5, 10).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(5, 11).ToVector2());

            //var water = new TerrainType("Water", "Terrain/Water");
            //water.AddLocation("Terrain/Water", new CellLocationData(1, 2).ToVector2());
            //water.AddLocation("Terrain/Water", new CellLocationData(1, 3).ToVector2());
            //water.AddLocation("Terrain/Water", new CellLocationData(1, 4).ToVector2());

            //var concreteWalls = new TerrainType("Concrete Wall", new[]
            //{
            //    "Terrain/Wall - Concrete - No Steel Bars",
            //    "Terrain/Wall - Concrete - Central Steel Bar",
            //    "Terrain/Wall - Concrete - Horizontal Steel Bars",
            //    "Terrain/Wall - Concrete - Vertical Steel Bars",
            //});
            //concreteWalls.AddLocations("Terrain/Wall - Concrete - No Steel Bars", new[]
            //{
            //    new CellLocationData(7, 8).ToVector2(),
            //    new CellLocationData(10, 3).ToVector2(),
            //});
            //concreteWalls.AddLocations("Terrain/Wall - Concrete - Horizontal Steel Bars", new[]
            //{
            //    new CellLocationData(8, 8).ToVector2(),
            //    new CellLocationData(2, 7).ToVector2(),
            //});

            ////level.Terrain.AddRange(new List<TerrainType>
            ////{
            ////    grass,
            ////    water,
            ////    concreteWalls,
            ////});

            //level.Player = new CellLocationData(5, 4);

            //Data = level;
        }
        #endregion
    }
}
