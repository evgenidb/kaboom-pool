﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.Sprites;

namespace KaboomPoolGame.Source.Models.GameObjects
{
    public interface IRenderableModel
    {
        Layer Layer { get; }
        ISpriteData Sprite { get; }

        IEnumerable<string> GetSpriteNames();
    }
}
