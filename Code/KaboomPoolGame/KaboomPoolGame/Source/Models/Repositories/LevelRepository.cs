﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.Loaders.LevelLoaders;

namespace KaboomPoolGame.Source.Models.Repositories
{
    public class LevelRepository
    {
        #region Public Properties
        public string CurrentDifficulty =>
            DifficultyOrder[DifficultyIndex];

        public string CurrentLevelName =>
            LevelNames[CurrentDifficulty][LevelIndex];

        public LevelData CurrentLevel
        {
            get
            {
                var name = CurrentLevelName;
                // Check if the level holder have
                // the wanted level.
                if (name == CurrentLevelHolder?.Name)
                {
                    return CurrentLevelHolder;
                }

                // Find level and load it
                var difficulty = CurrentDifficulty;
                var loadedLevels = Levels[difficulty];
                foreach (var loadedLevel in loadedLevels)
                {
                    if (name == loadedLevel.Name)
                    {
                        CurrentLevelHolder = loadedLevel;
                        return loadedLevel;
                    }
                }

                // Level not found. Load it then.
                var level =
                    LoadLevel(difficulty, name);
                CurrentLevelHolder = level;
                return level;
            }
        }

        public IReadOnlyList<string> Difficulties =>
            DifficultyOrder;
        #endregion


        #region Private Properties
        private bool IsLazyLoaded { get; }

        private int DifficultyIndex { get; set; } = 0;

        private int LevelIndex { get; set; } = 0;

        private List<string> DifficultyOrder { get; } =
            new List<string>();

        private IDictionary<string, HashSet<LevelData>> Levels { get; } =
            new Dictionary<string, HashSet<LevelData>>();

        private IDictionary<string, List<string>> LevelNames { get; } =
            new Dictionary<string, List<string>>();

        private LevelData CurrentLevelHolder { get; set; } = null;

        private ILevelLoader Loader =>
            GameServices.LevelLoader;
        #endregion


        #region Constructors
        public LevelRepository(bool areLevelsLazyLoaded)
        {
            IsLazyLoaded = areLevelsLazyLoaded;
        }
        #endregion


        #region Public Methods
        public void Initialize(IReadOnlyList<string> difficultyOrder, IReadOnlyDictionary<string, IReadOnlyList<string>> levelNames)
        {
            if (difficultyOrder.Count != levelNames.Count)
            {
                var exceptionMessage = string.Join(" ",
                    $"{difficultyOrder} count and",
                    $"difficulties count in {levelNames}",
                    "must be equal.");

                throw new ArgumentException(
                    message: exceptionMessage);
            }

            DifficultyOrder.Clear();
            LevelNames.Clear();
            Levels.Clear();

            DifficultyOrder.AddRange(difficultyOrder);

            foreach (var difficultyInfo in levelNames)
            {
                var difficulty = difficultyInfo.Key;
                LevelNames[difficulty] = new List<string>();
                Levels[difficulty] = new HashSet<LevelData>();

                AddLevels(
                    difficulty: difficulty,
                    levels: difficultyInfo.Value);
            }
        }

        public void LoadContent()
        {
            if (!IsLazyLoaded)
            {
                foreach (var difficultyInfo in LevelNames)
                {
                    var difficulty = difficultyInfo.Key;
                    var levels = difficultyInfo.Value;

                    foreach (var levelName in levels)
                    {
                        var level = LoadLevel(
                            difficulty: difficulty,
                            name: levelName);

                        Levels[difficulty].Add(level);
                    }
                }
            }
        }

        public void UnloadContent()
        {
            Levels.Clear();
        }

        public LevelData FirstLevel()
        {
            LevelIndex = 0;
            DifficultyIndex = 0;

            return CurrentLevel;
        }

        public LevelData NextLevel()
        {
            LevelIndex += 1;
            if (LevelIndex >= LevelNames[CurrentDifficulty].Count)
            {
                NextDifficulty();
            }

            return CurrentLevel;
        }

        public LevelData PreviousLevel()
        {
            LevelIndex -= 1;
            if (LevelIndex < 0)
            {
                PreviousDifficulty();
            }

            return CurrentLevel;
        }

        public void NextDifficulty()
        {
            LevelIndex = 0;

            DifficultyIndex += 1;
            if (DifficultyIndex >= DifficultyOrder.Count)
            {
                DifficultyIndex = 0;
            }
        }

        public void PreviousDifficulty()
        {
            LevelIndex = 0;

            DifficultyIndex -= 1;

            if (DifficultyIndex < 0)
            {
                DifficultyIndex = DifficultyOrder.Count - 1;
            }
        }

        public void ReloadAllLevels()
        {
            foreach (var item in Levels)
            {
                var difficulty = item.Key;
                var levelsCollection = item.Value;

                var reloadedLevels = new HashSet<LevelData>();
                foreach (var level in levelsCollection)
                {
                    var levelName = level.Name;

                    var reloadedLevel =
                        LoadLevel(difficulty, levelName);

                    reloadedLevels.Add(reloadedLevel);
                }

                levelsCollection.Clear();

                foreach (var reloadedLevel in reloadedLevels)
                {
                    levelsCollection.Add(reloadedLevel);
                }
            }
        }

        public void ReloadLevel(string difficulty, string name)
        {
            if (Levels.ContainsKey(difficulty))
            {
                int removedLevelsCount =
                    Levels[difficulty].RemoveWhere(
                        level => level.Name == name);

                if (removedLevelsCount > 0)
                {
                    var reloadedLevel =
                    LoadLevel(difficulty, name);

                    Levels[difficulty].Add(reloadedLevel);
                }
            }
        }
        #endregion


        #region Private Methods
        private void AddLevel(string difficulty, string level)
        {
            if (DifficultyOrder.Contains(difficulty))
            {
                LevelNames[difficulty].Add(level);
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    message: "No such difficulty",
                    paramName: nameof(difficulty),
                    actualValue: difficulty);
            }
        }

        private LevelData LoadLevel(string difficulty, string name)
        {
            return Loader.Load(difficulty, name);
        }

        private void AddLevels(string difficulty, IReadOnlyList<string> levels)
        {
            foreach (var level in levels)
            {
                AddLevel(difficulty, level);
            }
        }
        #endregion
    }
}
