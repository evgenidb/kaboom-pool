﻿using System;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Utils.MathUtils.Shapes
{
    public class PointShape : IShape
    {
        #region Properties
        public double X { get; private set; }
        public double Y { get; private set; }
        #endregion

        #region Constructors
        public PointShape(double x, double y)
        {
            X = x;
            Y = y;
        }
        #endregion

        #region Public Methods
        public bool CollidesWith(IShape other)
        {
            return IsInsideOf(other);
        }

        public bool IsInsideOf(IShape other)
        {
            if (other is RectangleShape)
            {
                var otherRectangle =
                    other as RectangleShape;

                var geometry = GameServices.Geometry;

                var rotatedPoint =
                    geometry.RotatePoint(
                        point: new Vector2(
                            x: (float)X,
                            y: (float)Y),
                        origin: new Vector2(
                            x: (float)otherRectangle.X,
                            y: (float)otherRectangle.Y),
                        angle: -otherRectangle.Rotation);

                var math = GameServices.Math;

                return
                    math.IsInRange(
                        min: otherRectangle.X - otherRectangle.Width / 2,
                        value: rotatedPoint.X,
                        max: otherRectangle.X + otherRectangle.Width / 2)
                    &&
                    math.IsInRange(
                        min: otherRectangle.Y - otherRectangle.Height / 2,
                        value: rotatedPoint.Y,
                        max: otherRectangle.Y + otherRectangle.Height / 2);

            }
            else if (other is CircleShape)
            {
                var otherCircle =
                    other as CircleShape;

                var distance = new Vector2(
                    x: (float)(X - otherCircle.X),
                    y: (float)(Y - otherCircle.Y))
                    .Length();

                return distance <= otherCircle.Radius;
            }
            else if (other is PointShape)
            {
                var otherPoint =
                    other as PointShape;

                return
                    X == otherPoint.X &&
                    Y == otherPoint.Y;
            }
            else if (other is NullShape)
            {
                return false;
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    message: "Unhandled Shape.",
                    paramName: nameof(other),
                    actualValue: other);
            }
        }
        #endregion
    }
}
