﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Models;
using KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.TestLevels;
using KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.TestLevels.Easy;
using KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.TestLevels.Normal;

namespace KaboomPoolGame.Source.Utils.Loaders.LevelLoaders
{
    public class TestLevelLoader : ILevelLoader
    {
        #region Properties
        private Dictionary<string, List<ITestLevel>> TestLevels { get; } =
            new Dictionary<string, List<ITestLevel>>();

        private List<string> DifficultyOrder { get; } =
            new List<string>();
        #endregion

        #region Public Methods
        public void Initialize()
        {
            DifficultyOrder.AddRange(new[] { "Easy", "Normal" });

            // Easy Levels
            var levels = new List<ITestLevel>();
            levels.AddRange(new ITestLevel[]
            {
                new EasyTestLevel_01(),
                new EasyTestLevel_02(),
                new EasyTestLevel_03(),
            });
            TestLevels["Easy"] = levels;


            // Normal Levels
            levels = new List<ITestLevel>();
            levels.AddRange(new ITestLevel[]
            {
                new NormalTestLevel_01(),
                new NormalTestLevel_02(),
            });
            TestLevels["Normal"] = levels;
        }

        public IReadOnlyList<string> LoadDifficultyOrder()
        {
            return DifficultyOrder;
        }

        public IReadOnlyList<string> LoadLevelOrder(string difficulty)
        {
            var levelNames = new List<string>();
            foreach (var level in TestLevels[difficulty])
            {
                levelNames.Add(level.Name);
            }
            return levelNames;
        }

        public LevelData Load(string difficulty, string name)
        {
            var foundLevel = TestLevels[difficulty]
                .Find(level => level.Name == name);

            return foundLevel.Data;
        }
        #endregion
    }
}
