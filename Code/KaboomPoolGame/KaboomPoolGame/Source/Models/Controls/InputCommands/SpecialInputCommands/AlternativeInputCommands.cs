﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.SpecialInputCommands
{
    public class AlternativeInputCommands : IInputCommand
    {
        #region Properties
        private HashSet<IInputCommand> AlternativeCommands { get; } =
            new HashSet<IInputCommand>();

        public IReadOnlyCollection<IInputCommand> Alternatives
        {
            get { return AlternativeCommands; }
        }
        #endregion

        #region Constructors
        public AlternativeInputCommands(IEnumerable<IInputCommand> alternativeCommands)
        {
            foreach (var alternative in alternativeCommands)
            {
                AddAlternative(alternative);
            }
        }

        public AlternativeInputCommands(params IInputCommand[] commandsArray)
            : this(alternativeCommands: commandsArray)
        { }
        #endregion

        #region Public Methods
        public bool Check(InputManager input, CheckInputArgs args = null)
        {
            foreach (var command in AlternativeCommands)
            {
                if (command.Check(input, args))
                {
                    return true;
                }
            }

            return false;
        }

        public bool Check(InputManager input, IEnumerable<CheckInputArgs> argsCollection)
        {
            foreach (var command in AlternativeCommands)
            {
                if (command.Check(input, argsCollection))
                {
                    return true;
                }
            }

            return false;
        }

        public void AddAlternative(IInputCommand command)
        {
            if (!(command is NullInputCommand))
            {
                AlternativeCommands.Add(command);
            }
        }

        public void RemoveAlternative(IInputCommand command)
        {
            AlternativeCommands.Remove(command);
        }
        #endregion
    }
}
