﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.UI;
using KaboomPoolGame.Source.Models.Structs;
using KaboomPoolGame.Source.Models.UI;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.Loaders;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Views.UI
{
    public class ButtonView : UIView<ButtonModel>, IView<ButtonModel>
    {
        #region Properties
        public Texture2D Texture
        {
            get { return ButtonTextures[Model.ButtonState]; }
        }

        private Dictionary<ButtonState, Texture2D> ButtonTextures { get; } =
            new Dictionary<ButtonState, Texture2D>();
        #endregion


        #region Constructors
        public ButtonView() : base() { }
        #endregion


        #region Protected Methods
        protected override void LoadContentImplementation(ContentLoader contentLoader)
        {
            foreach (var textureItem in Model.TextureNames)
            {
                var texture = contentLoader.LoadImage(textureItem.Value);
                ButtonTextures[textureItem.Key] = texture;
            }
            Model.Dimensions =
                    Texture.Bounds.Size.ToVector2();
        }

        protected override void UnloadContentImplementation()
        {
            ButtonTextures.Clear();
            Model.ButtonState = ButtonState.Normal;

            Model.Dimensions = Vector2.Zero;
        }

        protected override void DrawImplementation(SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(
            //    texture: Texture,
            //    position: Model.Position,
            //    origin: Model.Center);

            string textureName = Model.TextureNames[Model.ButtonState];

            SpriteInfo sprite =
                GameServices.SpritesheetRepository
                .Get("buttons", textureName);

            spriteBatch.Draw(
                texture: sprite.Spritesheet,
                sourceRectangle: sprite.Location,
                position: Model.Position,
                origin: Model.Center);
        }
        #endregion
    }
}
