﻿using KaboomPoolGame.Source.Enums.UI;
using KaboomPoolGame.Source.Models;
using KaboomPoolGame.Source.Views;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers
{
    public class MenuController : BaseController<MenuModel, MenuView>
    {
        #region Constructors
        public MenuController(MenuModel model, MenuView view)
            : base(model, view) { }
        #endregion

        #region Public Methods
        /// <summary>
        /// Select previous button.
        /// Does nothing if first button.
        /// </summary>
        public void PreviousButton()
        {
            ChangeSelectedButtonIndex(
                Model.SelectedButtonIndex - 1);
        }

        /// <summary>
        /// Select next button.
        /// Does nothing if last button.
        /// </summary>
        public void NextButton()
        {
            ChangeSelectedButtonIndex(
                Model.SelectedButtonIndex + 1);
        }

        /// <summary>
        /// Sets the button to clicked state
        /// </summary>
        /// <returns>The name of the clicked button</returns>
        public string ClickButton()
        {
            Model.SelectedButton.Click();
            return Model.SelectedButton.Title;
        }

        /// <summary>
        /// Set the Current Selected Button to Hover.
        /// This is to be called from outside the Controller.
        /// </summary>
        public void HoverCurrentButton()
        {
            Model.SelectedButton.Hover();
        }

        /// <summary>
        /// Checks if mouse is above any button (unless it is clicked!).
        /// Sets that button on hovered if it is.
        /// </summary>
        /// <param name="mousePosition">The vector position of the mouse.</param>
        /// <returns>True if it is above a button.</returns>
        public bool IsAboveButton(Vector2 mousePosition)
        {
            var buttonsCount = Model.Buttons.Count;
            for (int buttonIndex = 0; buttonIndex < buttonsCount; buttonIndex++)
            {
                var button = Model.Buttons[buttonIndex];
                if (button.Bounds.Contains(mousePosition) &&
                    button.ButtonState != ButtonState.Clicked)
                {
                    // Above a button
                    ChangeSelectedButtonIndex(buttonIndex);
                    return true;
                }
            }

            // Not above a button
            return false;
        }
        #endregion

        #region Private Methods
        private void ChangeSelectedButtonIndex(int newValue)
        {
            Model.SelectedButton.Release();
            Model.SelectedButtonIndex = newValue;
            Model.SelectedButton.Hover();
        }
        #endregion
    }
}
