﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.GameObjects;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.GameObjects.Equipments.Bombs
{
    public class Bomb : GameObject, IRenderableModel
    {
        #region Fields
        private double? timeLeft = null;
        #endregion

        #region Properties
        public Layer Layer { get; } = Layer.Bomb;

        public ISpriteData Sprite { get; private set; }

        public bool HasExploded { get; set; }

        public double ExplosionDelay { get; }

        public double TimeLeft
        {
            get { return timeLeft.Value; }
            set
            {
                if (value < timeLeft || timeLeft == null)
                {
                    timeLeft = value;
                }
                else
                {
                    var exceptionMessage = string.Join(" ",
                        "New value must be less",
                        $"the previous value {timeLeft}");

                    throw new ArgumentOutOfRangeException(
                        message: exceptionMessage,
                        paramName: nameof(TimeLeft),
                        actualValue: value);
                }
            }
        }

        public double Range { get; set; }
        public double Power { get; set; }
        public FunctionType ForceFunction { get; set; }
        #endregion

        #region Constructors
        public Bomb(Vector2 position, Vector2 size, ISpriteData sprite, double explosionDelay, double rotation = 0, Vector2? scale = default(Vector2?))
            : base("Bomb", position, size, rotation, scale)
        {
            Sprite = sprite;

            ExplosionDelay = explosionDelay;
            TimeLeft = explosionDelay;

            AddAllowedPublicProperties(new[]{
                nameof(HasExploded),
                nameof(TimeLeft),
                nameof(Range),
                nameof(Power),
                nameof(ForceFunction),
            });
        }
        #endregion

        #region Public Methods
        public IEnumerable<string> GetSpriteNames()
        {
            return Sprite.TextureNames;
        }
        #endregion
    }
}