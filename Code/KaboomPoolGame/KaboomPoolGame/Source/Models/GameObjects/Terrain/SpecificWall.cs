﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.GameObjects.Terrain
{
    public class SpecificWall : GameObject
    {
        #region Properties

        #endregion

        #region Constructors
        public SpecificWall(string wallName, Vector2 position, Vector2 size, double rotation = 0, Vector2? scale = null)
            : base(wallName, position, size, rotation, scale)
        { }
        #endregion

        #region Public Methods
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj is SpecificWall)
            {
                return Equals(obj as SpecificWall);
            }
            else
            {
                var exceptionMessage = string.Join(" ",
                    "Cannot compare",
                    $"{ nameof(SpecificWall)} object",
                    $"with non-{nameof(SpecificWall)} object.");

                throw new InvalidOperationException(
                    exceptionMessage);
            }
        }

        public bool Equals(SpecificWall obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(SpecificWall objA, SpecificWall objB)
        {
            return objA.Equals(objB);
        }

        public static bool operator !=(SpecificWall objA, SpecificWall objB)
        {
            return !(objA.Equals(objB));
        }
        #endregion
    }
}
