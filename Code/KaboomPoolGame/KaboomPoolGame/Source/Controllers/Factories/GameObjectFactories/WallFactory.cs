﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Models.GameObjects.Obstacles;
using KaboomPoolGame.Source.Models.GameObjects.Terrain;
using KaboomPoolGame.Source.Models.Sprites;

namespace KaboomPoolGame.Source.Controllers.Factories.GameObjectFactories
{
    public class WallFactory
    {
        public WallType Make(string type)
        {
            switch (type)
            {
                case "Concrete Wall":
                    return MakeConcreteWall();
                case "Brick Wall":
                    return MakeBrickWall();
                default:
                    throw new ArgumentOutOfRangeException(
                        message: "Unknown Wall type",
                        paramName: nameof(type),
                        actualValue: type);
            }
        }

        public WallType MakeConcreteWall()
        {
            var sprites = new Dictionary<string, ISpriteData>
            {
                ["Concrete Wall - No Bars"] = new SpriteData("Concrete Wall", "Terrain/Concrete Wall - No Bars"),
                ["Concrete Wall - One Bar"] = new SpriteData("Concrete Wall", "Terrain/Concrete Wall - One Bar"),
                ["Concrete Wall - Horizontal Bars"] = new SpriteData("Concrete Wall", "Terrain/Concrete Wall - Horizontal Bars"),
                ["Concrete Wall - Vertical Bars"] = new SpriteData("Concrete Wall", "Terrain/Concrete Wall - Vertical Bars"),
            };
            var model = new WallType("Concrete Wall", sprites);

            model.IsDestructable = false;

            return model;
        }

        public WallType MakeBrickWall()
        {
            var sprite = new SpriteData("Brick Wall", "Terrain/Brick Wall");
            var model = new WallType("Brick Wall", "Brick Wall", sprite);

            model.IsDestructable = false;

            return model;
        }
    }
}
