﻿namespace KaboomPoolGame.Source.Utils.MathUtils.Shapes
{
    public interface IShape
    {
        bool CollidesWith(IShape other);
        bool IsInsideOf(IShape other);
    }
}
