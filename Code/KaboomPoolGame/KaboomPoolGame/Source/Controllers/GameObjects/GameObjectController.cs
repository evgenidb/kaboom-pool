﻿using KaboomPoolGame.Source.Models.GameObjects;
using KaboomPoolGame.Source.Views;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.GameObjects
{
    public abstract class GameObjectController<TModel, TView> : BaseController<TModel, TView>
        where TModel : GameObject
        where TView : IView<TModel>
    {
        #region Properties
        public string Name => Model.Name;
        public virtual bool MustRemove => Model.MustRemove;
        public Vector2 Position => Model.Position;
        #endregion

        #region Constructors
        public GameObjectController(TModel model, TView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public T GetProperty<T>(string name)
        {
            return Model.GetProperty<T>(name);
        }
        #endregion
    }
}
