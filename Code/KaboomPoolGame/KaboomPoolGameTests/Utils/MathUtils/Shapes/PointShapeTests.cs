﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using KaboomPoolGame.Source.Utils.MathUtils.Shapes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KaboomPoolGameTests.Utils.MathUtils.Shapes
{
    [TestClass]
    public class PointShapeTests
    {
        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            GameServices.Geometry = new GeometryUtil();
            GameServices.Math = new MathUtil();
        }
        #endregion


        #region Test Methods

        #region Test Method IsInsideOf
        [TestMethod]
        public void PointShapeInsideNullShapeShouldReturnFalse()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new NullShape();

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointShapeInsidePointShouldReturnTrue()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new PointShape(2, 3);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeOutsidePointShouldReturnFalse()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new PointShape(1, 1);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointShapeInsideCircleShouldReturnTrue()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new CircleShape(
                x: 0,
                y: 1,
                radius: 5);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeOutsideCircleShouldReturnFalse()
        {
            // Assign
            var shape = new PointShape(12, 3);

            var otherShape = new CircleShape(
                x: 0,
                y: 1,
                radius: 5);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointShapeBorderInsideRectangleShouldReturnTrue()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 4);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeInsideRectangleShouldReturnTrue()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 7,
                height: 5);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeOutsideRectangleShouldReturnFalse()
        {
            // Assign
            var shape = new PointShape(2, 4);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 2);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointShapeBorderInsideRotatedRectangleShouldReturnTrue()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new PointShape(
                x: 2,
                y: 1 + Math.Tan(angle));

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 2,
                rotation: angle);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeInsideRotatedRectangleShouldReturnTrue()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new PointShape(
                x: 1,
                y: 2);
            
            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 7,
                height: 5,
                rotation: angle);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeOutsideRotatedRectangleShouldReturnFalse()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new PointShape(2, 0.5);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 2,
                rotation: angle);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Test Method CollidesWith
        [TestMethod]
        public void PointShapeCollidesNullShapeShouldReturnFalse()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new NullShape();

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointShapeCollidesPointShouldReturnTrue()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new PointShape(2, 3);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeNotCollidePointShouldReturnFalse()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new PointShape(1, 1);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointShapeCollidesCircleShouldReturnTrue()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new CircleShape(
                x: 0,
                y: 1,
                radius: 5);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeNotCollideCircleShouldReturnFalse()
        {
            // Assign
            var shape = new PointShape(12, 3);

            var otherShape = new CircleShape(
                x: 0,
                y: 1,
                radius: 5);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointShapeCollidesRectangleShouldReturnTrue()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 8,
                height: 5);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeBorderCollidesRectangleShouldReturnTrue()
        {
            // Assign
            var shape = new PointShape(2, 3);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 4);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeNotCollideRectangleShouldReturnFalse()
        {
            // Assign
            var shape = new PointShape(2, 4);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 2);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointShapeBorderCollideRotatedRectangleShouldReturnTrue()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new PointShape(
                x: 2,
                y: 1 + Math.Tan(angle));

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 2,
                rotation: angle);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeCollidesRotatedRectangleShouldReturnTrue()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new PointShape(
                x: 1,
                y: 2);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 7,
                height: 5,
                rotation: angle);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointShapeNotCollideRotatedRectangleShouldReturnFalse()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new PointShape(2, 0.5);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 2,
                rotation: angle);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #endregion
    }
}
