﻿using System;
using System.Collections.Generic;
using System.Linq;
using KaboomPoolGame.Source.Utils;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.Sprites
{
    public class VariableAnimation : ISpriteData
    {
        #region Fields
        private double totalTime;
        private int frameIndex;
        #endregion

        #region Properties
        public string SpriteName { get; }
        public string TextureName =>
            Frames[FrameIndex].TextureName;

        public IEnumerable<string> TextureNames =>
            Frames.Select(frame => frame.TextureName);

        public bool IsAnimation { get; } = true;
        public bool HasFinished { get; private set; } = false;
        #endregion

        #region Private Properties
        private bool IsLooped { get; }

        private List<FrameData> Frames { get; } =
            new List<FrameData>();

        private int FramesCount => Frames.Count;

        private double TotalAnimationDuration { get; }

        private int FrameIndex
        {
            get { return frameIndex; }
            set
            {
                if (value >= 0 && value < FramesCount)
                {
                    frameIndex = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        message: $"{FrameIndex} must be a positive number.",
                        paramName: nameof(FrameIndex),
                        actualValue: value);
                }
            }
        }

        private double FrameDuration =>
            Frames[FrameIndex].Duration;

        private double TotalTime
        {
            get { return totalTime; }
            set
            {
                if (value >= 0)
                {
                    totalTime = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        message: $"{TotalTime} must be a positive number or zero.",
                        paramName: nameof(TotalTime),
                        actualValue: value);
                }
            }
        }
        #endregion

        #region Constructors
        public VariableAnimation(string animationName, IList<FrameData> frames, bool isLooped)
        {
            SpriteName = animationName;
            IsLooped = isLooped;

            AddTextues(frames);
            TotalAnimationDuration =
                CalculateAnimationDuration(frames);

            TotalTime = 0;
        }
        #endregion

        #region Public Methods
        public void Update(GameTime gameTime)
        {
            var increment =
                gameTime.ElapsedGameTime.TotalSeconds;

            TotalTime += increment;

            CalculateFrameIndex();
        }

        public void Reset()
        {
            TotalTime = 0;
            FrameIndex = 0;
            HasFinished = false;
        }
        #endregion

        #region Private Methods
        private void AddTextues(IList<FrameData> textures)
        {
            if (textures == null)
            {
                throw new ArgumentNullException(
                    message: "Textures must be initialized list",
                    paramName: nameof(textures));
            }
            else if (textures.Count < 1)
            {
                throw new ArgumentException(
                    message: "Textures list cannot be empty.",
                    paramName: nameof(textures));
            }

            Frames.AddRange(textures);
        }

        private double CalculateAnimationDuration(IList<FrameData> frames)
        {
            var duration = frames
                .Select(texture => texture.Duration)
                .Sum();

            return duration;
        }

        private void CalculateFrameIndex()
        {
            // Return if Play Once and on last frame
            var lastFrameIndex = FramesCount - 1;

            if (!IsLooped && FrameIndex == lastFrameIndex)
            {
                return;
            }

            var timeSinceFirstFrame =
                TotalTime % (FramesCount * FrameDuration);

            var currentIndex = FrameIndex;
            var currentAnimationDuration =
                Frames.Take(FrameIndex + 1)
                .Select(texture => texture.Duration).Sum();

            // Reset if animation looped
            if (timeSinceFirstFrame < currentAnimationDuration)
            {
                currentAnimationDuration = 0;
                currentIndex = 0;
            }

            while (true)
            {
                var nextFrameDuration =
                    Frames[currentIndex].Duration;

                if (currentAnimationDuration + nextFrameDuration > timeSinceFirstFrame)
                {
                    break;
                }

                currentAnimationDuration += nextFrameDuration;
                currentIndex++;
            }

            FrameIndex = currentIndex;

            if (FrameIndex > lastFrameIndex)
            {
                if (IsLooped)
                {
                    FrameIndex = FrameIndex % lastFrameIndex;
                }
                else
                {
                    FrameIndex = GameServices.Math.Clamp(
                        min: 0,
                        value: FrameIndex,
                        max: lastFrameIndex);

                    HasFinished = true;
                }
            }
        }
        #endregion
    }
}
