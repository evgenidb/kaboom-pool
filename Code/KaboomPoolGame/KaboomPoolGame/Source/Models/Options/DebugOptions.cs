﻿using KaboomPoolGame.Source.Models.Controls;
using Microsoft.Xna.Framework.Input;

namespace KaboomPoolGame.Source.Models.Options
{
    public class DebugOptions
    {
        #region Fields
        private Keys[] deactivateMode = new Keys[0];
        #endregion

        #region Properties
        public bool IsDebugMode { get; set; } = false;

        public DebugControls Controls { get; set; }
        #endregion
    }
}
