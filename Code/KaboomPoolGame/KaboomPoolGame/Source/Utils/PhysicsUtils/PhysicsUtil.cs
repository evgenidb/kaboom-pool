﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Enums.GameObjects;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Utils.PhysicsUtils
{
    public class PhysicsUtil
    {
        public Vector2 GetForce(Vector2 direction, double distance, double force, double maxRange, FunctionType functionType)
        {
            if (distance > maxRange)
            {
                return Vector2.Zero;
            }

            var dir = Vector2.Normalize(direction);
            var mult = 1 - (distance / maxRange);

            double resultForce = 0;

            switch (functionType)
            {
                case FunctionType.Constant:
                    // Force is always the same
                    resultForce = force * 1;
                    break;
                case FunctionType.Linear:
                    // Force diminishes at same rate
                    resultForce =
                        force * mult;
                    break;
                case FunctionType.Quadratic:
                    // Force diminishes at quadratic rate
                    // (close to real life)
                    resultForce =
                        force * mult * mult;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(
                        message: "Unhandled Function type.",
                        paramName: nameof(functionType),
                        actualValue: functionType);
            }

            return dir * (float)resultForce;
        }
    }
}
