﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.GameObjects.Effects;
using KaboomPoolGame.Source.Controllers.GameObjects.Equipments.Bombs;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Enums.GameObjects;
using KaboomPoolGame.Source.Models;
using KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.ExtensionMethods;
using KaboomPoolGame.Source.Views;
using KaboomPoolGame.Source.Views.GameObjects.Effects;
using KaboomPoolGame.Source.Views.GameObjects.Equipments.Bombs;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers
{
    public class LevelController : BaseController<LevelModel, LevelView>
    {
        #region Constructors
        public LevelController(LevelModel model, LevelView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public void HandleInput(InputManager input)
        {
            var playControls =
                GameServices.PlayOptions.Controls;

            // Player Movement
            var gameTime = input.GameTime;
            var movementArgs = new HoldInputArgs(gameTime);

            var verticalDirection = "";
            if (playControls.MoveUp.Check(input, movementArgs))
            {
                verticalDirection = "Up";
            }
            else if (playControls.MoveDown.Check(input, movementArgs))
            {
                verticalDirection = "Down";
            }

            var horizontalDirection = "";
            if (playControls.MoveLeft.Check(input, movementArgs))
            {
                horizontalDirection = "Left";
            }
            else if (playControls.MoveRight.Check(input, movementArgs))
            {
                horizontalDirection = "Right";
            }

            bool hasMoved =
                !string.IsNullOrEmpty(verticalDirection) ||
                !string.IsNullOrEmpty(horizontalDirection);
            if (hasMoved)
            {
                var moveAction =
                    $"Player{verticalDirection}{horizontalDirection}";

                ChooseAction(moveAction);
            }


            // Bomb Placement
            if (playControls.PlaceBomb.Check(input))
            {
                ChooseAction("PlaceBomb");
            }
        }

        public override void Update(GameTime gameTime)
        {
            ApplyForces();
            UpdatePositions();
            HandleCollisions();

            // To Delete/Change/Move into methods
            foreach (var terrain in Model.Terrain)
            {
                terrain.Update(gameTime);
            }

            foreach (var wall in Model.Walls)
            {
                wall.Update(gameTime);
            }

            foreach (var target in Model.Targets)
            {
                target.Update(gameTime);
            }

            foreach (var obstacles in Model.Obstacles)
            {
                obstacles.Update(gameTime);
            }

            foreach (var bomb in Model.Bombs)
            {
                bomb.Update(gameTime);
            }

            foreach (var ball in Model.Balls)
            {
                ball.Update(gameTime);
            }

            Model.Player.Update(gameTime);

            CheckExplodedBombs(Model.Bombs);

            Model.Effects.Update(gameTime);

            RemoveGameObjects();    // Should be called last!
        }

        /// <summary>
        /// Restaty Level
        /// </summary>
        public void Restart()
        {
            Model.Initialize();
        }
        #endregion


        #region Private Methods
        private void ChooseAction(string action)
        {
            switch (action)
            {
                case "PlayerUp":
                    Model.Player.MoveUp();
                    break;
                case "PlayerDown":
                    Model.Player.MoveDown();
                    break;
                case "PlayerLeft":
                    Model.Player.MoveLeft();
                    break;
                case "PlayerRight":
                    Model.Player.MoveRight();
                    break;
                case "PlayerUpLeft":
                    Model.Player.MoveUpLeft();
                    break;
                case "PlayerUpRight":
                    Model.Player.MoveUpRight();
                    break;
                case "PlayerDownLeft":
                    Model.Player.MoveDownLeft();
                    break;
                case "PlayerDownRight":
                    Model.Player.MoveDownRight();
                    break;
                case "PlaceBomb":
                    var playerPosition =
                        Model.Player.Position;

                    var bombController =
                        CreateBomb(playerPosition);

                    Model.Bombs.Add(bombController);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(
                        message: "Unhandled action case.",
                        paramName: nameof(action),
                        actualValue: action);
            }
        }

        private void ApplyForces()
        {

        }

        private void UpdatePositions()
        {

        }

        private void HandleCollisions()
        {

        }

        private BombController CreateBomb(Vector2 position)
        {
            var factory = GameServices.EntityFactories
                .BombFactory;

            var model = factory.MakeBomb();

            model.Position = position;

            var controller = new BombController(
                model: model,
                view: new BombView());

            controller.Initialize();
            controller.LoadContent();

            return controller;
        }

        private ExplosionController CreateExplosion(Vector2 position)
        {
            var factory = GameServices.EntityFactories
                .EffectsFactory;

            var model = factory.MakeExplosion();

            model.Position = position;

            var controller = new ExplosionController(
                model: model,
                view: new ExplosionView());

            controller.Initialize();

            return controller;
        }

        private CraterController CreateCrater(Vector2 position)
        {
            var factory = GameServices.EntityFactories
                .EffectsFactory;

            var model = factory.MakeCrater();

            model.Position = position;

            var controller = new CraterController(
                model: model,
                view: new CraterView());

            controller.Initialize();

            return controller;
        }

        private void CheckExplodedBombs(ICollection<BombController> bombs)
        {
            foreach (var bomb in bombs)
            {
                if (bomb.HasExploded)
                {
                    var bombPosition = bomb.Position;

                    // Create Effects
                    var crater = CreateCrater(bombPosition);
                    var explosion = CreateExplosion(bombPosition);
                    Model.Effects.AddCrater(crater);
                    Model.Effects.AddExplosion(explosion);

                    // Apply Force to other objects
                    // (Balls, Glass Wall, etc.)
                    ApplyExplosiveForce(bomb);
                }
            }
        }

        private void ApplyExplosiveForce(BombController bomb)
        {
            var bombPosition = bomb.Position;

            var blastRange =
                bomb.GetProperty<double>("Range");

            var blastPower =
                bomb.GetProperty<double>("Power");

            var forceFunction =
                bomb.GetProperty<FunctionType>(
                    "ForceFunction");

            var geometry = GameServices.Geometry;
            var physics = GameServices.Physics;

            // Destructable Obstacles in range
            foreach (var obstacle in Model.Obstacles)
            {
                var isDestructable =
                    obstacle.GetProperty<bool>(
                        "IsDestructable");

                if (isDestructable)
                {
                    var obstaclePosition = obstacle.Position;

                    bool isObstacleInsideExplosion =
                        geometry.IsPointInsideCircle(
                            point: obstaclePosition,
                            circleCenter: bombPosition,
                            radius: blastRange);

                    if (isObstacleInsideExplosion)
                    {
                        var direction =
                            obstaclePosition - bombPosition;

                        var force = physics.GetForce(
                            direction: direction,
                            distance: direction.Length(),
                            force: blastPower,
                            maxRange: blastRange,
                            functionType: forceFunction);

                        obstacle.ApplyForce(
                            force: force,
                            source: "Bomb");
                    }
                }
            }

            // Balls in range
            foreach (var ball in Model.Balls)
            {
                var ballPosition = ball.Position;

                bool isBallInsideExplosion =
                    geometry.IsPointInsideCircle(
                        point: ballPosition,
                        circleCenter: bombPosition,
                        radius: blastRange);

                if (isBallInsideExplosion)
                {
                    var direction =
                        ballPosition - bombPosition;

                    var force = physics.GetForce(
                        direction: direction,
                        distance: direction.Length(),
                        force: blastPower,
                        maxRange: blastRange,
                        functionType: forceFunction);

                    ball.ApplyForce(
                        force: force,
                        source: "Bomb");
                }
            }
        }

        private void RemoveGameObjects()
        {
            foreach (var terrain in Model.Terrain)
            {
                terrain.Clean();
            }

            foreach (var wall in Model.Walls)
            {
                wall.Clean();
            }

            Model.Targets.RemoveWhere(
                predicate: (item) => item.MustRemove,
                beforeRemoval: (item) => item.UnloadContent());

            Model.Bombs.RemoveWhere(
                predicate: (item) => item.MustRemove,
                beforeRemoval: (item) => item.UnloadContent());

            Model.Balls.RemoveWhere(
                predicate: (item) => item.MustRemove,
                beforeRemoval: (item) => item.UnloadContent());

            Model.Obstacles.RemoveWhere(
                predicate: (item) => item.MustRemove,
                beforeRemoval: (item) => item.UnloadContent());

            Model.Effects.Clean();
        }
        #endregion
    }
}
