﻿using System;
using System.Collections.Generic;

namespace KaboomPoolGame.Source.Utils.ExtensionMethods
{
    public static class ICollectionExtensions
    {
        /// <summary>
        /// Removes items from collection based on predicate.
        /// </summary>
        /// <remarks>
        /// Complexity is at least O(n + m), where n is the collection's size and m is the count of items to be removed.
        /// </remarks>
        /// <typeparam name="T">The type of the items.</typeparam>
        /// <param name="collection">Collection from which the items would be removed.</param>
        /// <param name="predicate">The condition the items would be checked upon.</param>
        /// <param name="beforeRemoval">Action that can be run before the item is removed.</param>
        /// <returns>Items count that were removed</returns>
        /// <exception cref="InvalidOperationException">Thrown if Remove operation is invalid.</exception>
        public static int RemoveWhere<T>(this ICollection<T> collection, Predicate<T> predicate, Action<T> beforeRemoval = null)
        {
            int removedItemsCount = 0;

            // Check for empty action
            if (beforeRemoval == null)
            {
                beforeRemoval = (_) => { };
            }

            // Find items to remove
            var itemsToRemove = new List<T>();
            foreach (var item in collection)
            {
                if (predicate(item))
                {
                    itemsToRemove.Add(item);
                }
            }

            // Remove Items
            foreach (var item in itemsToRemove)
            {
                var isRemoved = collection.Remove(item);

                if (isRemoved)
                {
                    beforeRemoval(item);
                    removedItemsCount += 1;
                }
            }

            return removedItemsCount;
        }
    }
}
