﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.SpecialInputCommands
{
    public class NullInputCommand : IInputCommand
    {
        #region Public Methods
        public bool Check(InputManager input, CheckInputArgs args = null)
        {
            return false;
        }

        public bool Check(InputManager input, IEnumerable<CheckInputArgs> argsCollection)
        {
            return Check(input);
        }

        public override string ToString()
        {
            return "Null input";
        }
        #endregion
    }
}
