﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Models.GameObjects;
using KaboomPoolGame.Source.Views;

namespace KaboomPoolGame.Source.Controllers.GameObjects
{
    public abstract class GameObjectFlyweightController<TModel, TView> : BaseController<TModel, TView>
        where TModel : FlyweightGameObject
        where TView : IView<TModel>
    {
        #region Properties
        public string Name => Model.Name;
        #endregion

        #region Constructors
        public GameObjectFlyweightController(TModel model, TView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public T GetProperty<T>(string name)
        {
            return Model.GetProperty<T>(name);
        }

        public abstract void Clean();
        #endregion
    }
}
