﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace KaboomPoolGameTests.Utils.MathUtils.GeometryUtilTests
{
    [TestClass]
    public class PointInsideCicleTests
    {
        #region Fields
        private static GeometryUtil geometry;
        #endregion

        #region Properties
        public GeometryUtil Geometry =>
            geometry;
        #endregion

        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            geometry = new GeometryUtil();
            GameServices.Geometry = geometry;
        }
        #endregion

        #region Test Methods

        #region Centered Circle, i.e. center is (0, 0)
        [TestMethod]
        public void PointInsideCenteredCircleCenter()
        {
            // Assign
            Vector2 point = new Vector2(0, 0);

            Vector2 circleCenter = new Vector2(0, 0);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideCenteredCircle()
        {
            // Assign
            Vector2 point = new Vector2(3, 4);

            Vector2 circleCenter = new Vector2(0, 0);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideCenteredCircleBorderTop()
        {
            // Assign
            Vector2 point = new Vector2(0, 10);

            Vector2 circleCenter = new Vector2(0, 0);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideCenteredCircleBorderLeft()
        {
            // Assign
            Vector2 point = new Vector2(-10, 0);

            Vector2 circleCenter = new Vector2(0, 0);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideCenteredCircleBorderTopLeft()
        {
            // Assign
            float sqrt = (float)Math.Sqrt(2);
            float coordinate = 10 / sqrt;

            Vector2 point = new Vector2(
                x: -coordinate,
                y: coordinate);

            Vector2 circleCenter = new Vector2(0, 0);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointOutsideCenteredCircle()
        {
            // Assign
            Vector2 point = new Vector2(10, 1);
            Vector2 circleCenter = new Vector2(0, 0);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Shifted Circle, i.e. center is (1, 2)
        [TestMethod]
        public void PointInsideShiftedCircleCenter()
        {
            // Assign
            Vector2 point = new Vector2(1, 2);

            Vector2 circleCenter = new Vector2(1, 2);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideShiftedCircle()
        {
            // Assign
            Vector2 point = new Vector2(4, 6);

            Vector2 circleCenter = new Vector2(1, 2);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideShiftedCircleBorderTop()
        {
            // Assign
            Vector2 point = new Vector2(1, 12);

            Vector2 circleCenter = new Vector2(1, 2);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideShiftedCircleBorderLeft()
        {
            // Assign
            Vector2 point = new Vector2(-9, 2);

            Vector2 circleCenter = new Vector2(1, 2);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideShiftedCircleBorderTopLeft()
        {
            // Assign
            float sqrt = (float)Math.Sqrt(2);
            float coordinate = 10 / sqrt;

            Vector2 point = new Vector2(
                x: 1 - coordinate,
                y: 2 + coordinate);

            Vector2 circleCenter = new Vector2(1, 2);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointOutsideShiftedCircle()
        {
            // Assign
            Vector2 point = new Vector2(11, 3);
            Vector2 circleCenter = new Vector2(1, 2);
            double radius = 10;

            // Act
            bool actual = Geometry.IsPointInsideCircle(
                point: point,
                circleCenter: circleCenter,
                radius: radius);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #endregion
    }
}
