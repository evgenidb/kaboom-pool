﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Models.Structs
{
    public struct SpritesheetInfo
    {
        #region Private Properties
        private IDictionary<string, Rectangle> Sprites { get; }
        #endregion


        #region Properties
        public string Category { get; }
        public Texture2D Spritesheet { get; private set; }
        public string Path { get; }
        #endregion


        #region Constructors
        public SpritesheetInfo(string category, string spritesheetPath, Dictionary<string, Rectangle> sprites)
        {
            Category = category;
            Path = spritesheetPath;
            Sprites = sprites;
            
            Spritesheet = null;
        }
        #endregion


        #region Public Methods
        public void LoadContent()
        {
            var loader = GameServices.ContentLoader;
            Spritesheet = loader.LoadImage(
                $"Spritesheets/{Path}");
        }

        public void UnloadContent()
        {
            Spritesheet = null;
        }

        public Rectangle GetLocation(string spriteName)
        {
            return Sprites[spriteName];
        }

        public bool HasLocation(string image)
        {
            return Sprites.ContainsKey(image);
        }

        public override int GetHashCode()
        {
            return Path.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is SpritesheetInfo)
            {
                return Equals((SpritesheetInfo) obj);
            }
            else
            {
                return false;
            }
        }

        public bool Equals(SpritesheetInfo obj)
        {
            return Path == obj.Path;
        }
        #endregion
    }
}
