﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaboomPoolGame.Source.Utils.MathUtils
{
    public class MathUtil
    {
        public int Clamp(int value, int min, int max)
        {
            return (int)Clamp((double)value, min, max);
        }

        public double Clamp(double value, double min, double max)
        {
            return Math.Min(Math.Max(value, min), max);
        }

        public bool IsInRange(double min, double value, double max)
        {
            return min <= value && value <= max;
        }
    }
}
