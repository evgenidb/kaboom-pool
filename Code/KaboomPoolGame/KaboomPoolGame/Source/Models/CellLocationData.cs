﻿using System;
using KaboomPoolGame.Source.Utils;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models
{
    public class CellLocationData
    {
        #region Constants
        public const double MaxOffset = 0.5;
        #endregion


        #region Fields
        private int cellX;
        private int cellY;

        private double offsetX;
        private double offsetY;
        #endregion

        #region Properties
        public int CellX
        {
            get { return cellX; }
            set
            {
                if (0 <= value)
                {
                    cellX = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        message: "Cell position X must be a non-negative integer",
                        paramName: nameof(CellX),
                        actualValue: value);
                }
            }
        }

        public int CellY
        {
            get { return cellY; }
            set
            {
                if (0 <= value)
                {
                    cellY = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        message: "Cell position Y must be a non-negative integer",
                        paramName: nameof(CellY),
                        actualValue: value);
                }
            }
        }

        public double OffsetX
        {
            get { return offsetX; }
            set
            {
                if (-MaxOffset <= value && value <= MaxOffset)
                {
                    offsetX = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        message: "Offset X must be a non-negative integer",
                        paramName: nameof(OffsetY),
                        actualValue: value);
                }
            }
        }
        public double OffsetY
        {
            get { return offsetY; }
            set
            {
                if (-MaxOffset <= value && value <= MaxOffset)
                {
                    offsetY = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        message: "Offset Y must be a non-negative integer",
                        paramName: nameof(OffsetY),
                        actualValue: value);
                }
            }
        }

        /// <summary>
        /// In Degrees, i.e. 90 is rigth angle,
        /// 360 is full rotation, etc.
        /// </summary>
        public double Rotation { get; private set; }
        #endregion


        #region Constructors
        public CellLocationData(int cellX, int cellY)
        {
            CellX = cellX;
            CellY = cellY;

            OffsetX = 0;
            OffsetY = 0;

            Rotation = 0;
        }

        public CellLocationData(int cellX, int cellY, double rotation)
            : this(cellX, cellY)
        {
            Rotation = rotation;
        }

        public CellLocationData(int cellX, int cellY, double offsetX, double offsetY)
            : this(cellX, cellY)
        {
            OffsetX = offsetX;
            OffsetY = offsetY;
        }

        public CellLocationData(int cellX, int cellY, double offsetX, double offsetY, double rotation)
            : this(cellX, cellY, offsetX, offsetY)
        {
            Rotation = rotation;
        }
        #endregion

        #region Public Methods
        public Vector2 ToVector2()
        {
            var cellSize = GameServices.PlayOptions.CellSize;
            return ToVector2(cellSize);
        }

        public Vector2 ToVector2(double cellWidth, double cellHeight)
        {
            double x = (CellX + MaxOffset + OffsetX) * cellWidth;
            double y = (CellY + MaxOffset + OffsetY) * cellHeight;

            return new Vector2((float)x, (float)y);
        }

        public Vector2 ToVector2(Vector2 cellSize)
        {
            return ToVector2(cellSize.X, cellSize.Y);
        }

        public Vector2 ToVector2(Point cellSize)
        {
            return ToVector2(cellSize.X, cellSize.Y);
        }
        #endregion
    }
}
