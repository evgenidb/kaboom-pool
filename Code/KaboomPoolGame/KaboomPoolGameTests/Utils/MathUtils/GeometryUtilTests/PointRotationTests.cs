﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace KaboomPoolGameTests.Utils.MathUtils.GeometryUtilTests
{
    [TestClass]
    public class PointRotationTests
    {
        #region Constants
        private const double ExpectedPositionDelta =
            0.0000001;
        #endregion

        #region Fields
        private static GeometryUtil geometry;
        #endregion

        #region Properties
        public GeometryUtil Geometry =>
            geometry;
        #endregion

        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            geometry = new GeometryUtil();
            GameServices.Geometry = geometry;
            GameServices.Math = new MathUtil();
        }
        #endregion


        #region Test Methods

        #region No Shift
        [TestMethod]
        public void NoShiftPointAndRotationZero()
        {
            // Assign
            var origin = new Vector2(0, 0);

            var point = new Vector2(0, 0);
            double angle = 0;

            var expected = new Vector2(0, 0);

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }

        [TestMethod]
        public void NoShiftPointAndRotation90Degrees()
        {
            // Assign
            var origin = new Vector2(0, 0);

            var point = new Vector2(0, 0);
            double angle = Math.PI / 2;

            var expected = new Vector2(0, 0);

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }

        [TestMethod]
        public void NoShiftPointAndRotation45Degrees()
        {
            // Assign
            var origin = new Vector2(0, 0);

            var point = new Vector2(0, 0);
            double angle = Math.PI / 4;

            var expected = new Vector2(0, 0);

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }

        [TestMethod]
        public void NoShiftPointOriginShiftedAndRotationZero()
        {
            // Assign
            var origin = new Vector2(5, 18);

            var point = new Vector2(5, 18);
            double angle = 0;

            var expected = new Vector2(5, 18);

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }

        [TestMethod]
        public void NoShiftPointOriginShiftedAndRotation90Degrees()
        {
            // Assign
            var origin = new Vector2(5, 18);

            var point = new Vector2(5, 18);
            double angle = Math.PI / 2;

            var expected = new Vector2(5, 18);

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }

        [TestMethod]
        public void NoShiftPointOriginShiftedAndRotation45Degrees()
        {
            // Assign
            var origin = new Vector2(5, 18);

            var point = new Vector2(5, 18);
            double angle = Math.PI / 4;

            var expected = new Vector2(5, 18);

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }
        #endregion

        #region Point Shifted
        [TestMethod]
        public void ShiftedPointRotationZero()
        {
            var origin = new Vector2(0, 0);

            var point = new Vector2(1, 2);
            double angle = 0;

            var expected = new Vector2(1, 2);

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }

        [TestMethod]
        public void ShiftedPointRotation90Degrees()
        {
            var origin = new Vector2(0, 0);

            var point = new Vector2(1, 2);
            double angle = Math.PI / 2;

            var expected = new Vector2(-2, 1);

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }

        [TestMethod]
        public void ShiftedPointRotation45Degrees()
        {
            var origin = new Vector2(0, 0);

            var point = new Vector2(1, 1);
            double angle = Math.PI / 4;

            var expected = new Vector2(
                x: 0,
                y: (float)Math.Sqrt(2));

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            // (each coordinate separately)
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }

        [TestMethod]
        public void ShiftedPointOriginShiftedRotationZero()
        {
            var origin = new Vector2(5, 18);

            var point = new Vector2(6, 20);
            double angle = 0;

            var expected = new Vector2(6, 20);

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }

        [TestMethod]
        public void ShiftedPointOriginShiftedRotation90Degrees()
        {
            var origin = new Vector2(5, 18);

            var point = new Vector2(6, 20);
            double angle = Math.PI / 2;

            var expected = new Vector2(3, 19);

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }

        [TestMethod]
        public void ShiftedPointOriginShiftedRotation45Degrees()
        {
            var origin = new Vector2(5, 18);

            var point = new Vector2(6, 19);
            double angle = Math.PI / 4;

            var expected = new Vector2(
                x: 5,
                y: (float)(18 + Math.Sqrt(2)));

            // Act
            var actual = Geometry.RotatePoint(
                point: point,
                origin: origin,
                angle: angle);

            // Assert
            Assert.AreEqual(
                expected: expected.X,
                actual: actual.X,
                delta: ExpectedPositionDelta);
            Assert.AreEqual(
                expected: expected.Y,
                actual: actual.Y,
                delta: ExpectedPositionDelta);
        }
        #endregion

        #endregion

    }
}
