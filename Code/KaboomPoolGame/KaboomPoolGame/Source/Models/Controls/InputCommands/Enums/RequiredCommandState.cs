﻿namespace KaboomPoolGame.Source.Models.Controls.InputCommands.Enums
{
    public enum RequiredCommandState
    {
        Up,
        Hold,
        Press,
        Release,
        MouseHover
    }
}
