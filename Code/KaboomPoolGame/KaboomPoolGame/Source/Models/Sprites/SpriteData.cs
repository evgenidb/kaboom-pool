﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.Sprites
{
    public class SpriteData : ISpriteData
    {
        #region Properties
        public string SpriteName { get; }
        public string TextureName { get; }

        public IEnumerable<string> TextureNames =>
            new[] { TextureName };

        public bool IsAnimation { get; } = false;
        public bool HasFinished { get; private set; } = true;
        #endregion

        #region Constructor
        public SpriteData(string spriteName, string textureName)
        {
            SpriteName = spriteName;
            TextureName = textureName;
        }
        #endregion

        #region Public Methods
        public void Update(GameTime gameTime) { }
        public void Reset() { }
        #endregion
    }
}
