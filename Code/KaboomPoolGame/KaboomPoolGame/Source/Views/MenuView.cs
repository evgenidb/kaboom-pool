﻿using System;
using KaboomPoolGame.Source.Models;
using KaboomPoolGame.Source.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Views
{
    public class MenuView : IView<MenuModel>
    {
        #region Fields
        private MenuModel model;
        #endregion

        #region Properties
        protected MenuModel Model
        {
            get
            {
                return model;
            }
            set
            {
                if (value != null)
                {
                    model = value;
                }
                else
                {
                    throw new ArgumentNullException(
                        message: "Menu Model cannot be null",
                        paramName: nameof(Model));
                }
            }
        }
        public bool IsContentLoaded { get; private set; } = false;

        private Texture2D Background { get; set; }
        #endregion

        #region Constructors
        public MenuView() { }
        #endregion

        #region Public Methods
        public void Initialize(MenuModel model)
        {
            Model = model;
            if (IsContentLoaded)
            {
                UnloadContent();
                LoadContent();
            }
        }

        public void LoadContent()
        {
            GameServices.SpritesheetRepository
                   .LoadContent("buttons");

            var contentLoader = GameServices.ContentLoader;
            Background = contentLoader.LoadImage(Model.BackgroundName);
            
            var buttonsSprites = GameServices.ContentLoader
                .LoadSpritesheet(name: "Buttons Spritesheet");

            foreach (var image in Model.Images)
            {
                image.LoadContent();
            }
            foreach (var label in Model.Labels)
            {
                label.LoadContent();
            }
            foreach (var button in Model.Buttons)
            {
                button.LoadContent();
            }

            IsContentLoaded = true;
        }

        public void UnloadContent()
        {
            GameServices.SpritesheetRepository
                 .UnloadContent("buttons");

            Background = null;

            foreach (var image in Model.Images)
            {
                image.UnloadContent();
            }
            foreach (var label in Model.Labels)
            {
                label.UnloadContent();
            }
            foreach (var button in Model.Buttons)
            {
                button.UnloadContent();
            }

            IsContentLoaded = false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsContentLoaded)
            {
                // Background
                var options = GameServices.GameOptions;
                spriteBatch.Draw(
                    texture: Background,
                    //position: Vector2.Zero,
                    destinationRectangle: new Rectangle(
                        x: 0,
                        y: 0,
                        width: options.GameWidth,
                        height: options.GameHeight));

                // Images
                foreach (var image in Model.Images)
                {
                    image.Draw(spriteBatch);
                }

                // Labels
                foreach (var label in Model.Labels)
                {
                    label.Draw(spriteBatch);
                }

                // Buttons
                foreach (var button in Model.Buttons)
                {
                    button.Draw(spriteBatch);
                }
            }
        }
        #endregion
    }
}
