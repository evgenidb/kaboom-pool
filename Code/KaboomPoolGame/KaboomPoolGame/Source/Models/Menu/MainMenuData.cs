﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Models.UI;

namespace KaboomPoolGame.Source.Models.Menu
{
    public struct ButtonData
    {
        public string Title;
        public string NormalStateTextureName;
        public string HoverStateTextureName;
        public string ClickedStateTextureName;
    }

    public class MainMenuData
    {
        public IReadOnlyList<ButtonData> MenuData = new List<ButtonData>
        {
            new ButtonData { Title = "New Game", NormalStateTextureName = "New Game Button - Normal State", HoverStateTextureName = "New Game Button - Hover State", ClickedStateTextureName = "New Game Button - Click State" },
            new ButtonData { Title = "Exit", NormalStateTextureName = "Exit Button - Normal State", HoverStateTextureName = "Exit Button - Hover State", ClickedStateTextureName = "Exit Button - Click State" },
        };
    }
}
