﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaboomPoolGame.Source.Utils.ExtensionMethods
{
    public static class ArrayExtensions
    {
        public static int IndexOf<T>(this T[] array, T value)
        {
            const int InvalidIndex = -1;
            
            for (int elementIndex = 0; elementIndex < array.Length; elementIndex++)
            {
                var element = array[elementIndex];

                if (element.Equals(value))
                {
                    return elementIndex;
                }
            }

            return InvalidIndex;
        }

        public static int LastIndexOf<T>(this T[] array, T value)
        {
            const int InvalidIndex = -1;

            for (int elementIndex = array.Length - 1; elementIndex >= 0; elementIndex--)
            {
                var element = array[elementIndex];

                if (element.Equals(value))
                {
                    return elementIndex;
                }
            }

            return InvalidIndex;
        }
    }
}
