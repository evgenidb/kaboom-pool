﻿using System;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.GameObjects.Terrain;
using KaboomPoolGame.Source.Models.Sprites;

namespace KaboomPoolGame.Source.Controllers.Factories.GameObjectFactories
{
    public class TerrainFactory
    {
        public TerrainType Make(string type)
        {
            switch (type)
            {
                case "Concrete Block":
                    return MakeConcreteBlock();
                case "Concrete Block - Dust":
                    return MakeConcreteBlockDust();
                case "Concrete Block - Ice":
                    return MakeConcreteBlockIce();
                case "Grass":
                    return MakeGrass();
                case "Dust":
                    return MakeDust();
                case "Snow":
                    return MakeSnow();
                case "Ice":
                    return MakeIce();
                case "Water":
                    return MakeWater();
                default:
                    throw new ArgumentOutOfRangeException(
                        message: $"Unknown Terrain type",
                        paramName: nameof(type),
                        actualValue: type);
            }
        }

        public TerrainType MakeConcreteBlock()
        {
            var sprite = new SpriteData("Concrete Block", "Terrain/Concrete Block");
            var model = new TerrainType("Concrete Block", "Concrete Block", sprite);

            model.Drag = 4;
            model.IsDeadly = false;

            return model;
        }

        public TerrainType MakeConcreteBlockDust()
        {
            var sprite = new SpriteData("Concrete Block - Dust", "Terrain/Concrete Block - Dust");
            var model = new TerrainType("Concrete Block - Dust", "Concrete Block - Dust", sprite);

            model.Drag = 5;
            model.IsDeadly = false;

            return model;
        }

        public TerrainType MakeConcreteBlockIce()
        {
            var sprite = new SpriteData("Concrete Block - Ice", "Terrain/Concrete Block - Ice");
            var model = new TerrainType("Concrete Block - Ice", "Concrete Block - Ice", sprite);

            model.Drag = 3;
            model.IsDeadly = false;

            return model;
        }

        public TerrainType MakeGrass()
        {
            var sprite = new SpriteData("Grass", "Terrain/Grass");
            var model = new TerrainType("Grass", "Grass", sprite);

            model.Drag = 7;
            model.IsDeadly = false;

            return model;
        }

        public TerrainType MakeDust()
        {
            var sprite = new SpriteData("Dust", "Terrain/Dust");
            var model = new TerrainType("Dust", "Dust", sprite);

            model.Drag = 5;
            model.IsDeadly = false;

            return model;
        }

        public TerrainType MakeSnow()
        {
            var sprite = new SpriteData("Snow", "Terrain/Snow");
            var model = new TerrainType("Snow", "Snow", sprite);

            model.Drag = 10;
            model.IsDeadly = false;

            return model;
        }

        public TerrainType MakeIce()
        {
            var sprite = new SpriteData("Ice", "Terrain/Ice");
            var model = new TerrainType("Ice", "Ice", sprite);

            model.Drag = 1;
            model.IsDeadly = false;

            return model;
        }

        public TerrainType MakeWater()
        {
            var sprite = new SpriteData("Water", "Terrain/Water");
            var model = new TerrainType(
                terrainName: "Water",
                variation: "Water",
                sprite: sprite,
                layer: Layer.EffectlessTerrain);

            model.Drag = 0;
            model.IsDeadly = true;

            return model;
        }
    }
}
