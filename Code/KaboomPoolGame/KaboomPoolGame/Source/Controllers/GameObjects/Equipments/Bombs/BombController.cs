﻿using KaboomPoolGame.Source.Models.GameObjects.Equipments.Bombs;
using KaboomPoolGame.Source.Views.GameObjects.Equipments.Bombs;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.GameObjects.Equipments.Bombs
{
    public class BombController : GameObjectController<Bomb, BombView>
    {
        #region Properties
        public bool HasExploded => Model.HasExploded;
        #endregion


        #region Constructors
        public BombController(Bomb model, BombView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public override void Update(GameTime gameTime)
        {
            var elapsed = gameTime.ElapsedGameTime
                .TotalSeconds;

            Model.TimeLeft -= elapsed;
            if (Model.TimeLeft < 0)
            {
                Model.HasExploded = true;
            }

            Model.Sprite.Update(gameTime);

            Model.MustRemove =
                Model.MustRemove || HasExploded;
        }
        #endregion
    }
}
