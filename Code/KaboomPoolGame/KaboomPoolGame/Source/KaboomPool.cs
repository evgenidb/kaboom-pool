﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.States;
using KaboomPoolGame.Source.States.Game;
using KaboomPoolGame.Source.States.Menu;
using KaboomPoolGame.Source.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class KaboomPool : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        GameStateManager StateManager
        {
            get { return GameServices.GameStateManager; }
        }

        InputManager Input
        {
            get { return GameServices.Input; }
        }

        public KaboomPool()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            ApplyGameOptions();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // Get Spritesheets
            var spritesheetRepository =
                GameServices.SpritesheetRepository;

            var contentLoader = GameServices.ContentLoader;
            var buttons = contentLoader
                .LoadSpritesheet("Buttons Spritesheet");

            spritesheetRepository.AddSpritesheet(buttons);

            // Levels
            var levelLoader = GameServices.LevelLoader;
            levelLoader.Initialize();
            var difficultyOrder = levelLoader.LoadDifficultyOrder();
            var levelNames = new Dictionary<string, IReadOnlyList<string>>();
            foreach (var difficulty in difficultyOrder)
            {
                levelNames[difficulty] =
                    levelLoader.LoadLevelOrder(difficulty);
            }

            GameServices.LevelRepository.Initialize(
                difficultyOrder: difficultyOrder,
                levelNames: levelNames);

            // States
            var stateManager =
                GameServices.GameStateManager;

            stateManager.AddStates(new GameState[]
            {
                new MainMenuState(),
                new PauseGameMenuState(),
                new PlayState(),
            });
            stateManager.Initialize("Main Menu");

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            GameServices.LevelRepository.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            GameServices.LevelRepository.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Record Input
            Input.RecordInput(gameTime);

            // On/Off Debug Mode
            var debugOptions = GameServices.DebugOptions;
            var debugControls = debugOptions.Controls;
            if (debugControls.ActivateDebugMode.Check(Input))
            {
                debugOptions.IsDebugMode = true;
            }
            else if (debugControls.DeactivateDebugMode.Check(Input))
            {
                debugOptions.IsDebugMode = false;
            }

            // Input for states
            StateManager.HandleInput(Input);

            // Update states
            StateManager.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(
                sortMode: SpriteSortMode.BackToFront);

            StateManager.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }


        #region Public Methods
        public void ApplyGameOptions()
        {
            var options = GameServices.GameOptions;

            Window.Title = options.GameName;
            Window.AllowUserResizing = options.IsResizingAllowed;

            //graphics.PreferredBackBufferWidth = options.GameWidth;
            //graphics.PreferredBackBufferHeight = options.GameHeight;
            graphics.IsFullScreen = options.IsFullScreen;

            // Apply Changes to game
            graphics.ApplyChanges();
        }

        public void SetFPS(int fps)
        {
            if (fps < 1)
            {
                throw new ArgumentOutOfRangeException(
                    message: $"FPS must be a positive integer.",
                    paramName: nameof(fps),
                    actualValue: fps);
            }

            // From:
            // http://geekswithblogs.net/Sharpoverride/archive/2009/08/08/xna-tip-changing-the-frequency-for-the-update-and-draw.aspx
            TargetElapsedTime =
                TimeSpan.FromSeconds(1.0f / fps);
        }

        public void ReloadLevels()
        {
            GameServices.LevelRepository.ReloadAllLevels();
        }
        #endregion
    }
}
