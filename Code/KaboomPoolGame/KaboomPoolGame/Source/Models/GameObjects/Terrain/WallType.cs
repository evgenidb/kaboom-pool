﻿using System;
using System.Collections.Generic;
using System.Linq;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.Sprites;
using KaboomPoolGame.Source.Utils;

namespace KaboomPoolGame.Source.Models.GameObjects.Terrain
{
    public class WallType : FlyweightGameObject, IRenderableFlyweightModel
    {
        #region Properties
        public IDictionary<string, ICollection<SpecificWall>> Data { get; } =
            new Dictionary<string, ICollection<SpecificWall>>();

        public bool IsDestructable { get; set; } = false;

        public Layer Layer { get; } = Layer.Wall;

        public IDictionary<string, ISpriteData> Sprites { get; } =
            new Dictionary<string, ISpriteData>();
        #endregion


        #region Constructors
        public WallType(string wallName, string variation, ISpriteData sprite)
            : this(wallName, new Dictionary<string, ISpriteData> { [variation] = sprite })
        { }

        public WallType(string wallName, IDictionary<string, ISpriteData> sprites)
            : base(wallName)
        {
            foreach (var sprite in sprites)
            {
                AddVariation(
                    variation: sprite.Key,
                    sprite: sprite.Value);
            }

            AddAllowedPublicProperties(new[] {
                nameof(Name),
            });
        }
        #endregion

        #region Public Methods
        public void AddVariation(string variation, ISpriteData sprite)
        {
            Sprites[variation] = sprite;
            Data[variation] = new HashSet<SpecificWall>();
        }

        public void AddData(string variation, SpecificWall wallData)
        {
            if (Data.ContainsKey(variation))
            {
                Data[variation].Add(wallData);
            }
            else
            {
                var exceptionMethod = string.Join(" ",
                    "Missing Variation.",
                    "First add it with",
                    $"{ nameof(AddVariation)} method.");

                throw new ArgumentOutOfRangeException(
                    message: exceptionMethod,
                    paramName: nameof(variation),
                    actualValue: variation);
            }
        }

        public void AddData(SpecificWall wallData)
        {
            var variation = RandomVariation();
            Data[variation].Add(wallData);
        }

        public void AddDataCollection(string variation, IEnumerable<SpecificWall> wallData)
        {
            foreach (var data in wallData)
            {
                AddData(variation, data);
            }
        }

        public void AddDataCollection(IEnumerable<SpecificWall> wallData)
        {
            foreach (var data in wallData)
            {
                AddData(data);
            }
        }

        public bool RemoveData(string variation, SpecificWall wallData)
        {
            return Data[variation].Remove(wallData);
        }

        public bool RemoveVariation(string variation)
        {
            Data.Remove(variation);
            return Sprites.Remove(variation);
        }

        public void Clean()
        {
            foreach (var variation in Data)
            {
                foreach (var wall in variation.Value)
                {
                    if (wall.MustRemove)
                    {
                        RemoveData(variation.Key, wall);
                    }
                }
            }
        }

        public void ClearSpecificData()
        {
            foreach (var item in Data)
            {
                item.Value.Clear();
            }
        }

        public void ClearAllData()
        {
            Data.Clear();
            Sprites.Clear();
        }

        public IEnumerable<string> GetSpriteNames()
        {
            var names = new List<string>();
            foreach (var sprite in Sprites)
            {
                var textureNames = sprite.Value.TextureNames;
                names.AddRange(textureNames);
            }

            return names;
        }
        #endregion

        #region Private Methods
        private string RandomVariation()
        {
            var variation = Sprites.Keys.ToArray();

            return GameServices
                .Utils.ChooseRandom(variation);
        }
        #endregion
    }
}
