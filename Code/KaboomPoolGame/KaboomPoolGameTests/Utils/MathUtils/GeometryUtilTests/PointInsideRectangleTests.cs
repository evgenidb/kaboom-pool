﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace KaboomPoolGameTests.Utils.MathUtils.GeometryUtilTests
{
    [TestClass]
    public class PointInsideRectangleTests
    {
        #region Fields
        private static GeometryUtil geometry;
        #endregion

        #region Properties
        public GeometryUtil Geometry =>
            geometry;
        #endregion

        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            geometry = new GeometryUtil();
            GameServices.Geometry = geometry;
            GameServices.Math = new MathUtil();
        }
        #endregion


        #region Test Methods

        #region Rectangle - non-rotated, center (0, 0)
        [TestMethod]
        public void PointInsideCenterNonRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(0, 0);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideNonRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(1, 2);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopBorderNonRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(1, -2.5f);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideBottomBorderNonRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(1, 2.5f);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopLeftBorderNonRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(-5, -2.5f);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideLeftBorderNonRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(-5, -1);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideRightBorderNonRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(5, -1);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointOutsideLeftSideNonRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(-6, 2);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointOutsideBottomSideNonRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(2, 3);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Rectangle - non-rotated, shifted (23, 45)
        [TestMethod]
        public void PointInsideCenterNonRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(23, 45);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideNonRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(24, 47);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopBorderNonRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(24, 42.5f);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideBottomBorderNonRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(24, 47.5f);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopLeftBorderNonRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(18, 42.5f);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideLeftBorderNonRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(18, 44);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideRightBorderNonRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(28, 44);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointOutsideLeftSideNonRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(17, 47);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointOutsideBottomSideNonRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(25, 48);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = 0;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion
        
        #region Rectangle - 90 degrees rotated, center (0, 0)
        [TestMethod]
        public void PointInsideCenter90DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(0, 0);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInside90DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(1, 2);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopBorder90DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(1, -5);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideBottomBorder90DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(1, 5);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopLeftBorder90DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(-2.5f, -5f);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideLeftBorder90DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(-2.5f, -1);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideRightBorder90DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(2.5f, -1);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointOutsideLeftSide90DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(-3, 2);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointOutsideBottomSide90DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(6, 1);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Rectangle - 90 degrees rotated, shifted (23, 45)
        [TestMethod]
        public void PointInsideCenter90DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(23, 45);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInside90DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(24, 47);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopBorder90DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(24, 40);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideBottomBorder90DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(24, 50);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopLeftBorder90DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(20.5f, 40);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideLeftBorder90DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(20.5f, 44);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideRightBorder90DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(25.5f, 44);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointOutsideLeftSide90DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(20, 47);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointOutsideBottomSide90DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(25, 51);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 2;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Rectangle - 45 degrees rotated, center (0, 0)
        [TestMethod]
        public void PointInsideCenter45DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(0, 0);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInside45DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(1, 2);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopBorder45DegreesRotatedCenterRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: -5 / sqrt2,
                y: -5 / sqrt2);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideBottomBorder45DegreesRotatedCenterRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: 5 / sqrt2,
                y: 5 / sqrt2);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopLeftBorder45DegreesRotatedCenterRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: -15 / (2 * sqrt2),
                y: -5 / (2 * sqrt2));

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideLeftBorder45DegreesRotatedCenterRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: -5 / sqrt2,
                y: 0);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideRightBorder45DegreesRotatedCenterRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: 5 / sqrt2,
                y: 0);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointOutsideLeftSide45DegreesRotatedCenterRectangle()
        {
            // Assign
            var point = new Vector2(
                x: -4,
                y: 0);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointOutsideBottomSide45DegreesRotatedCenterRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: 0,
                y: 4);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Rectangle - 45 degrees rotated, shifted (23, 45)
        [TestMethod]
        public void PointInsideCenter45DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(23, 45);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInside45DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(24, 47);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideTopBorder45DegreesRotatedShiftedRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: 23 - (5 / sqrt2),
                y: 45 - (5 / sqrt2));

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideBottomBorder45DegreesRotatedShiftedRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: 23 + (5 / sqrt2) - 1f,
                y: 45 + (5 / sqrt2) - 1f);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideLeftBorder45DegreesRotatedShiftedRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: 23 - (5 / sqrt2),
                y: 45);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideRightBorder45DegreesRotatedShiftedRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: 23 + (5 / sqrt2),
                y: 45);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointOutsideLeftSide45DegreesRotatedShiftedRectangle()
        {
            // Assign
            var point = new Vector2(
                x: 19,
                y: 45);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void PointOutsideBottomSide45DegreesRotatedShiftedRectangle()
        {
            // Assign
            var sqrt2 = (float)Math.Sqrt(2);
            var point = new Vector2(
                x: 23,
                y: 49);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;
            double rotation = Math.PI / 4;

            // Act
            bool actual =
                Geometry.IsPointInsideRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height,
                    rotation: rotation);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #endregion
    }
}
