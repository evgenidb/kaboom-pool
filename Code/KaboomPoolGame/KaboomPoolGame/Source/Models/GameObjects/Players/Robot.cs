﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.GameObjects;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.GameObjects.Players
{
    public class Robot : GameObject, IRenderableModel, IMovable
    {
        #region Properties
        public Layer Layer { get; } = Layer.Player;

        public ISpriteData Sprite =>
            StateSprites[CurrentState];

        public Vector2 Direction { get; set; } = Vector2.Zero;
        public double MaxSpeed { get; set; } = 0;
        #endregion

        #region Private Properties
        public PlayerState CurrentState { get; set; }

        private Dictionary<PlayerState, ISpriteData> StateSprites { get; } =
            new Dictionary<PlayerState, ISpriteData>();
        #endregion

        #region Constructors
        public Robot(Vector2 position, Vector2 size, PlayerState initialState, IDictionary<PlayerState, ISpriteData> stateSprites, double rotation = 0, Vector2? scale = null)
            :base("Player", position, size, rotation, scale)
        {
            CurrentState = initialState;

            foreach (var state in stateSprites)
            {
                StateSprites.Add(state.Key, state.Value);
            }

            AddAllowedPublicProperties(new[] {
                nameof(Direction),
                nameof(MaxSpeed),
            });
        }
        #endregion

        #region Public Methods
        public IEnumerable<string> GetSpriteNames()
        {
            var names = new List<string>();

            foreach (var sprite in StateSprites)
            {
                var frames = sprite.Value.TextureNames;
                names.AddRange(frames);
            }

            return names;
        }
        #endregion
    }
}
