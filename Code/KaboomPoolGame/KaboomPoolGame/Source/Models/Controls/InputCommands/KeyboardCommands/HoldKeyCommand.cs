﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Enums;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;
using Microsoft.Xna.Framework.Input;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.KeyboardCommands
{
    public class HoldKeyCommand : IInputCommand
    {
        #region Constants
        private const RequiredCommandState RequiredState =
            RequiredCommandState.Hold;

        public const int InfiniteDuration = -1;
        #endregion

        #region Fields
        private double minTime;
        private double duration;
        #endregion

        #region Properties
        private Keys Key { get; }

        private double MinTime
        {
            get { return minTime; }
            set
            {
                if (value >= 0)
                {
                    minTime = value;
                }
                else
                {
                    var exceptionMessage = string.Join(" ",
                        "Minimal hold time",
                        "cannot be negative number.");

                    throw new ArgumentOutOfRangeException(
                        message: exceptionMessage,
                        paramName: nameof(MinTime),
                        actualValue: value);
                }
            }
        }

        private double Duration
        {
            get { return duration; }
            set
            {
                if (value == InfiniteDuration || value > 0)
                {
                    duration = value;
                }
                else
                {
                    var exceptionMessage = string.Join(" ",
                        "Hold duration",
                        "must be positive number or",
                        $"{InfiniteDuration} (unlimited).");

                    throw new ArgumentOutOfRangeException(
                        message: exceptionMessage,
                        paramName: nameof(Duration),
                        actualValue: value);
                }
            }
        }
        private double MaxTime => MinTime + Duration;

        private double HoldTime { get; set; } = 0;
        private double LastHoldTimeCheck { get; set; } = 0;
        #endregion

        #region Constructors
        /// <summary>
        /// Hold Keyboard key command.
        /// </summary>
        /// <param name="key">The Keyboard key to check.</param>
        /// <param name="minTime">
        /// (Measured in seconds.)
        /// Minimal time to hold the key,
        /// after which it check return true.
        /// If minTime is zero, the check will return true
        /// as soon as the key is pressed.
        /// </param>
        /// <param name="allowedHoldDuration">
        /// (Measured in seconds.)
        /// Max duration to hold the key.
        /// After the duration ends, check will return false
        /// until the hold is reset.
        /// </param>
        public HoldKeyCommand(Keys key, double minTime = 0, double allowedHoldDuration = InfiniteDuration)
        {
            Key = key;

            MinTime = minTime;
            Duration = allowedHoldDuration;
        }
        #endregion

        #region Public Methods
        public bool Check(InputManager input, CheckInputArgs args = null)
        {
            if (args == null)
            {
                var exceptionMessage = string.Join(" ",
                    "Input args is a required parameter.");

                throw new ArgumentNullException(
                    message: exceptionMessage,
                    paramName: nameof(args));
            }

            bool isKeyHeld = input.IsKeyHold(Key);
            if (isKeyHeld)
            {
                var holdArgs = (HoldInputArgs)args;

                var elapsedSeconds = holdArgs.GameTime
                    .ElapsedGameTime.TotalSeconds;

                var totalSeconds = holdArgs.GameTime
                    .TotalGameTime.TotalSeconds;

                if (LastHoldTimeCheck + elapsedSeconds < totalSeconds)
                {
                    ResetHoldTime();
                }
                else
                {
                    HoldTime += elapsedSeconds;
                }
                LastHoldTimeCheck = totalSeconds;

                bool isHeldEnough = MinTime <= HoldTime;
                if (isHeldEnough && Duration == InfiniteDuration)
                {
                    return true;
                }
                else if(isHeldEnough && HoldTime <= MaxTime)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                ResetHoldTime();
                return false;
            }
        }

        public bool Check(InputManager input, IEnumerable<CheckInputArgs> argsCollection)
        {
            foreach (var arg in argsCollection)
            {
                if (arg is HoldInputArgs)
                {
                    return Check(input, arg);
                }
            }

            // No HoldInputArgs was passed
            var exceptionMessage = string.Join(" ",
                $"{nameof(HoldInputArgs)} is required",
                "but is not found in the args collection.");

            throw new ArgumentException(
                message: exceptionMessage,
                paramName: nameof(argsCollection));
        }

        public override string ToString()
        {
            return string.Join(" ",
                $"{RequiredState.ToString()} {Key.ToString()}",
                $"between {MinTime} and {MaxTime} seconds");
        }
        #endregion

        #region Private Methods
        private void ResetHoldTime()
        {
            HoldTime = 0;
        }
        #endregion
    }
}
