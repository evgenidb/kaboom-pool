﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Enums;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;
using Microsoft.Xna.Framework.Input;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.KeyboardCommands
{
    public class ComboKeysCommand : IInputCommand
    {
        #region Properties
        private IEnumerable<Keys> Keys { get; }
        private bool IsStrict { get; }
        private bool SingleCheck { get; }

        private bool IsComboPressedBefore { get; set; }
        #endregion

        #region Constructors
        public ComboKeysCommand(bool isStrict, bool singleCheck, IEnumerable<Keys> keys)
        {
            Keys = keys;
            IsStrict = isStrict;
            SingleCheck = singleCheck;
        }

        public ComboKeysCommand(bool isStrict, bool singleCheck, params Keys[] keysArray)
            : this(keys: keysArray, isStrict: isStrict, singleCheck: singleCheck)
        { }
        #endregion

        #region Public Methods
        public bool Check(InputManager input, CheckInputArgs args = null)
        {
            var isComboPressed =
                    input.IsComboKeysPressed(Keys, IsStrict);

            if (SingleCheck)
            {
                if (isComboPressed)
                {
                    if (IsComboPressedBefore)
                    {
                        return false;
                    }
                    else
                    {
                        IsComboPressedBefore = true;
                        return true;
                    }
                }
                else
                {
                    IsComboPressedBefore = false;
                    return false;
                }
            }
            else
            {
                return isComboPressed;
            }
        }

        public bool Check(InputManager input, IEnumerable<CheckInputArgs> argsCollection)
        {
            return Check(input);
        }

        public override string ToString()
        {
            var holdKeys = string.Join(", ", Keys);
            var strict = IsStrict ? " only" : "";
            return $"{RequiredCommandState.Hold}{strict} {holdKeys}";
        }
        #endregion
    }
}
