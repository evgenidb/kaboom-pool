﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Models.GameObjects.Obstacles;
using KaboomPoolGame.Source.Views.GameObjects.Obstacles;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.GameObjects.Obstacles
{
    public class ObstacleController : GameObjectController<Obstacle, ObstacleView>
    {
        #region Constructors
        public ObstacleController(Obstacle model, ObstacleView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public override void Update(GameTime gameTime)
        {
            Model.Sprite.Update(gameTime);
        }

        public void ApplyForce(Vector2 force, string source)
        {
            if (Model.DestructableBy.Contains(source))
            {
                var forceMagnitude = force.Length();

                if (forceMagnitude >= Model.RequiredMinDestructionForce)
                {
                    Model.MustRemove =
                        Model.MustRemove || true;
                }
            }
        }
        #endregion
    }
}
