﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.Enums
{
    public enum MouseButton
    {
        Left,
        Middle,
        Right,
        X1,
        X2,
    }
}
