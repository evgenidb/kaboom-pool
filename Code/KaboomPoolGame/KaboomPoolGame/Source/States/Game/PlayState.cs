﻿using System;
using KaboomPoolGame.Source.Controllers;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.States.Game
{
    public class PlayState : GameState
    {
        #region Properties
        public LevelController LevelController { get; set; }
        #endregion

        #region Constructors
        public PlayState()
            : base("Play State")
        { }
        #endregion

        #region Public Methods
        public override void Enter()
        {
            ApplyOptions();
            LoadContent();
        }

        public override void Enter(GameState oldState)
        {
            ApplyOptions();
            if (oldState.Name != "Pause Menu")
            {
                LoadContent();
            }
        }

        public override void Leave()
        {
            UnloadContent();
        }

        public override void Leave(GameState newState)
        {
            if (newState.Name != "Pause Menu")
            {
                UnloadContent();
            }
        }

        public override void LoadContent()
        {
            LevelController.LoadContent();
        }

        public override void UnloadContent()
        {
            LevelController.UnloadContent();
        }

        public override void HandleInput(InputManager input)
        {
            var controls = GameServices.PlayOptions.Controls;
            if (controls.PauseMenu.Check(input))
            {
                ChooseAction("Pause Menu");
            }

            // Level Input (Player movement, etc.)
            LevelController.HandleInput(input);


            // Debug Controls
            if (GameServices.DebugOptions.IsDebugMode)
            {
                var debugControls =
                    GameServices.DebugOptions.Controls;

                if (debugControls.WinLevel.Check(input))
                {
                    ChooseAction("Win Level");
                }
                else if (debugControls.LoseLevel.Check(input))
                {
                    ChooseAction("Lose Level");
                }
                else if (debugControls.ReloadLevelData.Check(input))
                {
                    ChooseAction("Reload Current Level Data");
                }
                else if (debugControls.ReloadAllLevelsData.Check(input))
                {
                    ChooseAction("Reload All Levels Data");
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            LevelController.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            LevelController.Draw(spriteBatch);
        }

        public void SetLevel(LevelController level)
        {
            LevelController = level;
            LoadContent();
        }

        public void Restart()
        {
            LevelController.Restart();
        }
        #endregion

        #region Protected Methods
        protected override void InitializeImplementation()
        {
            LevelController.Initialize();
        }

        protected override void ChooseAction(string actionName)
        {
            if (actionName == "Pause Menu")
            {
                GameServices.GameStateManager
                    .SwitchState("Pause Game Menu");
            }
            else if (actionName == "Win Level")
            {
                // TODO: Should go to Win Level State instead
                var nextLevelData =
                    GameServices.LevelRepository.NextLevel();

                var nextLevel = new LevelController(
                    model: nextLevelData.MakeLevel(),
                    view: new LevelView());

                SetLevel(nextLevel);
            }
            else if (actionName == "Lose Level")
            {
                // TODO: Should go to Lose Level State instead
                LevelController.Restart();
            }
            else if (actionName == "Reload Current Level Data")
            {
                var levelRepository =
                    GameServices.LevelRepository;

                levelRepository.ReloadLevel(
                    difficulty: levelRepository.CurrentDifficulty,
                    name: levelRepository.CurrentLevelName);

                var reloadedLevelData = GameServices
                    .LevelRepository.CurrentLevel;

                var reloadedLevel = new LevelController(
                    model: reloadedLevelData.MakeLevel(),
                    view: new LevelView());

                SetLevel(reloadedLevel);
            }
            else if (actionName == "Reload All Levels Data")
            {
                GameServices.Game.ReloadLevels();

                var reloadedLevelData = GameServices
                    .LevelRepository.CurrentLevel;

                var reloadedLevel = new LevelController(
                    model: reloadedLevelData.MakeLevel(),
                    view: new LevelView());

                SetLevel(reloadedLevel);
            }
        }
        #endregion

        #region Private Methods
        private void ApplyOptions()
        {
            var options = GameServices.PlayOptions;
            var game = GameServices.Game;

            game.IsMouseVisible =
                options.Controls.IsMouseVisible;
            
            game.SetFPS(fps: options.FPS);
        }
        #endregion
    }
}
