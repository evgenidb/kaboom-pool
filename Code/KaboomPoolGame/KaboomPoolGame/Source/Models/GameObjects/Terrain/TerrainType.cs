﻿using System;
using System.Collections.Generic;
using System.Linq;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.Sprites;
using KaboomPoolGame.Source.Utils;

namespace KaboomPoolGame.Source.Models.GameObjects.Terrain
{
    public class TerrainType : FlyweightGameObject, IRenderableFlyweightModel
    {
        #region Properties
        public IDictionary<string, ICollection<SpecificTerrain>> Data { get; } =
            new Dictionary<string, ICollection<SpecificTerrain>>();
        
        public bool IsDeadly { get; set; } = false;
        public double Drag { get; set; } = 0;

        public Layer Layer { get; } = Layer.Ground;

        public IDictionary<string, ISpriteData> Sprites { get; } =
            new Dictionary<string, ISpriteData>();
        #endregion


        #region Constructors
        public TerrainType(string terrainName, string variation, ISpriteData sprite, Layer layer = Layer.Ground)
            : this(terrainName, new Dictionary<string, ISpriteData> { [variation] = sprite }, layer)
        { }

        public TerrainType(string terrainName, IDictionary<string, ISpriteData> sprites, Layer layer = Layer.Ground)
            : base(terrainName)
        {
            Layer = layer;

            foreach (var sprite in sprites)
            {
                AddVariation(
                    variation: sprite.Key,
                    sprite: sprite.Value);
            }

            AddAllowedPublicProperties(new[] {
                nameof(Name),
                nameof(IsDeadly),
                nameof(Drag),
            });
        }
        #endregion

        #region Public Methods
        public void AddVariation(string variation, ISpriteData sprite)
        {
            Sprites[variation] = sprite;
            Data[variation] = new HashSet<SpecificTerrain>();
        }

        public void AddData(string variation, SpecificTerrain terrainData)
        {
            if (Data.ContainsKey(variation))
            {
                Data[variation].Add(terrainData);
            }
            else
            {
                var exceptionMethod = string.Join(" ",
                    "Missing Variation.",
                    "First add it with",
                    $"{ nameof(AddVariation)} method.");

                throw new ArgumentOutOfRangeException(
                    message: exceptionMethod,
                    paramName: nameof(variation),
                    actualValue: variation);
            }
        }

        public void AddData(SpecificTerrain terrainData)
        {
            var variation = RandomVariation();
            Data[variation].Add(terrainData);
        }

        public void AddDataCollection(string variation, IEnumerable<SpecificTerrain> terrainData)
        {
            foreach (var data in terrainData)
            {
                AddData(variation, data);
            }
        }

        public void AddDataCollection(IEnumerable<SpecificTerrain> terrainData)
        {
            foreach (var data in terrainData)
            {
                AddData(data);
            }
        }

        public void Clean()
        {
            foreach (var variation in Data)
            {
                foreach (var terrain in variation.Value)
                {
                    if (terrain.MustRemove)
                    {
                        RemoveData(variation.Key, terrain);
                    }
                }
            }
        }

        public bool RemoveData(string variation, SpecificTerrain terrainData)
        {
            return Data[variation].Remove(terrainData);
        }

        public bool RemoveVariation(string variation, SpecificTerrain terrainData)
        {
            Data.Remove(variation);
            return Sprites.Remove(variation);
        }

        public void ClearSpecificData()
        {
            foreach (var item in Data)
            {
                item.Value.Clear();
            }
        }

        public void ClearAllData()
        {
            Data.Clear();
            Sprites.Clear();
        }

        public IEnumerable<string> GetSpriteNames()
        {
            var names = new List<string>();
            foreach (var sprite in Sprites)
            {
                var textureNames = sprite.Value.TextureNames;
                names.AddRange(textureNames);
            }

            return names;
        }
        #endregion

        #region Private Methods
        private string RandomVariation()
        {
            var variation = Sprites.Keys.ToArray();

            return GameServices
                .Utils.ChooseRandom(variation);
        }
        #endregion
    }
}