﻿using System;
using KaboomPoolGame.Source.Models.GameObjects.Balls;
using KaboomPoolGame.Source.Models.GameObjects.Obstacles;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.Factories.GameObjectFactories
{
    public class ObjectFactory
    {
        public Ball MakeBall()
        {
            var sprite = new SpriteData("Ball", "Objects/Ball");

            var model = new Ball(
                position: Vector2.Zero,
                size: new Vector2(20, 20),
                sprite: sprite);

            model.MaxSpeed = 100;

            return model;
        }

        public Obstacle MakeObstacle(string type)
        {
            switch (type)
            {
                case "Pylon":
                    return MakePylon();
                case "Box":
                    return MakeBox();
                case "Glass Wall":
                    return MakeGlassWall();
                default:
                    throw new ArgumentOutOfRangeException(
                        message: $"Unknown Obstacle type",
                        paramName: nameof(type),
                        actualValue: type);
            }
        }

        public Obstacle MakePylon()
        {
            var sprite = new SpriteData("Pylon", "Objects/Obstacle - Pylon");

            var model = new Obstacle(
                name: "Pylon",
                position: Vector2.Zero,
                size: new Vector2(20, 20),
                sprite: sprite);

            return model;
        }

        public Obstacle MakeBox()
        {
            var sprite = new SpriteData("Box", "Objects/Box");

            var model = new Obstacle(
                name: "Box",
                position: Vector2.Zero,
                size: new Vector2(20, 20),
                sprite: sprite);

            model.DestructableBy.Add("Bomb");

            model.RequiredMinDestructionForce = 1.5;

            return model;
        }

        public Obstacle MakeGlassWall()
        {
            var sprite = new SpriteData("Glass Wall", "Terrain/Glass Wall");

            var model = new Obstacle(
                name: "Glass Wall",
                position: Vector2.Zero,
                size: new Vector2(20, 20),
                sprite: sprite);

            model.DestructableBy.Add("Bomb");
            model.DestructableBy.Add("Ball");

            model.RequiredMinDestructionForce = 0.1;

            return model;
        }
    }
}
