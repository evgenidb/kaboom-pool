﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Models.Structs;
using KaboomPoolGame.Source.Utils.FileReaderWriters.Interfaces;
using Microsoft.Xna.Framework;
using Newtonsoft.Json.Linq;

namespace KaboomPoolGame.Source.Utils.FileReaderWriters
{
    public class JsonFileSpritesheetReader : IFileReader<SpritesheetInfo>
    {
        #region Public Methods
        public SpritesheetInfo Read(string path)
        {
            var json = GameServices.DataLoader
                .LoadJson(path);

            var spritesheet = Parse(json);
            return spritesheet;
        }
        #endregion


        #region Private Methods
        private SpritesheetInfo Parse(JObject json)
        {
            // Extract Sheet Image
            string sheetName = (string)json["source"];

            // Extract Sheet Image
            string category = (string)json["category"];

            // Extract Sprites Location
            Dictionary<string, Rectangle> sprites =
                new Dictionary<string, Rectangle>();

            var imagesToken = json["images"];
            var spritesData = imagesToken.ToObject<
                Dictionary<
                    string,
                    Dictionary<string, int>>>();

            foreach (var sprite in spritesData)
            {
                var imagePath = sprite.Key;
                var rectData = sprite.Value;
                sprites[imagePath] = new Rectangle(
                    x: rectData["x"],
                    y: rectData["y"],
                    width: rectData["width"],
                    height: rectData["height"]);
            }

            // Save data
            var spritesheet = new SpritesheetInfo(
                category: category,
                spritesheetPath: sheetName,
                sprites: sprites);

            return spritesheet;
        }
        #endregion
    }
}
