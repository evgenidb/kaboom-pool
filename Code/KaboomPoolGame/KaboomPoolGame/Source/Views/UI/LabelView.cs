﻿using KaboomPoolGame.Source.Models.UI;
using KaboomPoolGame.Source.Utils.Loaders;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Views.UI
{
    public class LabelView : UIView<LabelModel>, IView<LabelModel>
    {
        #region Properties
        private Texture2D Background { get; set; }
        private SpriteFont Font { get; set; }
        #endregion

        #region Constructors
        public LabelView() : base() { }
        #endregion

        #region Public Methods
        protected override void LoadContentImplementation(ContentLoader contentLoader)
        {
            Background = contentLoader.LoadImage(Model.BackgroundName);
            Font = contentLoader.LoadFont(Model.FontName);

            Model.Dimensions =
                Background.Bounds.Size.ToVector2();
        }

        protected override void UnloadContentImplementation()
        {
            Background = null;
            Font = null;

            Model.Dimensions = Vector2.Zero;
        }

        protected override void DrawImplementation(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                texture: Background,
                position: Model.Position,
                origin: Model.Center);

            spriteBatch.DrawString(
                spriteFont: Font,
                text: Model.Text,
                position: Model.Position,
                origin: Model.Center,
                color: Model.TextColor,
                rotation: 0,
                scale: 1,
                effects: SpriteEffects.None,
                layerDepth: 0);
        }
        #endregion
    }
}
