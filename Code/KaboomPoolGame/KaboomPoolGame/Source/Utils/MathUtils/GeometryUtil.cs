﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.GameObjects;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Utils.MathUtils
{
    struct BoundingBox
    {
        public Vector2 TopLeft;
        public Vector2 BottomRight;

        public BoundingBox(Vector2 topLeft, Vector2 bottomRight)
        {
            TopLeft = topLeft;
            BottomRight = bottomRight;
        }

        public bool IntersectsWith(BoundingBox other)
        {
            var horizontallyInside =
                TopLeft.X <= other.BottomRight.X &&
                BottomRight.X >= other.TopLeft.X;

            var verticallyInside =
                TopLeft.Y <= other.BottomRight.Y &&
                BottomRight.Y >= other.TopLeft.Y;

            return
                horizontallyInside &&
                verticallyInside;
        }
    }

    public class GeometryUtil
    {
        public double DegreeToRadian(double degrees)
        {
            return (Math.PI / 180) * degrees;
        }

        public Vector2 RotatePoint(Vector2 point, Vector2 origin, double angle)
        {
            var sinAngle = Math.Sin(angle);
            var cosAngle = Math.Cos(angle);

            var translation = point - origin;

            var rotatedX =
                (translation.X * cosAngle -
                translation.Y * sinAngle);

            var rotatedY =
                (translation.X * sinAngle +
                translation.Y * cosAngle);

            var rotatedPoint = new Vector2(
                x: (float)rotatedX,
                y: (float)rotatedY);
            
            var resultPoint =
                rotatedPoint + origin;

            return resultPoint;
        }

        public bool IsPointInsideCircle(Vector2 point, Vector2 circleCenter, double radius)
        {
            var distanceSquared = Vector2.DistanceSquared(point, circleCenter);
            var radiusSquared = radius * radius;

            return distanceSquared <= radiusSquared;
        }

        public bool IsPointInsideAABBRectangle(Vector2 point, Vector2 rectangleCenter, double width, double height)
        {
            var math = GameServices.Math;

            var halfWidth = width / 2;
            var halfHeight = height / 2;

            var isPointInside =
                math.IsInRange(
                    min: rectangleCenter.X - halfWidth,
                    value: point.X,
                    max: rectangleCenter.X + halfWidth)
                &&
                math.IsInRange(
                    min: rectangleCenter.Y - halfHeight,
                    value: point.Y,
                    max: rectangleCenter.Y + halfHeight);

            return isPointInside;
        }

        public bool IsPointInsideRectangle(Vector2 point, Vector2 rectangleCenter, double width, double height, double rotation)
        {
            var rotatedPoint = RotatePoint(
                point: point, 
                origin: rectangleCenter, 
                angle: -rotation);

            return IsPointInsideAABBRectangle(
                point: rotatedPoint,
                rectangleCenter: rectangleCenter,
                width: width,
                height: height);
        }

        public bool AreLinesParallel(Vector2 line1A, Vector2 line1B, Vector2 line2A, Vector2 line2B)
        {
            if (line1A == line1B)
            {
                throw new InvalidOperationException(
                    message: "The points for Line 1 must be different to create a line.");
            }
            if (line2A == line2B)
            {
                throw new InvalidOperationException(
                    message: "The points for Line 2 must be different to create a line.");
            }

            var line1 = line1A - line1B;
            var line2 = line2A - line2B;
            if (line1.Y == 0 && line2.Y == 0)
            {
                return true;
            }

            var line1Angle = line1.X / line1.Y;
            var line2Angle = line2.X / line2.Y;

            return
                Math.Abs(line1Angle) ==
                Math.Abs(line2Angle);
        }

        public double AngleBetweenVectors(Vector2 v1, Vector2 v2)
        {
            if (v1 == Vector2.Zero || v2 == Vector2.Zero)
            {
                throw new InvalidOperationException(
                    message: "Both vectors must be non-zero.");
            }

            var v1Normalized = Vector2.Normalize(v1);
            var v2Normalized = Vector2.Normalize(v2);

            var dotProduct =
                Vector2.Dot(v1Normalized, v2Normalized);

            return Math.Acos(dotProduct);
        }

        public bool DoesSegmentIntersectsLine(Vector2 segmentA, Vector2 segmentB, Vector2 lineA, Vector2 lineB)
        {
            const float epsilon = 0.00001f;

            var translatedSegment = new[]
            {
                segmentA - lineA,
                segmentB - lineA,
            };

            var translatedLine = new[]
            {
                Vector2.Zero,
                lineB - lineA,
            };

            var lineAngle = AngleBetweenVectors(
                v1: Vector2.UnitX,
                v2: translatedLine[1]);
            
            var rotateLinePoint = RotatePoint(
                point: translatedSegment[0],
                origin: Vector2.Zero,
                angle: lineAngle);

            double angle;
            if (Math.Abs(rotateLinePoint.Y) < epsilon)
            {
                angle = lineAngle;
            }
            else
            {
                angle = -lineAngle;
            }

            var rotatedSegmentPoints = new[]
            {
                RotatePoint(
                    point: translatedSegment[0],
                    origin: Vector2.Zero,
                    angle: angle),
                RotatePoint(
                    point: translatedSegment[1],
                    origin: Vector2.Zero,
                    angle: angle),
            };

            bool areSegmentPointsOnDifferentSidesOfAxisX =
                Math.Sign(rotatedSegmentPoints[0].Y) ==
                -1 * Math.Sign(rotatedSegmentPoints[1].Y);

            bool isPointAOnAxisX =
                Math.Abs(rotatedSegmentPoints[0].Y) < epsilon;
            bool isPointBOnAxisX =
                Math.Abs(rotatedSegmentPoints[1].Y) < epsilon;

            return areSegmentPointsOnDifferentSidesOfAxisX ||
                isPointAOnAxisX || isPointBOnAxisX;
        }

        private BoundingBox SegmentBoundingBox(Vector2 pointA, Vector2 pointB)
        {
            var topLeft = new Vector2(
                x: Math.Min(pointA.X, pointB.X),
                y: Math.Min(pointA.Y, pointB.Y));

            var bottomRight = new Vector2(
                x: Math.Max(pointA.X, pointB.X),
                y: Math.Max(pointA.Y, pointB.Y));

            var boundingBox = new BoundingBox(
                topLeft, bottomRight);

            return boundingBox;
        }

        public bool DoSegmentsIntersect(Vector2 segment1A, Vector2 segment1B, Vector2 segment2A, Vector2 segment2B)
        {
            if (segment1A == segment1B || segment2A ==segment2B)
            {
                throw new InvalidOperationException(
                    message: "Segments must be of non-zero length.");
            }

            // BoundingBoxIntersection
            var segment1BoundingBox =
                SegmentBoundingBox(
                    pointA: segment1A,
                    pointB: segment1B);

            var segment2BoundingBox =
                SegmentBoundingBox(
                    pointA: segment2A,
                    pointB: segment2B);

            var doBoxesIntersect =
                segment1BoundingBox
                .IntersectsWith(segment2BoundingBox);

            if (!doBoxesIntersect)
            {
                return false;
            }

            // If Intersects
            var segment1Result =
                DoesSegmentIntersectsLine(
                    segmentA: segment1A, segmentB: segment1B,
                    lineA: segment2A, lineB: segment2B);

            var segment2Result =
                DoesSegmentIntersectsLine(
                    segmentA: segment2A, segmentB: segment2B,
                    lineA: segment1A, lineB: segment1B);

            var result =
                segment1Result && segment2Result;
                
            return result;
        }
    }
}
