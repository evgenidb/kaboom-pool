﻿using System.Collections.Generic;
using System.Linq;
using KaboomPoolGame.Source.Models;
using Microsoft.Xna.Framework;
using Newtonsoft.Json.Linq;

namespace KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.Parsers
{
    public class JsonLevelParser
    {
        #region Public Methods
        public LevelData Parse(JObject json, string difficulty, string name)
        {
            var level = new LevelData(difficulty, name);

            ParseMeta(json, level);
            ParsePlayer(json, level);
            ParseBalls(json, level);
            ParseTargets(json, level);
            ParseTerrain(json, level);
            ParseWalls(json, level);
            ParseObjects(json, level);

            return level;
        }
        #endregion


        #region Private Methods
        private void ParseMeta(JObject json, LevelData level)
        {
            // Bomb Count
            level.InitialBombCount =
                (int)json["meta"]["bombs-count"];

            // Cell size
            var jsonCellSize = json["meta"]["cell-size"];

            level.CellSize = new Point(
                x: (int)jsonCellSize["width"],
                y: (int)jsonCellSize["height"]);

            // Map Size
            var jsonMapSize = json["meta"]["map-size"];

            level.MapSize = new Point(
                x: (int)jsonMapSize["width"],
                y: (int)jsonMapSize["height"]);
            
        }

        private void ParsePlayer(JObject json, LevelData level)
        {
            level.Player = ParseLocation(
                json["map"]["Player"]);
        }

        private void ParseBalls(JObject json, LevelData level)
        {
            var jsonBalls = json["map"]["Ball"].AsJEnumerable();

            foreach (var jsonLocation in jsonBalls)
            {
                var ball = ParseLocation(jsonLocation);

                level.Balls.Add(ball);
            }
        }

        private void ParseTargets(JObject json, LevelData level)
        {
            var jsonTargets = json["map"]["Target"].AsJEnumerable();

            foreach (var jsonLocation in jsonTargets)
            {
                var target = ParseLocation(jsonLocation);

                level.Targets.Add(target);
            }
        }

        private void ParseTerrain(JObject json, LevelData level)
        {
            var jsonTerrains = json["map"]["Terrain"].Value<JObject>();

            IEnumerable<string> terrainNames =
                jsonTerrains.Properties().Select(p => p.Name);

            foreach (string terrainName in terrainNames)
            {
                var terrainLocations = jsonTerrains[terrainName].AsJEnumerable();

                var locations = new List<CellLocationData>();
                foreach (var jsonLocation in terrainLocations)
                {
                    var location =
                        ParseTileLocation(jsonLocation);

                    locations.Add(location);
                }

                level.Terrain.Add(terrainName, locations);
            }
        }

        private void ParseWalls(JObject json, LevelData level)
        {
            var jsonWalls = json["map"]["Walls"].Value<JObject>();

            IEnumerable<string> wallsNames =
                jsonWalls.Properties().Select(p => p.Name);

            foreach (string wallName in wallsNames)
            {
                var wallLocations = jsonWalls[wallName].AsJEnumerable();

                var locations = new List<CellLocationData>();
                foreach (var jsonLocation in wallLocations)
                {
                    var location =
                        ParseLocation(jsonLocation);

                    locations.Add(location);
                }

                level.Walls.Add(wallName, locations);
            }
        }

        private void ParseObjects(JObject json, LevelData level)
        {
            var jsonObjects = json["map"]["Objects"].Value<JObject>();

            IEnumerable<string> objectsNames =
                jsonObjects.Properties().Select(p => p.Name);

            foreach (string objectName in objectsNames)
            {
                var objectLocations = jsonObjects[objectName].AsJEnumerable();

                var locations = new List<CellLocationData>();
                foreach (var jsonLocation in objectLocations)
                {
                    var location =
                        ParseLocation(jsonLocation);

                    locations.Add(location);
                }

                level.Obstacles.Add(objectName, locations);
            }
        }
        #endregion

        #region Helper Methods
        private CellLocationData ParseTileLocation(JToken jsonLocation)
        {
            int cellX = (jsonLocation["x"] ?? 0).Value<int>();
            int cellY = (jsonLocation["y"] ?? 0).Value<int>();

            // Set values
            return new CellLocationData(
                cellX: cellX,
                cellY: cellY);
        }

        private CellLocationData ParseLocation(JToken jsonLocation)
        {
            int cellX = (jsonLocation["x"] ?? 0).Value<int>();
            int cellY = (jsonLocation["y"] ?? 0).Value<int>();
            double offsetX = (jsonLocation["offset-x"] ?? 0.0).Value<double>();
            double offsetY = (jsonLocation["offset-y"] ?? 0.0).Value<double>();
            double rotation = (jsonLocation["rotation"] ?? 0.0).Value<double>();

            double radians = GameServices.Geometry
                .DegreeToRadian(rotation);

            // Set values
            var location = new CellLocationData(
                cellX: cellX,
                cellY: cellY,
                offsetX: offsetX,
                offsetY: offsetY,
                rotation: radians);
            
            return location;
        }
        #endregion
    }
}
