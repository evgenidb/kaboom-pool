﻿using System.IO;
using KaboomPoolGame.Source.Utils.FileReaderWriters.Interfaces;
using Newtonsoft.Json.Linq;

namespace KaboomPoolGame.Source.Utils.FileReaderWriters
{
    public class JsonFileReader : IFileReader<JObject>
    {
        #region Constants
        private const string Extension = "json";
        #endregion


        #region Public Methods
        public JObject Read(string path)
        {
            path = FixExtension(path);

            JObject json = null;

            using (var file = new StreamReader(path))
            {
                var text = file.ReadToEnd();
                json = JObject.Parse(text);
            }

            return json;
        }
        #endregion


        #region Private Methods
        private static string FixExtension(string path)
        {
            if (!path.EndsWith($".{Extension}"))
            {
                path += $".{Extension}";
            }

            return path;
        }
        #endregion
    }
}
