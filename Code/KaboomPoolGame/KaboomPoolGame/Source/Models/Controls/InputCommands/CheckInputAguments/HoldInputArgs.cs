﻿using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments
{
    public class HoldInputArgs : CheckInputArgs
    {
        #region Properties
        public GameTime GameTime { get; }
        #endregion

        #region Constructors
        public HoldInputArgs(GameTime gameTime)
        {
            GameTime = gameTime;
        }
        #endregion
    }
}
