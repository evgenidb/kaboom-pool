﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Models;
using KaboomPoolGame.Source.Models.GameObjects.Terrain;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.TestLevels.Easy
{
    public class EasyTestLevel_02 : ITestLevel
    {
        #region Properties
        public string Name { get; } = "Easy Level 02";

        public LevelData Data { get; }
        #endregion

        #region Constructors
        public EasyTestLevel_02()
        {
            //var level = new LevelData(
            //    difficulty: "Easy",
            //    name: Name);

            //// Level Meta
            //level.InitialBombCount = 3;
            //level.MapSize = new Point(20, 20);
            //level.CellSize = new Point(20, 20);

            //// Grass
            //var grass = new TerrainType("Grass", "Terrain/Grass");
            //grass.AddLocation("Terrain/Grass", new CellLocationData(0, 0).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(1, 1).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(1, 0).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(0, 1).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(10, 10).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(5, 10).ToVector2());
            //grass.AddLocation("Terrain/Grass", new CellLocationData(5, 11).ToVector2());

            //// Water
            //var water = new TerrainType("Water", "Terrain/Water");
            //water.AddLocation("Terrain/Water", new CellLocationData(1, 2).ToVector2());
            //water.AddLocation("Terrain/Water", new CellLocationData(1, 3).ToVector2());
            //water.AddLocation("Terrain/Water", new CellLocationData(1, 4).ToVector2());

            //// Concrete Walls
            //var concreteWalls = new TerrainType("Concrete Wall", new[]
            //{
            //    "Terrain/Wall - Concrete - No Steel Bars",
            //    "Terrain/Wall - Concrete - Central Steel Bar",
            //    "Terrain/Wall - Concrete - Horizontal Steel Bars",
            //    "Terrain/Wall - Concrete - Vertical Steel Bars",
            //});
            //concreteWalls.AddLocations("Terrain/Wall - Concrete - No Steel Bars", new[]
            //{
            //    new CellLocationData(7, 8).ToVector2(),
            //    new CellLocationData(10, 3).ToVector2(),
            //});
            //concreteWalls.AddLocations("Terrain/Wall - Concrete - Horizontal Steel Bars", new[]
            //{
            //    new CellLocationData(8, 8).ToVector2(),
            //    new CellLocationData(2, 7).ToVector2(),
            //});

            //// Brick Walls
            //var brickWalls = new TerrainType("Brick Wall", "Terrain/Wall - Brick");
            //brickWalls.AddLocation("Terrain/Wall - Brick", new CellLocationData(14, 14).ToVector2());
            //brickWalls.AddLocation("Terrain/Wall - Brick", new CellLocationData(14, 15).ToVector2());
            //brickWalls.AddLocation("Terrain/Wall - Brick", new CellLocationData(14, 16).ToVector2());
            //brickWalls.AddLocation("Terrain/Wall - Brick", new CellLocationData(15, 14).ToVector2());
            //brickWalls.AddLocation("Terrain/Wall - Brick", new CellLocationData(15, 15).ToVector2());
            //brickWalls.AddLocation("Terrain/Wall - Brick", new CellLocationData(15, 16).ToVector2());
            //brickWalls.AddLocation("Terrain/Wall - Brick", new CellLocationData(16, 14).ToVector2());
            //brickWalls.AddLocation("Terrain/Wall - Brick", new CellLocationData(16, 15).ToVector2());
            //brickWalls.AddLocation("Terrain/Wall - Brick", new CellLocationData(16, 16).ToVector2());

            //// Add Terrain Types
            ////level.Terrain.AddRange(new List<TerrainType>
            ////{
            ////    grass,
            ////    water,
            ////    concreteWalls,
            ////    brickWalls,
            ////});

            //// Add player
            //level.Player = new CellLocationData(5, 4);

            //// Set Data
            //Data = level;
        }
        #endregion
    }
}
