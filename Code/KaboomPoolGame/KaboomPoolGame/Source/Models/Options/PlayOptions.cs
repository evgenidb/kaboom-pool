﻿using KaboomPoolGame.Source.Models.Controls;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.Options
{
    public class PlayOptions
    {
        public int FPS { get; set; } = 60;
        public Vector2 CellSize { get; set; }

        public PlayControls Controls { get; set; }
    }
}
