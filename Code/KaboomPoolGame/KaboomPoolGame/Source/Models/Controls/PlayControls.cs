﻿using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;
using KaboomPoolGame.Source.Models.Controls.InputCommands.SpecialInputCommands;

namespace KaboomPoolGame.Source.Models.Controls
{
    public class PlayControls
    {
        public bool IsMouseVisible { get; set; } = true;

        public IInputCommand MoveUp { get; set; } = new NullInputCommand();
        public IInputCommand MoveDown { get; set; } = new NullInputCommand();
        public IInputCommand MoveLeft { get; set; } = new NullInputCommand();
        public IInputCommand MoveRight { get; set; } = new NullInputCommand();

        public IInputCommand PlaceBomb { get; set; } = new NullInputCommand();

        public IInputCommand PauseMenu { get; set; } = new NullInputCommand();
    }
}
