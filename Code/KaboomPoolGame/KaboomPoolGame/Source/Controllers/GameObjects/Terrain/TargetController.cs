﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Models.GameObjects.Terrain;
using KaboomPoolGame.Source.Views.GameObjects.Targets;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.GameObjects.Terrain
{
    public class TargetController : GameObjectController<Target, TargetView>
    {
        #region Constructors
        public TargetController(Target model, TargetView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public override void Update(GameTime gameTime)
        {
            // Update Sprite
            Model.Sprite.Update(gameTime);
        }
        #endregion
    }
}
