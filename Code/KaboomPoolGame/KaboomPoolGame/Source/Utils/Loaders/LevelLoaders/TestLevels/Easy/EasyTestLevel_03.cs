﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Models;

namespace KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.TestLevels.Easy
{
    public class EasyTestLevel_03 : ITestLevel
    {
        #region Properties
        public string Name { get; } = "Easy Level 03";

        public LevelData Data { get; }
        #endregion

        #region Constructors
        public EasyTestLevel_03()
        {
            var level = new LevelData(
                difficulty: "Easy",
                name: Name);

            // Level Initialization Code.
            // Do it like in EasyTestLevel_01

            Data = level;
        }
        #endregion
    }
}
