﻿using KaboomPoolGame.Source.Models.GameObjects.Balls;
using KaboomPoolGame.Source.Views.GameObjects.Balls;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.GameObjects.Balls
{
    public class BallController : GameObjectController<Ball, BallView>
    {
        #region Constructors
        public BallController(Ball model, BallView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public override void Update(GameTime gameTime)
        {
            Model.Sprite.Update(gameTime);

            Model.Position += Model.Direction;
            Model.Direction *= (float) 0.9;
            if (Model.Direction.LengthSquared() < 0.001)
            {
                Model.Direction = Vector2.Zero;
            }
        }

        public void ApplyForce(Vector2 force, string source)
        {
            if (source == "Bomb")
            {
                Model.Direction += force;
                
                var normalized =
                    Vector2.Normalize(Model.Direction);

                var maxSpeedVector = normalized * (float)Model.MaxSpeed;

                if (maxSpeedVector.LengthSquared() < Model.Direction.LengthSquared())
                {
                    Model.Direction = maxSpeedVector;
                }
            }
        }
        #endregion
    }
}
