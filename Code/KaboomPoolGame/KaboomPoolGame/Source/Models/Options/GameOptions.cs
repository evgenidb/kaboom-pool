﻿using KaboomPoolGame.Source.Models.Controls;

namespace KaboomPoolGame.Source.Models.Options
{
    public class GameOptions
    {
        #region Properties
        public string GameName { get; set; } = "Kaboom Pool";
        public int GameWidth { get; set; } = 800;
        public int GameHeight { get; set; } = 600;
        public bool IsFullScreen { get; set; } = false;
        public bool IsResizingAllowed { get; set; } = false;

        public GameControls Controls { get; set; }
        #endregion
    }
}
