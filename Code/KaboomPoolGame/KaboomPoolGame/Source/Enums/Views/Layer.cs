﻿namespace KaboomPoolGame.Source.Enums.Views
{
    public enum Layer
    {
        Ground,
        Wall,
        Obstacle,
        Bomb,
        Ball,
        Player,
        GroundEffect,
        AirEffect,
        EffectlessTerrain,
        Background,
        MovingBackground,
    }
}
