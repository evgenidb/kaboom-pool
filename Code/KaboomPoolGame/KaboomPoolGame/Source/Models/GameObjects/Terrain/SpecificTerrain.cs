﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.GameObjects.Terrain
{
    public class SpecificTerrain : GameObject
    {
        #region Properties

        #endregion

        #region Constructors
        public SpecificTerrain(string terrainName, Vector2 position, Vector2 size, double rotation = 0, Vector2? scale = null)
            : base(terrainName, position, size, scale: scale)
        { }
        #endregion

        #region Public Methods
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj is SpecificTerrain)
            {
                return Equals(obj as SpecificTerrain);
            }
            else
            {
                var exceptionMessage = string.Join(" ",
                    "Cannot compare",
                    $"{ nameof(SpecificTerrain)} object",
                    $"with non-{nameof(SpecificTerrain)} object.");

                throw new InvalidOperationException(
                    exceptionMessage);
            }
        }

        public bool Equals(SpecificTerrain obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(SpecificTerrain objA, SpecificTerrain objB)
        {
            return objA.Equals(objB);
        }

        public static bool operator !=(SpecificTerrain objA, SpecificTerrain objB)
        {
            return !(objA.Equals(objB));
        }
        #endregion
    }
}
