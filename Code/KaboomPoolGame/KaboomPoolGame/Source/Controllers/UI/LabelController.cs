﻿using KaboomPoolGame.Source.Models.UI;
using KaboomPoolGame.Source.Views.UI;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.UI
{
    public class LabelController : BaseController<LabelModel, LabelView>
    {
        #region Constructors
        public LabelController(LabelModel model, LabelView view)
            : base(model, view) { }
        #endregion

        #region Public Methods
        public void ChangeFont(string name)
        {
            Model.FontName = name;
            if (View.IsContentLoaded)
            {
                View.LoadContent();
            }

        }

        public void ChangeBackground(string name)
        {
            Model.BackgroundName = name;
            if (View.IsContentLoaded)
            {
                View.LoadContent();
            }
            
        }

        public void ChangeText(string text)
        {
            Model.Text = text;
        }

        public void ChangeTextColor(Color color)
        {
            Model.TextColor = color;
        }
        #endregion
    }
}
