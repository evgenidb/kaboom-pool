﻿using System.Collections.Generic;

namespace KaboomPoolGame.Source.Models.GameObjects
{
    public interface IDestructable
    {
        /// <summary>
        /// Returns true if Obstacle can be destroyed
        /// by ANY other object.
        /// </summary>
        bool IsDestructable { get; }

        /// <summary>
        /// Collection of names of objects that can
        /// destroy this obstacle, e.g. Bomb, Ball, etc.
        /// If the Obstacle is indestructable, 
        /// then the collection is empty.
        /// </summary>
        HashSet<string> DestructableBy { get; }

        /// <summary>
        /// Minimum Force required to destroy
        /// the object.
        /// </summary>
        double RequiredMinDestructionForce { get; }
    }
}
