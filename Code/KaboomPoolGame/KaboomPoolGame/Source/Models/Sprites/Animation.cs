﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Utils;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.Sprites
{
    public class Animation : ISpriteData
    {
        #region Fields
        private double totalTime;
        private double frameDuration;
        private int frameIndex;
        #endregion

        #region Properties
        public string SpriteName { get; }
        public string TextureName => Frames[FrameIndex];

        public IEnumerable<string> TextureNames => Frames;
        public bool IsAnimation { get; } = true;
        public bool HasFinished { get; private set; } = false;
        #endregion

        #region Private Properties
        private bool IsLooped { get; }

        private List<string> Frames { get; } =
            new List<string>();

        private int FramesCount => Frames.Count;

        private int FrameIndex
        {
            get { return frameIndex; }
            set
            {
                if (value >= 0 && value < FramesCount)
                {
                    frameIndex = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        message: $"{FrameIndex} must be a positive number.",
                        paramName: nameof(FrameIndex),
                        actualValue: value);
                }
            }
        }

        private double FrameDuration
        {
            get { return frameDuration; }
            set
            {
                if (value > 0)
                {
                    frameDuration = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        message: $"{FrameDuration} must be a positive number.",
                        paramName: nameof(FrameDuration),
                        actualValue: value);
                }
            }
        }

        private double TotalTime
        {
            get { return totalTime; }
            set
            {
                if (value >= 0)
                {
                    totalTime = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        message: $"{TotalTime} must be a positive number or zero.",
                        paramName: nameof(TotalTime),
                        actualValue: value);
                }
            }
        }
        #endregion

        #region Constructors
        public Animation(string animationName, double frameDuration, IList<string> frames, bool isLooped)
        {
            SpriteName = animationName;
            FrameDuration = frameDuration;
            IsLooped = isLooped;

            AddTextures(frames);

            TotalTime = 0;
        }

        public Animation(string animationName, IList<FrameData> frames, bool isLooped)
        {
            SpriteName = animationName;
            IsLooped = isLooped;

            AddTextures(frames);

            TotalTime = 0;
        }
        #endregion

        #region Public Methods
        public void Update(GameTime gameTime)
        {
            var increment =
                gameTime.ElapsedGameTime.TotalSeconds;

            TotalTime += increment;

            CalculateFrameIndex();
        }

        public void Reset()
        {
            TotalTime = 0;
            FrameIndex = 0;
            HasFinished = false;
        }
        #endregion

        #region Private Methods
        private void AddTextures(IList<string> textures)
        {
            if (textures == null)
            {
                throw new ArgumentNullException(
                    message: "Textures must be initialized list",
                    paramName: nameof(textures));
            }
            else if (textures.Count < 1)
            {
                throw new ArgumentException(
                    message: "Textures list cannot be empty.",
                    paramName: nameof(textures));
            }

            Frames.AddRange(textures);
        }

        private void AddTextures(IList<FrameData> frames)
        {
            var textures = new List<string>();
            if (frames == null)
            {
                throw new ArgumentNullException(
                    message: "Frames must be initialized list",
                    paramName: nameof(textures));
            }
            else if (frames.Count < 1)
            {
                throw new ArgumentException(
                    message: "Frames list cannot be empty.",
                    paramName: nameof(textures));
            }

            FrameDuration = frames[0].Duration;

            foreach (var frame in frames)
            {
                if (frame.Duration != FrameDuration)
                {
                    throw new ArgumentException(
                        message: "Duration in frames must be the same, but different given.",
                        paramName: nameof(frames));
                }
                textures.Add(frame.TextureName);
            }
            AddTextures(textures);
        }

        private void CalculateFrameIndex()
        {
            // Return if Play Once and on last frame
            var lastFrameIndex = FramesCount - 1;

            if (!IsLooped && FrameIndex == lastFrameIndex)
            {
                HasFinished = true;
                return;
            }

            double timeSinceFirstFrame =
                TotalTime % (FramesCount * FrameDuration);
            
            FrameIndex =
                (int)(timeSinceFirstFrame / FrameDuration);

            if (FrameIndex > lastFrameIndex)
            {
                if (IsLooped)
                {
                    FrameIndex = FrameIndex % lastFrameIndex;
                }
                else
                {
                    FrameIndex = GameServices.Math.Clamp(
                        min: 0,
                        value: FrameIndex,
                        max: lastFrameIndex);

                    HasFinished = true;
                }
            }
        }
        #endregion
    }
}
