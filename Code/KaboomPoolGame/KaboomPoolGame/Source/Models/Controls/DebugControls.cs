﻿using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;
using KaboomPoolGame.Source.Models.Controls.InputCommands.SpecialInputCommands;

namespace KaboomPoolGame.Source.Models.Controls
{
    public class DebugControls
    {
        public IInputCommand ActivateDebugMode { get; set; } = new NullInputCommand();
        public IInputCommand DeactivateDebugMode { get; set; } = new NullInputCommand();

        public IInputCommand WinLevel { get; set; } = new NullInputCommand();
        public IInputCommand LoseLevel { get; set; } = new NullInputCommand();

        public IInputCommand ReloadLevelData { get; set; } = new NullInputCommand();
        public IInputCommand ReloadAllLevelsData { get; set; } = new NullInputCommand();
    }
}
