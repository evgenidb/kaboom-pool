﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace KaboomPoolGameTests.Utils.MathUtils.GeometryUtilTests
{
    [TestClass]
    public class AngleBetweenVectorsTests
    {
        #region Constants
        private const double ExpectedAngleDelta = 0.000001;
        #endregion

        #region Fields
        private static GeometryUtil geometry;
        #endregion

        #region Properties
        public GeometryUtil Geometry =>
            geometry;
        #endregion

        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            geometry = new GeometryUtil();
            GameServices.Geometry = geometry;
        }
        #endregion


        #region Test Methods
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ZeroVectors()
        {
            // Assign
            Vector2 v1 = Vector2.Zero;
            Vector2 v2 = Vector2.Zero;

            // Act
            Geometry.AngleBetweenVectors(v1, v2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void FirstVectorIsZero()
        {
            // Assign
            Vector2 v1 = Vector2.Zero;
            Vector2 v2 = new Vector2(1, 1);

            // Act
            Geometry.AngleBetweenVectors(v1, v2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SecondVectorIsZero()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 1);
            Vector2 v2 = Vector2.Zero;

            // Act
            Geometry.AngleBetweenVectors(v1, v2);
        }

        [TestMethod]
        public void SameAxisVectorsSameLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 0);
            Vector2 v2 = new Vector2(1, 0);

            double expected = Radians(0);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void OppositeAxisVectorsSameLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 0);
            Vector2 v2 = new Vector2(-1, 0);

            double expected = Radians(180);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void Angle90AxisVectorsSameLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 0);
            Vector2 v2 = new Vector2(0, 1);

            double expected = Radians(90);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void AngleMinus90AxisVectorsSameLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 0);
            Vector2 v2 = new Vector2(0, -1);

            double expected = Radians(90);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void SameVectorsSameLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 2);
            Vector2 v2 = new Vector2(1, 2);

            double expected = Radians(0);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void OppositeVectorsSameLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 2);
            Vector2 v2 = new Vector2(-1, -2);

            double expected = Radians(180);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void Angle90VectorsSameLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 2);
            Vector2 v2 = new Vector2(-2, 1);

            double expected = Radians(90);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void AngleMinus90VectorsSameLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 2);
            Vector2 v2 = new Vector2(2, -1);

            double expected = Radians(90);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void SameAxisVectorsDifferentLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 0);
            Vector2 v2 = new Vector2(10, 0);

            double expected = Radians(0);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void OppositeAxisVectorsDifferentLength()
        {
            // Assign
            Vector2 v1 = new Vector2(10, 0);
            Vector2 v2 = new Vector2(-1, 0);

            double expected = Radians(180);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void Angle90AxisVectorsDifferentLength()
        {
            // Assign
            Vector2 v1 = new Vector2(10, 0);
            Vector2 v2 = new Vector2(0, 100);

            double expected = Radians(90);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void AngleMinus90AxisVectorsDifferentLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 0);
            Vector2 v2 = new Vector2(0, -10);

            double expected = Radians(90);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void SameVectorsDifferentLength()
        {
            // Assign
            Vector2 v1 = new Vector2(10, 20);
            Vector2 v2 = new Vector2(1, 2);

            double expected = Radians(0);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void OppositeVectorsDifferentLength()
        {
            // Assign
            Vector2 v1 = new Vector2(10, 20);
            Vector2 v2 = new Vector2(-1, -2);

            double expected = Radians(180);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void Angle90VectorsDifferentLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 20);
            Vector2 v2 = new Vector2(-400, 20);

            double expected = Radians(90);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }

        [TestMethod]
        public void AngleMinus90VectorsDifferentLength()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 2);
            Vector2 v2 = new Vector2(200, -100);

            double expected = Radians(90);

            // Act
            double actual = Geometry.AngleBetweenVectors(v1, v2);

            // Assert
            Assert.AreEqual(expected, actual, ExpectedAngleDelta);
        }



        [TestMethod]
        public void AngleIsSameIfVectorsSwapped()
        {
            // Assign
            Vector2 v1 = new Vector2(1, 2);
            Vector2 v2 = new Vector2(-3, 4);
            
            // Act
            double actualNormal = Geometry
                .AngleBetweenVectors(v1, v2);
            double actualSwapped = Geometry
                .AngleBetweenVectors(v2, v1);

            bool areEqual =
                Math.Abs(actualNormal - actualSwapped)
                < ExpectedAngleDelta;

            // Assert
            Assert.IsTrue(areEqual);
        }
        #endregion

        #region Helper Methods
        private double Radians(double degrees)
        {
            return GameServices.Geometry.DegreeToRadian(degrees);
        }
        #endregion
    }
}
