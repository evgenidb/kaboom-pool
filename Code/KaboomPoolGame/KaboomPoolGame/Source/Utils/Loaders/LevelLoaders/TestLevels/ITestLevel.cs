﻿using KaboomPoolGame.Source.Models;

namespace KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.TestLevels
{
    public interface ITestLevel
    {
        string Name { get; }
        LevelData Data { get; }
    }
}
