﻿using KaboomPoolGame.Source.Models.UI;
using KaboomPoolGame.Source.Views.UI;

namespace KaboomPoolGame.Source.Controllers.UI
{
    public class ImageController : BaseController<ImageModel, ImageView>
    {
        #region Constructors
        public ImageController(ImageModel model, ImageView view)
            : base(model, view) { }
        #endregion
    }
}