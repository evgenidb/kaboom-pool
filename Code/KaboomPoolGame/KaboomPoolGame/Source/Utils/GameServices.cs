﻿using System;
using KaboomPoolGame.Source.Controllers.Factories;
using KaboomPoolGame.Source.Controllers.Factories.GameObjectFactories;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Options;
using KaboomPoolGame.Source.Models.Repositories;
using KaboomPoolGame.Source.Utils.Loaders;
using KaboomPoolGame.Source.Utils.Loaders.LevelLoaders;
using KaboomPoolGame.Source.Utils.MathUtils;
using KaboomPoolGame.Source.Utils.PhysicsUtils;

namespace KaboomPoolGame.Source.Utils
{
    public class GameServices
    {
        #region Constructors
        public static void Initialize(KaboomPool game)
        {
            Game = game;
        }
        #endregion


        #region Services
        public static Random Random { get; set; }
        public static KaboomPool Game { get; private set; }
        public static GameStateManager GameStateManager { get; set; }
        public static SpriteManager SpritesManager { get; set; }
        public static InputManager Input { get; set; }
        public static GameUtil Utils { get; set; }
        #endregion


        #region Repositories
        public static LevelRepository LevelRepository { get; set; }
        public static SpritesheetRepository SpritesheetRepository { get; set; }
        #endregion


        #region Factories
        public static UIFactory UIFactory { get; set; }
        public static EntityFactoriesService EntityFactories { get; set; }
        #endregion


        #region Loaders
        public static ContentLoader ContentLoader { get; set; }
        public static DataLoader DataLoader { get; set; }
        public static ILevelLoader LevelLoader { get; set; }
        #endregion


        #region Options
        public static GameOptions GameOptions { get; set; }
        public static MenuOptions MenuOptions { get; set; }
        public static PlayOptions PlayOptions { get; set; }
        public static DebugOptions DebugOptions { get; set; }
        #endregion


        #region Physics
        public static PhysicsUtil Physics { get; set; }
        #endregion


        #region Math
        public static MathUtil Math { get; set; }
        public static GeometryUtil Geometry { get; set; }
        #endregion
    }
}
