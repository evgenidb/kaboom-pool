﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaboomPoolGame.Source.Models.Sprites
{
    public struct FrameData
    {
        public string TextureName { get; set; }
        public double Duration { get; set; }

        public FrameData(string texture, double duration)
        {
            TextureName = texture;
            Duration = duration;
        }
    }
}
