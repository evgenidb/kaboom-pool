﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace KaboomPoolGameTests.Utils.MathUtils.GeometryUtilTests
{
    [TestClass]
    public class SegmentsIntersectionTests
    {
        #region Structs
        private struct Segment
        {
            public Vector2 A;
            public Vector2 B;

            public Segment(Vector2 a, Vector2 b)
            {
                A = a;
                B = b;
            }
        }
        #endregion

        #region Fields
        private static GeometryUtil geometry;
        #endregion

        #region Properties
        public GeometryUtil Geometry =>
            geometry;
        #endregion

        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            geometry = new GeometryUtil();
            GameServices.Geometry = geometry;
        }
        #endregion


        #region Test Methods
        [TestMethod]
        public void OnSameLine_SameSegments()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(3, 4),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(3, 4),
            };

            // Act
            var actual = Geometry.DoSegmentsIntersect(
                segment1.A, segment1.B,
                segment2.A, segment2.B);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void OnSameLine_SegmentContainsAnother()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(3, 4),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(-1, 0),
                B = new Vector2(7, 8),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsTrue(actualFirst1);
            Assert.IsTrue(actualFirst2);
        }

        [TestMethod]
        public void OnSameLine_SegmentsPartiallyContain()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(5, 6),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(3, 4),
                B = new Vector2(7, 8),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsTrue(actualFirst1);
            Assert.IsTrue(actualFirst2);
        }

        [TestMethod]
        public void OnSameLine_SegmentsTouchPoint()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(3, 4),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(3, 4),
                B = new Vector2(7, 8),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsTrue(actualFirst1);
            Assert.IsTrue(actualFirst2);
        }

        [TestMethod]
        public void OnSameLine_NoIntersection()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(3, 4),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(5, 6),
                B = new Vector2(7, 8),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsFalse(actualFirst1);
            Assert.IsFalse(actualFirst2);
        }
        [TestMethod]
        public void OnParallelLines_SameSegments()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(3, 4),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(1, 4),
                B = new Vector2(3, 7),
            };

            // Act
            var actual = Geometry.DoSegmentsIntersect(
                segment1.A, segment1.B,
                segment2.A, segment2.B);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void OnParallelLines_SegmentContainsAnother()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(3, 4),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(-1, 3),
                B = new Vector2(7, 11),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsFalse(actualFirst1);
            Assert.IsFalse(actualFirst2);
        }

        [TestMethod]
        public void OnParallelLines_SegmentsPartiallyContain()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(5, 6),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(3, 7),
                B = new Vector2(7, 11),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsFalse(actualFirst1);
            Assert.IsFalse(actualFirst2);
        }

        [TestMethod]
        public void OnParallelLines_SegmentsTouchPoint()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(3, 4),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(3, 7),
                B = new Vector2(7, 11),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsFalse(actualFirst1);
            Assert.IsFalse(actualFirst2);
        }

        [TestMethod]
        public void OnParallelLines_NoIntersection()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(3, 4),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(5, 9),
                B = new Vector2(7, 11),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsFalse(actualFirst1);
            Assert.IsFalse(actualFirst2);
        }

        [TestMethod]
        public void OnDifferentLines_CompleteNoIntersection()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 1),
                B = new Vector2(3, 3),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(1, 2),
                B = new Vector2(5, 7),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsFalse(actualFirst1);
            Assert.IsFalse(actualFirst2);
        }

        [TestMethod]
        public void OnDifferentLines_NoIntersection_SegmentsIntersectIfFirstIsLine()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 1),
                B = new Vector2(3, 3),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(1, 3),
                B = new Vector2(0, 4),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsFalse(actualFirst1);
            Assert.IsFalse(actualFirst2);
        }

        [TestMethod]
        public void OnDifferentLines_PointIntersectionOnVertex()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 1),
                B = new Vector2(3, 3),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(1, 1),
                B = new Vector2(0, 4),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsTrue(actualFirst1);
            Assert.IsTrue(actualFirst2);
        }

        [TestMethod]
        public void OnDifferentLines_PointIntersectionInMiddle()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 1),
                B = new Vector2(3, 3),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(2, 2),
                B = new Vector2(0, 4),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsTrue(actualFirst1);
            Assert.IsTrue(actualFirst2);
        }

        [TestMethod]
        public void OnDifferentLines_CrossIntersection()
        {
            // Assign
            var segment1 = new Segment()
            {
                A = new Vector2(1, 1),
                B = new Vector2(3, 3),
            };
            var segment2 = new Segment()
            {
                A = new Vector2(5, -2),
                B = new Vector2(0, 4),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    segment1.A, segment1.B,
                    segment2.A, segment2.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment2.A, segment2.B,
                    segment1.A, segment1.B);

            // Assert
            Assert.IsTrue(actualFirst1);
            Assert.IsTrue(actualFirst2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void OnZeroLengthLine_PointOutsideSegment()
        {
            // Assign
            var point = new Segment()
            {
                A = new Vector2(1, 1),
                B = new Vector2(1, 1),
            };
            var segment = new Segment()
            {
                A = new Vector2(0, -2),
                B = new Vector2(0, 4),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    point.A, point.B,
                    segment.A, segment.B);

            var actualFirst2 =
                Geometry.DoSegmentsIntersect(
                    segment.A, segment.B,
                    point.A, point.B);

            // Assert
            Assert.IsFalse(actualFirst1);
            Assert.IsFalse(actualFirst2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void OnZeroLengthLine_PointOnSegment()
        {
            // Assign
            var point = new Segment()
            {
                A = new Vector2(1, 1),
                B = new Vector2(1, 1),
            };
            var segment = new Segment()
            {
                A = new Vector2(0, 0),
                B = new Vector2(4, 4),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    point.A, point.B,
                    segment.A, segment.B);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void OnZeroLengthLine_PointOnSegmentVertex()
        {
            // Assign
            var point = new Segment()
            {
                A = new Vector2(1, 1),
                B = new Vector2(1, 1),
            };
            var segment = new Segment()
            {
                A = new Vector2(1, 1),
                B = new Vector2(4, 4),
            };

            // Act
            var actualFirst1 =
                Geometry.DoSegmentsIntersect(
                    point.A, point.B,
                    segment.A, segment.B);
        }
        #endregion
    }
}
