﻿using KaboomPoolGame.Source.Enums.GameObjects;
using KaboomPoolGame.Source.Models.GameObjects.Equipments.Bombs;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.Factories.GameObjectFactories
{
    public class BombFactory
    {
        public Bomb MakeBomb()
        {
            var frames = new[]
            {
                "Objects/Round Bomb",
                "Objects/Round Bomb - Flash",
            };

            var sprite = new Animation(
                animationName: "Bomb Blip",
                frameDuration: 1,
                frames: frames,
                isLooped: true);

            var model = new Bomb(
                position: Vector2.Zero,
                explosionDelay: 3,
                size: new Vector2(20, 20),
                sprite: sprite);

            model.Power = 10;
            model.Range = 50;
            model.ForceFunction = FunctionType.Linear;

            return model;
        }
    }
}
