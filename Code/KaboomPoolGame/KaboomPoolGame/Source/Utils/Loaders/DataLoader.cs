﻿using System;
using System.Collections.Generic;
using System.IO;
using KaboomPoolGame.Source.Models.Structs;
using KaboomPoolGame.Source.Utils.FileReaderWriters.Interfaces;
using Newtonsoft.Json.Linq;

namespace KaboomPoolGame.Source.Utils.Loaders
{
    public class DataLoader
    {
        #region Properties
        private IDictionary<string, dynamic> Readers { get; }
        #endregion

        #region Constructors
        public DataLoader(IDictionary<string, dynamic> readers)
        {
            Readers = readers;
        }
        #endregion

        #region Public Methods
        public JObject LoadJson(string path)
        {
            var reader = Readers["json"]
                as IFileReader<JObject>;

            if (reader != null)
            {
                return Load(path, reader);
            }
            else
            {
                var exceptionMessage = string.Join(" ",
                    "Reader does not implement",
                    $"{nameof(IFileReader<JObject>)}.");

                throw new Exception(
                    message: exceptionMessage);
            }
        }

        public JObject LoadJson(string path, string name)
        {
            var reader = Readers["json"]
                as IFileReader<JObject>;

            if (reader != null)
            {
                return Load(path, name, reader);
            }
            else
            {
                var exceptionMessage = string.Join(" ",
                    "Reader does not implement",
                    $"{nameof(IFileReader<JObject>)}.");

                throw new Exception(
                    message: exceptionMessage);
            }
        }

        public SpritesheetInfo LoadSpritesheetData(string name)
        {
            var reader = Readers["spritesheet"]
                as IFileReader<SpritesheetInfo>;

            if (reader != null)
            {
                return Load("Spritesheets", name, reader);
            }
            else
            {
                var exceptionMessage = string.Join(" ",
                    "Reader does not implement",
                    $"{nameof(IFileReader<SpritesheetInfo>)}.");

                throw new Exception(
                    message: exceptionMessage);
            }
        }
        #endregion

        #region Private Methods
        private T Load<T>(string path, string name, IFileReader<T> reader)
        {
            var file = Path.Combine(
                "Data", path, name);

            return Load(file, reader);
        }

        private T Load<T>(string path, IFileReader<T> reader)
        {
            return reader.Read(path);
        }
        #endregion
    }
}
