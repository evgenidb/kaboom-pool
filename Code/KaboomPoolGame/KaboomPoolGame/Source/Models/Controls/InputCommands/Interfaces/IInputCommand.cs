﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces
{
    public interface IInputCommand
    {
        bool Check(InputManager input, CheckInputArgs args = null);
        bool Check(InputManager input, IEnumerable<CheckInputArgs> argsCollection);
    }
}
