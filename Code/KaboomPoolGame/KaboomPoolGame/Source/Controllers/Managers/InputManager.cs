﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace KaboomPoolGame.Source.Controllers.Managers
{
    public class InputManager
    {
        #region Properties
        // Game Time
        public GameTime GameTime { get; set; }

        // Keyboard
        private KeyboardState PreviousKeyboard { get; set; }
        private KeyboardState CurrentKeyboard { get; set; }

        // Mouse
        private MouseState PreviousMouse { get; set; }
        private MouseState CurrentMouse { get; set; }
        #endregion

        public void RecordInput(GameTime gameTime)
        {
            GameTime = gameTime;

            // Keyboard
            PreviousKeyboard = CurrentKeyboard;
            CurrentKeyboard = Keyboard.GetState();

            // Mouse
            PreviousMouse = CurrentMouse;
            CurrentMouse = Mouse.GetState();
        }

        #region Keyboard Methods
        /// <summary>
        /// Check if a key is currently held down,
        /// regardless for how long.
        /// </summary>
        /// <param name="key">The key to check.</param>
        /// <returns>True if key is down, otherwise - false.</returns>
        public bool IsKeyHold(Keys key)
        {
            return CurrentKeyboard.IsKeyDown(key);
        }

        /// <summary>
        /// Check if a key is currently up,
        /// regardless if it was for a long time or just released.
        /// </summary>
        /// <param name="key">The key to check.</param>
        /// <returns>True if key is up, otherwise - false.</returns>
        public bool IsKeyUp(Keys key)
        {
            return CurrentKeyboard.IsKeyUp(key);
        }

        /// <summary>
        /// Check if key is released,
        /// i.e. was down in previous record and now it is up.
        /// </summary>
        /// <param name="key">The key to check.</param>
        /// <returns>True if key is released, otherwise - false.</returns>
        public bool IsKeyReleased(Keys key)
        {
            return PreviousKeyboard.IsKeyDown(key) &&
                CurrentKeyboard.IsKeyUp(key);
        }

        /// <summary>
        /// Check if key is pressed,
        /// i.e. was up in previous record and now it is down.
        /// </summary>
        /// <param name="key">The key to check.</param>
        /// <returns>True if key is pressed, otherwise - false.</returns>
        public bool IsKeyPressed(Keys key)
        {
            return PreviousKeyboard.IsKeyUp(key) &&
                CurrentKeyboard.IsKeyDown(key);
        }

        /// <summary>
        /// Check if a combination of keys is held down
        /// at the same time. The press order does not matter.
        /// </summary>
        /// <param name="keys">Collection of keys to check.</param>
        /// <param name="isStrict">
        /// Enforces that only the keys of the collection
        /// must be pressed and not any others.
        /// </param>
        /// <returns>True if combo, otherwise - false.</returns>
        public bool IsComboKeysPressed(IEnumerable<Keys> keys, bool isStrict = false)
        {
            foreach (var key in keys)
            {
                if (!IsKeyHold(key))
                {
                    return false;
                }
            }

            if (isStrict)
            {
                // Check if any additional keys are pressed
                var keysCount = (new List<Keys>(keys)).Count;
                return keysCount == CurrentKeyboard.GetPressedKeys().Length;
            }
            else
            {
                // Other keys can be pressed
                return true;
            }
        }
        #endregion

        #region Mouse Methods
        // Left Mouse Button
        public bool IsLeftMouseButtonDown()
        {
            return CurrentMouse.LeftButton == ButtonState.Pressed;
        }

        public bool IsLeftMouseButtonUp()
        {
            return CurrentMouse.LeftButton == ButtonState.Released;
        }

        public bool IsLeftMouseButtonReleased()
        {
            return PreviousMouse.LeftButton == ButtonState.Pressed &&
                CurrentMouse.LeftButton == ButtonState.Released;
        }

        public bool IsLeftMouseButtonPressed()
        {
            return PreviousMouse.LeftButton == ButtonState.Released &&
                CurrentMouse.LeftButton == ButtonState.Pressed;
        }

        // Right Mouse Button
        public bool IsRightMouseButtonDown()
        {
            return CurrentMouse.RightButton == ButtonState.Pressed;
        }

        public bool IsRightMouseButtonUp()
        {
            return CurrentMouse.RightButton == ButtonState.Released;
        }

        public bool IsRightMouseButtonReleased()
        {
            return PreviousMouse.RightButton == ButtonState.Pressed &&
                CurrentMouse.RightButton == ButtonState.Released;
        }

        public bool IsRightMouseButtonPressed()
        {
            return PreviousMouse.RightButton == ButtonState.Released &&
                CurrentMouse.RightButton == ButtonState.Pressed;
        }

        // Mouse Position
        public Vector2 PreviousMousePosition()
        {
            return PreviousMouse.Position.ToVector2();
        }

        public Vector2 CurrentMousePosition()
        {
            return CurrentMouse.Position.ToVector2();
        }

        public Vector2 DeltaMousePosition()
        {
            var delta =
                -PreviousMousePosition() +
                CurrentMousePosition();

            return delta;
        }
        #endregion
    }
}
