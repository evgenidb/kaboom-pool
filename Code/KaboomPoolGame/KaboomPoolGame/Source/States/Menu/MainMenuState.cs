﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Controllers.UI;
using KaboomPoolGame.Source.Models;
using KaboomPoolGame.Source.States.Game;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Views;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.States.Menu
{
    public class MainMenuState : MenuState
    {
        #region Constructors
        public MainMenuState()
            : base("Main Menu") { }
        #endregion


        #region Public Methods
        public override void Enter()
        {
            base.Enter();
            Menu.LoadContent();
        }

        public override void Enter(GameState oldState)
        {
            base.Enter(oldState);
            Menu.LoadContent();
        }

        public override void Leave()
        {
            Menu.UnloadContent();
        }

        public override void Leave(GameState newState)
        {
            Menu.UnloadContent();
        }

        public override void HandleInput(InputManager input)
        {
            base.HandleInput(input);
        }
        #endregion


        #region Protected Methods
        protected override void InitializeImplementation()
        {
            var factory = GameServices.UIFactory;
            var buttons = new List<ButtonController>
            {
                factory.MakeButton("New Game", new Vector2(200, 200), "Buttons/New Game button - Normal State", "Buttons/New Game button - Hover State", "Buttons/New Game button - Click State", "Buttons/New Game button - Disabled"),
                factory.MakeButton("Exit", new Vector2(200, 300), "Buttons/Exit Game button - Normal State", "Buttons/Exit Game button - Hover State", "Buttons/Exit Game button - Click State", "Buttons/Exit Game button - Disabled"),
            };

            var labels = new List<LabelController>();

            var images = new List<ImageController>
            {
                //factory.MakeImage("Logos/Game Logo", new Vector2(200, 50))
            };

            var model = new MenuModel("Backgrounds/Menu Background");
            model.Buttons.AddRange(buttons);
            model.Labels.AddRange(labels);
            model.Images.AddRange(images);

            model.SelectedButton.Hover();

            var view = new MenuView();
            Menu = new MenuController(model, view);
        }

        protected override void ChooseAction(string actionName)
        {
            var stateManager = GameServices.GameStateManager;

            if (actionName == "New Game")
            {
                var playState = (PlayState)stateManager.GetState("Play State");

                var levelRepository = GameServices.LevelRepository;
                var level = levelRepository.FirstLevel();

                var levelController = new LevelController(
                    model: level.MakeLevel(),
                    view: new LevelView());

                playState.SetLevel(levelController);

                stateManager.SwitchState("Play State");
            }
            else if (actionName == "Exit")
            {
                stateManager.ExitGame();
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    message: "Unhandled action",
                    paramName: nameof(actionName),
                    actualValue: actionName);
            }
        }
        #endregion
    }
}
