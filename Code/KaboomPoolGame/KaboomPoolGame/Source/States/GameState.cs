﻿using KaboomPoolGame.Source.Controllers.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.States
{
    public abstract class GameState
    {
        #region Properties
        public string Name { get; }

        public bool IsInitialized { get; internal set; } = false;
        #endregion

        #region Constructors
        public GameState(string name)
        {
            Name = name;
        }
        #endregion

        #region Abstract Methods
        public abstract void Leave(GameState newState);
        public abstract void Leave();

        public abstract void Enter(GameState oldState);
        public abstract void Enter();

        public abstract void LoadContent();

        public abstract void UnloadContent();

        public abstract void HandleInput(InputManager input);

        public abstract void Update(GameTime gameTime);

        public abstract void Draw(SpriteBatch spriteBatch);
        #endregion

        #region Public Methods
        public void Initialize()
        {
            InitializeImplementation();
            IsInitialized = true;
        }
        #endregion

        #region Protected Methods
        protected abstract void InitializeImplementation();

        protected abstract void ChooseAction(string actionName);
        #endregion
    }
}
