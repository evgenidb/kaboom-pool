﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Models.UI
{
    public class ButtonModel : UIComponent
    {
        #region Private Methods
        private Dictionary<ButtonState, string> Textures { get; } =
            new Dictionary<ButtonState, string>();
        #endregion


        #region Properties
        public ButtonState ButtonState { get; set; }
        public string Title { get; private set; }

        public IReadOnlyDictionary<ButtonState, string> TextureNames
        {
            get { return Textures; }
        }
        #endregion


        #region Constructors
        public ButtonModel(string title, Vector2 position,
            string normalTexture, string hoverTexture, string clickTexture, string disabledTexture)
            : base(position)
        {
            Title = title;
            ButtonState = ButtonState.Normal;

            // Set button state textures
            Textures[ButtonState.Normal] = normalTexture;
            Textures[ButtonState.Hovered] = hoverTexture;
            Textures[ButtonState.Clicked] = clickTexture;
            Textures[ButtonState.Disabled] = disabledTexture;
        }
        #endregion

    }
}
