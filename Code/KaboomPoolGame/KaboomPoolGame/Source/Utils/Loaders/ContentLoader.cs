﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Models.Structs;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Utils.Loaders
{
    public class ContentLoader
    {
        #region Properties
        private ContentManager Content
        {
            get { return GameServices.Game.Content; }
        }
        #endregion


        #region Constructors
        public ContentLoader() { }
        #endregion


        #region Public Methods
        public Texture2D LoadImage(string name)
        {
            return Load<Texture2D>("Graphics", name);
        }

        public SpriteFont LoadFont(string name)
        {
            return Load<SpriteFont>("Fonts", name);
        }

        public SpritesheetInfo LoadSpritesheet(string name)
        {
            var spritesheet = GameServices.DataLoader
                .LoadSpritesheetData(name);

            spritesheet.LoadContent();

            return spritesheet;
        }
        #endregion


        #region Private Methods
        private T Load<T>(string path, string name)
        {
            return Content.Load<T>($"{path}/{name}");
        }
        #endregion
    }
}
