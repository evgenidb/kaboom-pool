﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.Sprites
{
    public interface ISpriteData
    {
        string SpriteName { get; }
        string TextureName { get; }
        IEnumerable<string> TextureNames { get; }
        bool IsAnimation { get; }
        bool HasFinished { get; }

        void Update(GameTime gameTime);
        void Reset();
    }
}
