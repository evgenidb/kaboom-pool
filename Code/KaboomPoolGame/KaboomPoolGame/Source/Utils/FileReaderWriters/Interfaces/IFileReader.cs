﻿namespace KaboomPoolGame.Source.Utils.FileReaderWriters.Interfaces
{
    public interface IFileReader<T>
    {
        T Read(string path);
    }
}
