﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace KaboomPoolGameTests.Utils.MathUtils.GeometryUtilTests
{
    [TestClass]
    public class PointInsideAABBRectangleTests
    {
        #region Fields
        private static GeometryUtil geometry;
        #endregion

        #region Properties
        public GeometryUtil Geometry =>
            geometry;
        #endregion

        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            geometry = new GeometryUtil();
            GameServices.Geometry = geometry;
            GameServices.Math = new MathUtil();
        }
        #endregion


        #region Test Methods

        #region Rectangle - centered (0, 0)
        [TestMethod]
        public void CenterPointInsideCenteredRectangle()
        {
            // Assign
            var point = new Vector2(0, 0);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideCenteredRectangle()
        {
            // Assign
            var point = new Vector2(1, 2);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TopBorderPointInsideCenteredRectangle()
        {
            // Assign
            var point = new Vector2(1, -2.5f);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void BottomBorderPointInsideCenteredRectangle()
        {
            // Assign
            var point = new Vector2(1, 2.5f);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TopLeftBorderPointInsideCenteredRectangle()
        {
            // Assign
            var point = new Vector2(-5, -2.5f);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void LeftBorderPointInsideCenteredRectangle()
        {
            // Assign
            var point = new Vector2(-5, -1);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void RightBorderPointInsideCenteredRectangle()
        {
            // Assign
            var point = new Vector2(5, -1);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void LeftSidePointOutsideCenteredRectangle()
        {
            // Assign
            var point = new Vector2(-6, 2);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void BottomSidePointOutsideCenteredRectangle()
        {
            // Assign
            var point = new Vector2(2, 3);

            var center = new Vector2(0, 0);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Rectangle - shifted (23, 45)
        [TestMethod]
        public void CenterPointInsideShiftedRectangle()
        {
            // Assign
            var point = new Vector2(23, 45);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void PointInsideShiftedRectangle()
        {
            // Assign
            var point = new Vector2(24, 47);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TopBorderPointInsideShiftedRectangle()
        {
            // Assign
            var point = new Vector2(24, 42.5f);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void BottomBorderPointInsideShiftedRectangle()
        {
            // Assign
            var point = new Vector2(24, 47.5f);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TopLeftBorderPointInsideShiftedRectangle()
        {
            // Assign
            var point = new Vector2(18, 42.5f);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void LeftBorderPointInsideShiftedRectangle()
        {
            // Assign
            var point = new Vector2(18, 44);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void RightBorderPointInsideShiftedRectangle()
        {
            // Assign
            var point = new Vector2(28, 44);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void LeftSidePointOutsideShiftedRectangle()
        {
            // Assign
            var point = new Vector2(17, 47);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void BottomSidePointOutsideShiftedRectangle()
        {
            // Assign
            var point = new Vector2(25, 48);

            var center = new Vector2(23, 45);
            double width = 10;
            double height = 5;

            // Act
            bool actual =
                Geometry.IsPointInsideAABBRectangle(
                    point: point,
                    rectangleCenter: center,
                    width: width,
                    height: height);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #endregion
    }
}
