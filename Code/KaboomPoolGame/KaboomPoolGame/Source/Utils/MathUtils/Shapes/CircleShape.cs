﻿using System;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Utils.MathUtils.Shapes
{
    public class CircleShape : IShape
    {
        #region Properties
        public double X { get; private set; }
        public double Y { get; private set; }
        public double Radius { get; private set; }
        #endregion

        #region Constructors
        public CircleShape(double x, double y, double radius)
        {
            X = x;
            Y = y;
            Radius = radius;
        }
        #endregion

        #region Public Methods
        public bool CollidesWith(IShape other)
        {
            if (other is RectangleShape)
            {
                // Either circle is inside the rectangle
                // (check the center only),
                // or any rectangle vertex
                // is inside the circle,
                // or the distnace from one of
                // rectangle's sides to the
                // circle's center is less than
                // the radius.
                var otherRectangle =
                    other as RectangleShape;

                var geometry = GameServices.Geometry;

                var circleCenter =
                    geometry.RotatePoint(
                        point: new Vector2(
                            x: (float)X,
                            y: (float)Y),
                        origin: new Vector2(
                            x: (float)otherRectangle.X,
                            y: (float)otherRectangle.Y),
                        angle: -otherRectangle.Rotation);

                bool isCenterInsideRectangle =
                    geometry.IsPointInsideRectangle(
                    point: circleCenter,
                    rectangleCenter: new Vector2(
                        x: (float)otherRectangle.X,
                        y: (float)otherRectangle.Y),
                    width: otherRectangle.Width,
                    height: otherRectangle.Height,
                    rotation: otherRectangle.Rotation);

                if (isCenterInsideRectangle)
                {
                    return true;
                }

                var halfWidth = otherRectangle.Width / 2;
                var halfHeight = otherRectangle.Height / 2;

                // Check if vertex is inside circle
                var rectangleVertices = new[]
                {
                    new Vector2(
                        x: (float)(otherRectangle.X - halfWidth),
                        y: (float)(otherRectangle.Y - halfHeight)),
                    new Vector2(
                        x: (float)(otherRectangle.X + halfWidth),
                        y: (float)(otherRectangle.Y - halfHeight)),
                    new Vector2(
                        x: (float)(otherRectangle.X + halfWidth),
                        y: (float)(otherRectangle.Y + halfHeight)),
                    new Vector2(
                        x: (float)(otherRectangle.X - halfWidth),
                        y: (float)(otherRectangle.Y + halfHeight)),
                };

                foreach (var vertex in rectangleVertices)
                {
                    var isVertexInside =
                        geometry.IsPointInsideCircle(
                            point: vertex,
                            circleCenter: circleCenter,
                            radius: Radius);

                    if (isVertexInside)
                    {
                        return true;
                    }
                }

                // Check if side collides with circle
                var math = GameServices.Math;

                var rectangleTop = otherRectangle.Y - halfHeight;
                var rectangleBottom = otherRectangle.Y + halfHeight;
                var rectangleLeft = otherRectangle.X - halfWidth;
                var rectangleRight = otherRectangle.X + halfWidth;

                if (math.IsInRange(rectangleTop, circleCenter.Y, rectangleBottom))
                {
                    // Left side
                    double sideDistance =
                        (
                            circleCenter -
                            new Vector2(
                                x: (float)rectangleLeft,
                                y: (float)circleCenter.Y)
                        ).Length();
                    if (sideDistance <= Radius)
                    {
                        return true;
                    }

                    // Right side
                    sideDistance =
                        (
                            circleCenter -
                            new Vector2(
                                x: (float)rectangleRight,
                                y: (float)circleCenter.Y)
                        ).Length();
                    if (sideDistance <= Radius)
                    {
                        return true;
                    }
                }

                if (math.IsInRange(rectangleLeft, circleCenter.X, rectangleRight))
                {
                    // Top side
                    double sideDistance =
                        (
                            circleCenter -
                            new Vector2(
                                x: (float)circleCenter.X,
                                y: (float)rectangleTop)
                        ).Length();
                    if (sideDistance <= Radius)
                    {
                        return true;
                    }

                    // Down side
                    sideDistance =
                        (
                            circleCenter -
                            new Vector2(
                                x: (float)circleCenter.X,
                                y: (float)rectangleBottom)
                        ).Length();
                    if (sideDistance <= Radius)
                    {
                        return true;
                    }
                }

                // No intersection
                return false;
            }
            else if (other is CircleShape)
            {
                var otherCircle =
                    other as CircleShape;

                var distance = new Vector2(
                    x: (float)(X - otherCircle.X),
                    y: (float)(Y - otherCircle.Y))
                    .Length();

                var maxDistance =
                    Radius + otherCircle.Radius;

                return distance <= maxDistance;
            }
            else if (other is PointShape)
            {
                var otherPoint = other as PointShape;
                return otherPoint.IsInsideOf(this);
            }
            else if (other is NullShape)
            {
                return false;
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    message: "Unhandled Shape.",
                    paramName: nameof(other),
                    actualValue: other);
            }
        }

        public bool IsInsideOf(IShape other)
        {
            if (other is RectangleShape)
            {
                var otherRectangle =
                    other as RectangleShape;

                var halfWidth = otherRectangle.Width / 2;
                var halfHeight = otherRectangle.Height / 2;

                var geometry = GameServices.Geometry;

                var rotatedCircleCenter =
                    geometry.RotatePoint(
                        point: new Vector2(
                            x: (float)X,
                            y: (float)Y),
                        origin: new Vector2(
                            x: (float)otherRectangle.X,
                            y: (float)otherRectangle.Y),
                        angle: -otherRectangle.Rotation);

                var isInside =
                    (otherRectangle.X - halfWidth) <= (rotatedCircleCenter.X - Radius)
                    &&
                    (otherRectangle.X + halfWidth) >= (rotatedCircleCenter.X + Radius)
                    &&
                    (otherRectangle.Y - halfHeight) <= (rotatedCircleCenter.Y - Radius)
                    &&
                    (otherRectangle.Y + halfHeight) >= (rotatedCircleCenter.Y + Radius);

                return isInside;
            }
            else if (other is CircleShape)
            {
                var otherCircle =
                    other as CircleShape;

                if (otherCircle.Radius < Radius)
                {
                    return false;
                }

                var distance = new Vector2(
                    x: (float)(X - otherCircle.X),
                    y: (float)(Y - otherCircle.Y))
                    .Length();

                var maxDistance =
                    otherCircle.Radius - Radius;

                return distance <= maxDistance;
            }
            else if (other is PointShape)
            {
                return false;
            }
            else if (other is NullShape)
            {
                return false;
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    message: "Unhandled Shape.",
                    paramName: nameof(other),
                    actualValue: other);
            }
        }
        #endregion
    }
}
