﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.GameObjects.Effects;
using KaboomPoolGame.Source.Utils.ExtensionMethods;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Models.Repositories
{
    public class EffectsRepository
    {
        #region Properties
        private bool IsContentLoaded { get; set; } = false;

        private List<ExplosionController> Explosions { get; } =
            new List<ExplosionController>();

        private List<CraterController> Craters { get; } =
            new List<CraterController>();
        #endregion

        #region Public Methods
        public void LoadContent()
        {
            foreach (var item in Explosions)
            {
                item.LoadContent();
            }

            foreach (var item in Craters)
            {
                item.LoadContent();
            }

            IsContentLoaded = true;
        }

        public void UnloadContent()
        {
            foreach (var item in Explosions)
            {
                item.UnloadContent();
            }

            foreach (var item in Craters)
            {
                item.UnloadContent();
            }

            IsContentLoaded = false;
        }

        public void ClearAll()
        {
            if (IsContentLoaded)
            {
                foreach (var item in Explosions)
                {
                    item.UnloadContent();
                }

                foreach (var item in Craters)
                {
                    item.UnloadContent();
                }
            }

            Explosions.Clear();
            Craters.Clear();
        }

        public void Clean()
        {
            // Explosions
            for (int itemIndex = Explosions.Count - 1; itemIndex >= 0; itemIndex--)
            {
                var item = Explosions[itemIndex];

                if (item.MustRemove)
                {
                    if (IsContentLoaded)
                    {
                        item.UnloadContent();
                    }

                    Explosions.RemoveAt(itemIndex);
                }
            }

            // Craters
            for (int itemIndex = Craters.Count - 1; itemIndex >= 0; itemIndex--)
            {
                var item = Craters[itemIndex];

                if (item.MustRemove)
                {
                    if (IsContentLoaded)
                    {
                        item.UnloadContent();
                    }

                    Craters.RemoveAt(itemIndex);
                }
            }
        }

        public void AddCrater(CraterController crater)
        {
            if (IsContentLoaded)
            {
                crater.LoadContent();
            }
            Craters.Add(crater);
        }

        public void AddExplosion(ExplosionController explosion)
        {
            if (IsContentLoaded)
            {
                explosion.LoadContent();
            }
            Explosions.Add(explosion);
        }

        public void Update(GameTime gameTime)
        {
            foreach (var explosion in Explosions)
            {
                explosion.Update(gameTime);
            }

            foreach (var crater in Craters)
            {
                crater.Update(gameTime);
            }

            Clean();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsContentLoaded)
            {
                foreach (var explosion in Explosions)
                {
                    explosion.Draw(spriteBatch);
                }

                foreach (var crater in Craters)
                {
                    crater.Draw(spriteBatch);
                }
            }
        }
        #endregion
    }
}
