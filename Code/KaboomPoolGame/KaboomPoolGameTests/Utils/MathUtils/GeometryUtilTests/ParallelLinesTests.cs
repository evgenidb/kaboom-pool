﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace KaboomPoolGameTests.Utils.MathUtils.GeometryUtilTests
{
    [TestClass]
    public class ParallelLinesTests
    {
        #region Structs
        private struct Line
        {
            public Vector2 A;
            public Vector2 B;
        }
        #endregion

        #region Fields
        private static GeometryUtil geometry;
        #endregion

        #region Properties
        public GeometryUtil Geometry =>
            geometry;
        #endregion

        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            geometry = new GeometryUtil();
            GameServices.Geometry = geometry;
        }
        #endregion


        #region Test Methods
        [TestMethod]
        public void SameLine()
        {
            // Assign
            var line1 = new Line
            {
                A = new Vector2(10, 20),
                B = new Vector2(5, 2),
            };

            var line2 = new Line
            {
                A = new Vector2(10, 20),
                B = new Vector2(5, 2),
            };

            // Act
            var actual = Geometry.AreLinesParallel(
                line1.A, line1.B,
                line2.A, line2.B);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void ParallelXAxisLines()
        {
            // Assign
            var line1 = new Line
            {
                A = new Vector2(0, 0),
                B = new Vector2(5, 0),
            };

            var line2 = new Line
            {
                A = new Vector2(-1, 30),
                B = new Vector2(10, 30),
            };

            // Act
            var actual = Geometry.AreLinesParallel(
                line1.A, line1.B,
                line2.A, line2.B);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void ParallelYAxisLines()
        {
            // Assign
            var line1 = new Line
            {
                A = new Vector2(0, 0),
                B = new Vector2(0, 5),
            };

            var line2 = new Line
            {
                A = new Vector2(10, 3),
                B = new Vector2(10, 30),
            };

            // Act
            var actual = Geometry.AreLinesParallel(
                line1.A, line1.B,
                line2.A, line2.B);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void ParallelXAxisOverlapingLines()
        {
            // Assign
            var line1 = new Line
            {
                A = new Vector2(0, 0),
                B = new Vector2(5, 0),
            };

            var line2 = new Line
            {
                A = new Vector2(-1, 0),
                B = new Vector2(-10, 0),
            };

            // Act
            var actual = Geometry.AreLinesParallel(
                line1.A, line1.B,
                line2.A, line2.B);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void ParallelYAxisOverlapingLines()
        {
            // Assign
            var line1 = new Line
            {
                A = new Vector2(10, 0),
                B = new Vector2(10, 5),
            };

            var line2 = new Line
            {
                A = new Vector2(10, 3),
                B = new Vector2(10, 30),
            };

            // Act
            var actual = Geometry.AreLinesParallel(
                line1.A, line1.B,
                line2.A, line2.B);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void ParallelLines()
        {
            // Assign
            var line1 = new Line
            {
                A = new Vector2(0, 0),
                B = new Vector2(5, 2),
            };

            var line2 = new Line
            {
                A = new Vector2(-1, -1),
                B = new Vector2(9, 3),
            };

            // Act
            var actual = Geometry.AreLinesParallel(
                line1.A, line1.B,
                line2.A, line2.B);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void NonParallelLines()
        {
            // Assign
            var line1 = new Line
            {
                A = new Vector2(0, 1),
                B = new Vector2(5, 2),
            };

            var line2 = new Line
            {
                A = new Vector2(0, -1),
                B = new Vector2(9, 3),
            };

            // Act
            var actual = Geometry.AreLinesParallel(
                line1.A, line1.B,
                line2.A, line2.B);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void PointForLine1()
        {
            // Assign
            var line1 = new Line
            {
                A = new Vector2(0, 1),
                B = new Vector2(0, 1),
            };

            var line2 = new Line
            {
                A = new Vector2(0, -1),
                B = new Vector2(9, 3),
            };

            // Act
            Geometry.AreLinesParallel(
                line1.A, line1.B,
                line2.A, line2.B);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void PointForLine2()
        {
            // Assign
            var line1 = new Line
            {
                A = new Vector2(0, 1),
                B = new Vector2(10, 1),
            };

            var line2 = new Line
            {
                A = new Vector2(3, -1),
                B = new Vector2(3, -1),
            };

            // Act
            Geometry.AreLinesParallel(
                line1.A, line1.B,
                line2.A, line2.B);
        }
        #endregion
    }
}
