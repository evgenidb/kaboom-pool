﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Models.GameObjects.Terrain;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models
{
    public class LevelData
    {
        #region Properties
        public string Name { get; }
        public string Difficulty { get; }
        public int InitialBombCount { get; set; }
        public Point CellSize { get; set; }
        public Point MapSize { get; set; }

        public Dictionary<string, List<CellLocationData>> Terrain { get; } =
            new Dictionary<string, List<CellLocationData>>();

        public Dictionary<string, List<CellLocationData>> Walls { get; } =
            new Dictionary<string, List<CellLocationData>>();

        public List<CellLocationData> Balls { get; } =
            new List<CellLocationData>();

        public List<CellLocationData> Targets { get; } =
            new List<CellLocationData>();

        public Dictionary<string, List<CellLocationData>> Obstacles { get; } =
            new Dictionary<string, List<CellLocationData>>();

        public CellLocationData Player { get; set; }
        #endregion

        #region Constructors
        public LevelData(string difficulty, string name)
        {
            Name = name;
            Difficulty = difficulty;
        }
        #endregion

        #region Factory Methods
        public LevelModel MakeLevel()
        {
            return new LevelModel(this);
        }
        #endregion
    }
}
