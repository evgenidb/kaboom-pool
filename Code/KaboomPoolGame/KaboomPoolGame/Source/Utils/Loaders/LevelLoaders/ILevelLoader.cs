﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Models;

namespace KaboomPoolGame.Source.Utils.Loaders.LevelLoaders
{
    public interface ILevelLoader
    {
        void Initialize();
        IReadOnlyList<string> LoadDifficultyOrder();
        IReadOnlyList<string> LoadLevelOrder(string difficulty);
        LevelData Load(string difficulty, string name);
    }
}
