﻿using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.GameObjects
{
    public interface IMovable
    {
        Vector2 Direction { get; set; }
        double MaxSpeed { get; set; }
    }
}
