﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.GameObjects;
using KaboomPoolGame.Source.Models.GameObjects.Players;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.Factories.GameObjectFactories
{
    public class PlayerFactory
    {
        public Robot MakeRobot()
        {
            var idleFrames = new[]
            {
                new FrameData(
                    texture: "Players/Robot/Robot - Idle",
                    duration: 2),

                new FrameData(
                    texture: "Players/Robot/Robot - Flash",
                    duration: 0.5),
            };

            var sprites = new Dictionary<PlayerState, ISpriteData>
            {
                [PlayerState.Idle] = new VariableAnimation(
                    animationName: "Idle",
                    frames: idleFrames,
                    isLooped: true),

                [PlayerState.MoveUp] = new SpriteData("Idle", "Players/Robot/Robot - Up"),
                [PlayerState.MoveDown] = new SpriteData("Idle", "Players/Robot/Robot - Down"),
                [PlayerState.MoveLeft] = new SpriteData("Idle", "Players/Robot/Robot - Left"),
                [PlayerState.MoveRight] = new SpriteData("Idle", "Players/Robot/Robot - Right"),
                [PlayerState.MoveUpLeft] = new SpriteData("Idle", "Players/Robot/Robot - Up-Left"),
                [PlayerState.MoveUpRight] = new SpriteData("Idle", "Players/Robot/Robot - Up-Right"),
                [PlayerState.MoveDownLeft] = new SpriteData("Idle", "Players/Robot/Robot - Down-Left"),
                [PlayerState.MoveDownRight] = new SpriteData("Idle", "Players/Robot/Robot - Down-Right"),
            };
            
            sprites[PlayerState.Idle] =
                new Animation("Idle", 0.5,
                new[]
                {
                    idleFrames[0].TextureName,
                    idleFrames[1].TextureName,
                }, true);

            var model = new Robot(
                position: Vector2.Zero,
                size: new Vector2(20, 20),
                initialState: PlayerState.Idle,
                stateSprites: sprites);

            model.MaxSpeed = 50;

            return model;
        }
    }
}
