﻿namespace KaboomPoolGame.Source.Enums.UI
{
    public enum ButtonState
    {
        Normal,
        Hovered,
        Clicked,
        Disabled,
    }
}
