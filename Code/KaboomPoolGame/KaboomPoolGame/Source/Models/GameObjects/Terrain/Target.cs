﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.GameObjects.Terrain
{
    public class Target : GameObject, IRenderableModel
    {
        #region Properties
        public Layer Layer { get; } = Layer.EffectlessTerrain;

        public ISpriteData Sprite { get; private set; }
        #endregion

        #region Constructors
        public Target(Vector2 position, Vector2 size, ISpriteData sprite, double rotation = 0, Vector2? scale = default(Vector2?))
            : base("Target", position, size, rotation, scale)
        {
            Sprite = sprite;
        }
        #endregion

        #region Public Methods
        public IEnumerable<string> GetSpriteNames()
        {
            return Sprite.TextureNames;
        }
        #endregion
    }
}
