﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KaboomPoolGameTests.Utils.MathUtils.GeometryUtilTests
{
    [TestClass]
    public class AngleConverionTests
    {
        #region Constants
        private const double ExpectedAngleDelta = 0;
        #endregion

        #region Fields
        private static GeometryUtil geometry;
        #endregion

        #region Properties
        public GeometryUtil Geometry =>
            geometry;
        #endregion

        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            geometry = new GeometryUtil();
            GameServices.Geometry = geometry;
        }
        #endregion

        #region Test Methods
        [TestMethod]
        public void DegreeOf0ShouldReturnRadians0()
        {
            // Assign
            double degree = 0;

            double expected = 0;

            // Act
            double actual = Geometry.DegreeToRadian(degree);

            // Assert
            Assert.AreEqual(
                expected: expected,
                actual: actual,
                delta: ExpectedAngleDelta);
        }

        [TestMethod]
        public void DegreeOf360ShouldReturnRadians0()
        {
            // Assign
            double degree = 360;

            double expected = 2 * Math.PI;

            // Act
            double actual = Geometry.DegreeToRadian(degree);

            // Assert
            Assert.AreEqual(
                expected: expected,
                actual: actual,
                delta: ExpectedAngleDelta);
        }

        [TestMethod]
        public void DegreeOf180ShouldReturnRadiansPi()
        {
            // Assign
            double degree = 180;

            double expected = Math.PI;

            // Act
            double actual = Geometry.DegreeToRadian(degree);

            // Assert
            Assert.AreEqual(
                expected: expected,
                actual: actual,
                delta: ExpectedAngleDelta);
        }

        [TestMethod]
        public void DegreeOfMinus180ShouldReturnRadiansMinusPi()
        {
            // Assign
            double degree = -180;

            double expected = -Math.PI;

            // Act
            double actual = Geometry.DegreeToRadian(degree);

            // Assert
            Assert.AreEqual(
                expected: expected,
                actual: actual,
                delta: ExpectedAngleDelta);
        }

        [TestMethod]
        public void DegreeOf90ShouldReturnRadiansHalfPi()
        {
            // Assign
            double degree = 90;

            double expected = Math.PI / 2;

            // Act
            double actual = Geometry.DegreeToRadian(degree);

            // Assert
            Assert.AreEqual(
                expected: expected,
                actual: actual,
                delta: ExpectedAngleDelta);
        }

        [TestMethod]
        public void DegreeOfMinus90ShouldReturnRadiansMinusHalfPi()
        {
            // Assign
            double degree = -90;

            double expected = -Math.PI / 2;

            // Act
            double actual = Geometry.DegreeToRadian(degree);

            // Assert
            Assert.AreEqual(
                expected: expected,
                actual: actual,
                delta: ExpectedAngleDelta);
        }

        [TestMethod]
        public void DegreeOf45ShouldReturnRadiansQuaterPi()
        {
            // Assign
            double degree = 45;

            double expected = Math.PI / 4;

            // Act
            double actual = Geometry.DegreeToRadian(degree);

            // Assert
            Assert.AreEqual(
                expected: expected,
                actual: actual,
                delta: ExpectedAngleDelta);
        }

        [TestMethod]
        public void DegreeOf30ShouldReturnRadiansSixthPi()
        {
            // Assign
            double degree = 30;

            double expected = Math.PI / 6;

            // Act
            double actual = Geometry.DegreeToRadian(degree);

            // Assert
            Assert.AreEqual(
                expected: expected,
                actual: actual,
                delta: ExpectedAngleDelta);
        }
        #endregion
    }
}
