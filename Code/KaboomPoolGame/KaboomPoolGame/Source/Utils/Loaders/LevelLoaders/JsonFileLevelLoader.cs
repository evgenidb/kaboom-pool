﻿using System;
using System.Collections.Generic;
using System.Linq;
using KaboomPoolGame.Source.Models;
using KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.Parsers;
using Newtonsoft.Json.Linq;

namespace KaboomPoolGame.Source.Utils.Loaders.LevelLoaders
{
    public class JsonFileLevelLoader : ILevelLoader
    {
        #region Constants
        private const string LevelExtension = "json";
        private const string MetaExtension = "json";
        #endregion


        #region Properties
        private JObject MetaFileJson { get; set; }
        private string LevelsPath { get; }
        private string LevelsMetaFileName { get; }

        private string MetaFileLocation =>
            $"{LevelsPath}/{LevelsMetaFileName}.{MetaExtension}";

        private JsonLevelParser Parser { get; }
        #endregion


        #region Constructors
        public JsonFileLevelLoader(string levelsPath, string metaFileName, JsonLevelParser parser)
        {
            LevelsPath = levelsPath;
            LevelsMetaFileName = metaFileName;
            Parser = parser;
        }
        #endregion


        #region Public Methods
        public void Initialize()
        {
            MetaFileJson = LoadJson(LevelsPath, LevelsMetaFileName);
        }

        public IReadOnlyList<string> LoadDifficultyOrder()
        {
            var difficulties =
                MetaFileJson["difficulty_order"]
                .Children()
                .Select(t => t.ToString())
                .ToList();

            return difficulties;
        }

        public IReadOnlyList<string> LoadLevelOrder(string difficulty)
        {
            var levels =
                MetaFileJson["levels"][difficulty]
                .Children()
                .Select(t => t.ToString())
                .ToList();

            return levels;
        }

        public LevelData Load(string difficulty, string name)
        {
            var path = GetLevelPath(difficulty);
            var json = LoadJson(path, name);

            var levelData = ParseLevelJson(json, difficulty, name);
            return levelData;
        }
        #endregion


        #region Private Methods
        private JObject LoadJson(string path, string name)
        {
            var json = GameServices.DataLoader
                .LoadJson(path, name);

            return json;
        }

        private string GetLevelPath(
            string difficulty)
        {
            return $"{LevelsPath}/{difficulty}";
        }

        private LevelData ParseLevelJson(JObject json, string difficulty, string name)
        {
            return Parser.Parse(json, difficulty, name);
        }
        #endregion
    }
}
