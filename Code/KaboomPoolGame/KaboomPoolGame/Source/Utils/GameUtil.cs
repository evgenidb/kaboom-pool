﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Utils.ExtensionMethods;

namespace KaboomPoolGame.Source.Utils
{
    public class GameUtil
    {
        #region Properties
        /// <summary>
        /// Order for the Layers.
        /// <remarks>
        /// Important!!! The first is drawn on top,
        /// the last is drawn in the bottom.
        /// </remarks>
        /// </summary>
        private Layer[] LayersOrder { get; } =
            new[]
            {
                Layer.AirEffect,
                Layer.Wall,
                Layer.Obstacle,
                Layer.Player,
                Layer.Ball,
                Layer.Bomb,
                Layer.EffectlessTerrain,
                Layer.GroundEffect,
                Layer.Ground,
                Layer.MovingBackground,
                Layer.Background,
            };
        #endregion

        #region Public Methods
        public double GetLayerDepth(Layer layer)
        {
            var index = LayersOrder.IndexOf(layer);
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException(
                    message: "Unhandled Layer value.",
                    paramName: nameof(layer),
                    actualValue: layer);
            }

            return (double)index / LayersOrder.Length;
        }

        public T ChooseRandom<T>(IList<T> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(
                    message: "Collection cannot be null.",
                    paramName: nameof(collection));
            }
            else if (collection.Count < 1)
            {
                throw new ArgumentException(
                    message: "Collection must have elements.",
                    paramName: nameof(collection));
            }

            var randomIndex =
                GameServices.Random.Next(collection.Count);

            return collection[randomIndex];
        }
        #endregion
    }
}
