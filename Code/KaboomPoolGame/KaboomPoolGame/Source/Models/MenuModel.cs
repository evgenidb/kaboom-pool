﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.UI;
using KaboomPoolGame.Source.Utils;

namespace KaboomPoolGame.Source.Models
{
    public class MenuModel
    {
        #region Fields
        private int selectedButtonIndex;
        #endregion

        #region Properties
        public string BackgroundName { get; set; }

        public List<ButtonController> Buttons { get; }
            = new List<ButtonController>();

        public List<LabelController> Labels { get; }
            = new List<LabelController>();

        public List<ImageController> Images { get; }
            = new List<ImageController>();

        public ButtonController SelectedButton
        {
            get { return Buttons[SelectedButtonIndex]; }
        }

        public int SelectedButtonIndex
        {
            get { return selectedButtonIndex; }
            set
            {
                var math = GameServices.Math;

                selectedButtonIndex =
                    math.Clamp(
                        value: value,
                        min: 0,
                        max: (Buttons.Count - 1));
            }
        }
        #endregion


        #region Constructors
        public MenuModel(string backgroundName)
        {
            BackgroundName = backgroundName;
        }
        #endregion
    }
}
