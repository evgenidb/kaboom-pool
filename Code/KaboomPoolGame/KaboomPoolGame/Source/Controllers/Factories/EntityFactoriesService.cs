﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Controllers.Factories.GameObjectFactories;

namespace KaboomPoolGame.Source.Controllers.Factories
{
    public class EntityFactoriesService
    {
        public BombFactory BombFactory { get; set; }
        public EffectsFactory EffectsFactory { get; set; }
        public ObjectFactory ObjectFactory { get; set; }
        public PlayerFactory PlayerFactory { get; set; }
        public TargetFactory TargetFactory { get; set; }
        public TerrainFactory TerrainFactory { get; set; }
        public WallFactory WallFactory { get; set; }
    }
}
