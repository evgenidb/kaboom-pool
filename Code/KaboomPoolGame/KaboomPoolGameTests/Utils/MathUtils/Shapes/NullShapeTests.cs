﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using KaboomPoolGame.Source.Utils.MathUtils.Shapes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KaboomPoolGameTests.Utils.MathUtils.Shapes
{
    [TestClass]
    public class NullShapeTests
    {
        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            GameServices.Geometry = new GeometryUtil();
            GameServices.Math = new MathUtil();
        }
        #endregion


        #region Test Methods

        #region Test Method IsInsideOf
        [TestMethod]
        public void NullShapeInsideNullShapeShouldReturnFalse()
        {
            // Assign
            var shape = new NullShape();

            var otherShape = new NullShape();

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void NullShapeInsidePointShouldReturnFalse()
        {
            // Assign
            var shape = new NullShape();

            var otherShape = new PointShape(0, 1);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void NullShapeInsideCircleShouldReturnFalse()
        {
            // Assign
            var shape = new NullShape();

            var otherShape = new CircleShape(
                x: 0,
                y: 1,
                radius: 5);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void NullShapeInsideRectangleShouldReturnFalse()
        {
            // Assign
            var shape = new NullShape();

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 2);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void NullShapeInsideRotatedRectangleShouldReturnFalse()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new NullShape();

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 2,
                rotation: angle);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Test Method CollidesWith
        [TestMethod]
        public void NullShapeCollidesNullShapeShouldReturnFalse()
        {
            // Assign
            var shape = new NullShape();

            var otherShape = new NullShape();

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void NullShapeCollidesPointShouldReturnFalse()
        {
            // Assign
            var shape = new NullShape();

            var otherShape = new PointShape(0, 1);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void NullShapeCollidesCircleShouldReturnFalse()
        {
            // Assign
            var shape = new NullShape();

            var otherShape = new CircleShape(
                x: 0,
                y: 1,
                radius: 5);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void NullShapeCollidesRectangleShouldReturnFalse()
        {
            // Assign
            var shape = new NullShape();

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 2);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void NullShapeCollidesRotatedRectangleShouldReturnFalse()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new NullShape();

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 5,
                height: 2,
                rotation: angle);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #endregion
    }
}
