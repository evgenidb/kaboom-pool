﻿using KaboomPoolGame.Source.Controllers;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace KaboomPoolGame.Source.States.Menu
{
    public abstract class MenuState : GameState
    {
        #region Properties
        protected MenuController Menu { get; set; }
        #endregion


        #region Constructor
        public MenuState(string name)
            : base(name) { }
        #endregion


        #region Public Methods
        public override void Enter()
        {
            ApplyOptions();
            Menu.HoverCurrentButton();
        }

        public override void Enter(GameState oldState)
        {
            ApplyOptions();
            Menu.HoverCurrentButton();
        }

        public override void HandleInput(InputManager input)
        {
            var controls = GameServices.MenuOptions.Controls;

            if (controls.NextMenuItem.Check(input))
            {
                Menu.NextButton();
            }
            if (controls.PreviousMenuItem.Check(input))
            {
                Menu.PreviousButton();
            }
            if (controls.ClickMenuItem.Check(input))
            {
                var buttonName = Menu.ClickButton();
                ChooseAction(buttonName);
            }
        }

        public override void LoadContent()
        {
            Menu.LoadContent();
        }

        public override void UnloadContent()
        {
            Menu.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            Menu.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            Menu.Draw(spriteBatch);
        }
        #endregion


        #region Private Methods
        private void ApplyOptions()
        {
            var options = GameServices.MenuOptions;
            var game = GameServices.Game;

            game.IsMouseVisible =
                options.Controls.IsMouseVisible;

            game.SetFPS(options.FPS);
        }
        #endregion
    }
}
