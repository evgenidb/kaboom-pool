﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Models.Structs;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Controllers.Managers
{
    public class SpriteManager
    {
        #region Properties
        private Dictionary<string, SpriteInfo> Sprites { get; } =
            new Dictionary<string, SpriteInfo>();
        #endregion

        #region Constructors
        public SpriteManager() { }
        #endregion

        #region Public Methods
        public void AddSpriteSheet(string sheetName, Texture2D sheet, IReadOnlyDictionary<string, Rectangle> spritesData)
        {
            foreach (var sprite in spritesData)
            {
                var spriteName = sprite.Key;

                if (Sprites.ContainsKey(spriteName))
                {
                    throw new ArgumentException(
                        message: $"Name collision! Already exists a sprite with name: {spriteName}.",
                        paramName: nameof(spritesData));
                }

                var info = new SpriteInfo(
                    name: spriteName,
                    location: sprite.Value,
                    spriteSheet: sheet);

                Sprites[spriteName] = info;
            }
        }

        public SpriteInfo GetSprite(string textureName)
        {
            return Sprites[textureName];
        }
        #endregion
    }
}
