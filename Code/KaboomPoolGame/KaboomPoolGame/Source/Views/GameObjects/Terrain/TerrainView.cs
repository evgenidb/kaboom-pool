﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Models.GameObjects.Terrain;
using KaboomPoolGame.Source.Utils;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Views.GameObjects.Terrain
{
    public class TerrainView : IView<TerrainType>
    {
        #region Fields
        private TerrainType model;
        #endregion

        #region Properties
        public bool IsContentLoaded { get; private set; }

        private TerrainType Model
        {
            get
            {
                return model;
            }
            set
            {
                if (value != null)
                {
                    model = value;
                }
                else
                {
                    throw new ArgumentNullException(
                        message: "Terrain Model cannot be null",
                        paramName: nameof(Model));
                }
            }
        }

        private Dictionary<string, Texture2D> Textures { get; } =
            new Dictionary<string, Texture2D>();
        #endregion

        #region Public Methods
        public void Initialize(TerrainType model)
        {
            Model = model;
            if (IsContentLoaded)
            {
                UnloadContent();
                LoadContent();
            }
        }

        public void LoadContent()
        {
            var content = GameServices.ContentLoader;

            foreach (var spriteName in Model.GetSpriteNames())
            {
                var texture = content.LoadImage(spriteName);
                Textures[spriteName] = texture;
            }

            IsContentLoaded = true;
        }

        public void UnloadContent()
        {
            IsContentLoaded = false;

            Textures.Clear();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsContentLoaded)
            {
                var layerDepth = GameServices.Utils
                    .GetLayerDepth(Model.Layer);

                foreach (var sprite in Model.Sprites)
                {
                    var variation = sprite.Key;
                    var textureName = sprite.Value.TextureName;

                    var texture = Textures[textureName];

                    var dataCollection = Model.Data[variation];

                    foreach (var data in dataCollection)
                    {
                        spriteBatch.Draw(
                            texture: texture,
                            position: data.Position,
                            origin: data.Origin,
                            rotation: (float)data.Rotation,
                            scale: data.Scale,
                            layerDepth: (float)layerDepth);
                    }
                }
            }
        }
        #endregion
    }
}
