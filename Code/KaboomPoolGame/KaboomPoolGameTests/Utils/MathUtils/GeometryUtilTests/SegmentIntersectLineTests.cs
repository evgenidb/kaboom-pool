﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace KaboomPoolGameTests.Utils.MathUtils.GeometryUtilTests
{
    [TestClass]
    public class SegmentIntersectLineTests
    {
        #region Structs
        private struct Line
        {
            public Vector2 A;
            public Vector2 B;

            public Line(Vector2 a, Vector2 b)
            {
                A = a;
                B = b;
            }
        }
        #endregion

        #region Fields
        private static GeometryUtil geometry;
        #endregion

        #region Properties
        public GeometryUtil Geometry =>
            geometry;
        #endregion

        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            geometry = new GeometryUtil();
            GameServices.Geometry = geometry;
        }
        #endregion


        #region Test Methods
        [TestMethod]
        public void SameLine()
        {
            // Assign
            var segment = new Line
            {
                A = new Vector2(10, 20),
                B = new Vector2(5, 2),
            };

            var line = new Line
            {
                A = new Vector2(10, 20),
                B = new Vector2(5, 2),
            };

            // Act
            var actual =
                Geometry.DoesSegmentIntersectsLine(
                    segment.A, segment.B,
                    line.A, line.B);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void SegmentOnLineBetweenPoints()
        {
            // Assign
            var segment = new Line
            {
                A = new Vector2(1, 1),
                B = new Vector2(2, 2),
            };

            var line = new Line
            {
                A = new Vector2(0, 0),
                B = new Vector2(5, 5),
            };

            // Act
            var actual =
                Geometry.DoesSegmentIntersectsLine(
                    segment.A, segment.B,
                    line.A, line.B);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void SegmentOnLineOutsidePoints()
        {
            // Assign
            var segment = new Line
            {
                A = new Vector2(1, 1),
                B = new Vector2(2, 2),
            };

            var line = new Line
            {
                A = new Vector2(10, 10),
                B = new Vector2(5, 5),
            };

            // Act
            var actual =
                Geometry.DoesSegmentIntersectsLine(
                    segment.A, segment.B,
                    line.A, line.B);

            // Assert
            Assert.IsTrue(actual);
        }
        #endregion
    }
}
