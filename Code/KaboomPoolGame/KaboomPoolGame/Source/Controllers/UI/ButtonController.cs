﻿using KaboomPoolGame.Source.Enums.UI;
using KaboomPoolGame.Source.Models.UI;
using KaboomPoolGame.Source.Views.UI;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.UI
{
    public class ButtonController : BaseController<ButtonModel, ButtonView>
    {
        #region Properties
        public string Title
        {
            get { return Model.Title; }
        }

        public Rectangle Bounds
        {
            get { return Model.Bounds; }
        }

        public ButtonState ButtonState
        {
            get { return Model.ButtonState; }
        }
        #endregion

        #region Constructors
        public ButtonController(ButtonModel model, ButtonView view)
            : base(model, view) { }
        #endregion

        #region Public Methods
        public void Release()
        {
            Model.ButtonState = ButtonState.Normal;
        }

        public void Hover()
        {
            Model.ButtonState = ButtonState.Hovered;
        }

        public void Click()
        {
            Model.ButtonState = ButtonState.Clicked;
        }

        public void Disable()
        {
            Model.ButtonState = ButtonState.Disabled;
        }

        public void Enable(ButtonState state = ButtonState.Normal)
        {
            Model.ButtonState = state;
        }
        #endregion
    }
}
