﻿using KaboomPoolGame.Source.Controllers.UI;
using KaboomPoolGame.Source.Models.UI;
using KaboomPoolGame.Source.Views.UI;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.Factories
{
    public class UIFactory
    {
        public ButtonController MakeButton(string title, Vector2 position, string normalImage, string hoverImage, string clickImage, string disabledImage)
        {
            var model = new ButtonModel(
                title: title,
                position: position,
                normalTexture: normalImage,
                hoverTexture: hoverImage,
                clickTexture: clickImage,
                disabledTexture: disabledImage);

            var view = new ButtonView();
            var controller = new ButtonController(model, view);

            return controller;
        }

        public ImageController MakeImage(string imageName, Vector2 position)
        {
            var model = new ImageModel(position, imageName);
            var view = new ImageView();

            var controller = new ImageController(model, view);

            return controller;
        }

        public LabelController MakeLabel(string text, string backgroundImage, Vector2 position, Color textColor, string font)
        {
            var model = new LabelModel(
                text: text,
                backgroundName: backgroundImage,
                position: position,
                color: textColor,
                font: font);

            var view = new LabelView();

            var controller = new LabelController(model, view);

            return controller;
        }
    }
}
