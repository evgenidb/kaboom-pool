﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.UI
{
    public class ImageModel : UIComponent
    {
        #region Properties
        public string ImageName { get; set; }
        #endregion


        #region Constructors
        public ImageModel(Vector2 position, string imageName)
            : base(position)
        {
            ImageName = imageName;
        }
        #endregion
    }
}
