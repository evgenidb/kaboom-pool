﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.SpecialInputCommands
{
    public class CompositeInputCommand : IInputCommand
    {
        #region Properties
        private HashSet<IInputCommand> CompositeCommand { get; } =
            new HashSet<IInputCommand>();

        public IReadOnlyCollection<IInputCommand> Commands
        {
            get { return CompositeCommand; }
        }
        #endregion

        #region Constructors
        public CompositeInputCommand(IEnumerable<IInputCommand> commands)
        {
            foreach (var command in commands)
            {
                AddCommand(command);
            }

            if (Commands.Count < 1)
            {
                var exceptionMessage = string.Join(" ",
                    "No commands are passed to the constructor",
                    "or they are null commands.",
                    "At least one non-null command is required!");

                throw new ArgumentException(
                    message: exceptionMessage,
                    paramName: nameof(commands));
            }
        }

        public CompositeInputCommand(params IInputCommand[] commandsArray)
            : this(commands: commandsArray)
        { }
        #endregion

        #region Public Methods
        public bool Check(InputManager input, CheckInputArgs args = null)
        {
            foreach (var command in CompositeCommand)
            {
                if (!command.Check(input, args))
                {
                    return false;
                }
            }

            return true;
        }

        public bool Check(InputManager input, IEnumerable<CheckInputArgs> argsCollection)
        {
            return Check(input);
        }
        #endregion

        #region Private Methods
        private void AddCommand(IInputCommand command)
        {
            if (!(command is NullInputCommand))
            {
                CompositeCommand.Add(command);
            }
        }
        #endregion
    }
}
