﻿using KaboomPoolGame.Source.Models.Controls;

namespace KaboomPoolGame.Source.Models.Options
{
    public class MenuOptions
    {
        public int FPS { get; set; } = 60;
        public string DefaultLabelFont { get; set; } = "Default Menu Font";
        
        public MenuControls Controls { get; set; }
    }
}
