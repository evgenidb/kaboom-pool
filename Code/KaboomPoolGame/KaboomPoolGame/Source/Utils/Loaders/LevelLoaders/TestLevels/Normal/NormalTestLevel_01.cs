﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaboomPoolGame.Source.Models;

namespace KaboomPoolGame.Source.Utils.Loaders.LevelLoaders.TestLevels.Normal
{
    public class NormalTestLevel_01 : ITestLevel
    {
        #region Properties
        public string Name { get; } = "Normal Level 01";

        public LevelData Data { get; }
        #endregion

        #region Constructors
        public NormalTestLevel_01()
        {
            var level = new LevelData(
                difficulty: "Normal",
                name: Name);

            // Level Initialization Code.
            // Do it like in EasyTestLevel_01

            Data = level;
        }
        #endregion
    }
}
