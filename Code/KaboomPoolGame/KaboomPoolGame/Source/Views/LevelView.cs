﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Models;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Views
{
    public class LevelView : IView<LevelModel>
    {
        #region Fields
        private LevelModel model;
        #endregion

        #region Properties
        protected LevelModel Model
        {
            get
            {
                return model;
            }
            set
            {
                if (value != null)
                {
                    model = value;
                }
                else
                {
                    throw new ArgumentNullException(
                        message: "Menu Model cannot be null",
                        paramName: nameof(Model));
                }
            }
        }
        public bool IsContentLoaded { get; private set; } = false;

        private Dictionary<string, Texture2D> TerrainTextures { get; } =
            new Dictionary<string, Texture2D>();

        private Dictionary<string, Texture2D> WallTextures { get; } =
            new Dictionary<string, Texture2D>();

        private Dictionary<string, Texture2D> BallTextures { get; } =
            new Dictionary<string, Texture2D>();

        private Dictionary<string, Texture2D> PlayerTextures { get; } =
            new Dictionary<string, Texture2D>();
        #endregion

        #region Constructors
        public LevelView() { }
        #endregion

        #region Public Methods
        public void Initialize(LevelModel model)
        {
            Model = model;
            if (IsContentLoaded)
            {
                UnloadContent();
                LoadContent();
            }
        }

        public void LoadContent()
        {
            // Load Terrain Content
            foreach (var terrain in Model.Terrain)
            {
                terrain.LoadContent();
            }

            // Load Wall Content
            foreach (var wall in Model.Walls)
            {
                wall.LoadContent();
            }

            // Load Obstacle Content
            foreach (var obstacle in Model.Obstacles)
            {
                obstacle.LoadContent();
            }

            // Load Target Content
            foreach (var target in Model.Targets)
            {
                target.LoadContent();
            }

            // Load Balls Content
            foreach (var ball in Model.Balls)
            {
                ball.LoadContent();
            }

            // Load Bomb Content
            foreach (var bomb in Model.Bombs)
            {
                bomb.LoadContent();
            }
            
            Model.Player.LoadContent();

            Model.Effects.LoadContent();

            IsContentLoaded = true;
        }

        public void UnloadContent()
        {
            // Unload Terrain Content
            foreach (var terrain in Model.Terrain)
            {
                terrain.UnloadContent();
            }

            // Unload Wall Content
            foreach (var wall in Model.Walls)
            {
                wall.UnloadContent();
            }

            // Unload Obstacle Content
            foreach (var obstacle in Model.Obstacles)
            {
                obstacle.UnloadContent();
            }

            // Unload Target Content
            foreach (var target in Model.Targets)
            {
                target.UnloadContent();
            }

            // Unload Ball Content
            foreach (var ball in Model.Balls)
            {
                ball.UnloadContent();
            }

            // Unload Bomb Content
            foreach (var bomb in Model.Bombs)
            {
                bomb.UnloadContent();
            }
            
            Model.Player.UnloadContent();

            Model.Effects.UnloadContent();

            IsContentLoaded = false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsContentLoaded)
            {
                // Draw Terrain
                foreach (var terrain in Model.Terrain)
                {
                    terrain.Draw(spriteBatch);
                }

                // Draw Walls
                foreach (var wall in Model.Walls)
                {
                    wall.Draw(spriteBatch);
                }

                // Draw Obstacles
                foreach (var obstacle in Model.Obstacles)
                {
                    obstacle.Draw(spriteBatch);
                }

                // Draw Targets
                foreach (var target in Model.Targets)
                {
                    target.Draw(spriteBatch);
                }

                // Draw Balls
                foreach (var ball in Model.Balls)
                {
                    ball.Draw(spriteBatch);
                }

                // Draw Bombs
                foreach (var bomb in Model.Bombs)
                {
                    bomb.Draw(spriteBatch);
                }

                // Draw Player
                Model.Player.Draw(spriteBatch);

                // Draw Effects
                Model.Effects.Draw(spriteBatch);
            }
        }
        #endregion
    }
}
