﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Controllers.UI;
using KaboomPoolGame.Source.Models;
using KaboomPoolGame.Source.States.Game;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.States.Menu
{
    public class PauseGameMenuState : MenuState
    {
        #region Properties
        public PlayState PlayState { get; set; } = null;
        #endregion


        #region Constructors
        public PauseGameMenuState()
            : base("Pause Game Menu")
        { }
        #endregion


        #region Public Methods
        public override void Enter()
        {
            base.Enter();

            PlayState = null;
            Menu.LoadContent();
        }

        public override void Enter(GameState oldState)
        {
            base.Enter(oldState);

            if (oldState is PlayState)
            {
                PlayState = oldState as PlayState;
            }
            
            Menu.LoadContent();
        }

        public override void Leave()
        {
            UnloadPlayStateContent();

            Menu.UnloadContent();
        }

        public override void Leave(GameState newState)
        {
            if (!(newState is PlayState))
            {
                UnloadPlayStateContent();
            }

            Menu.UnloadContent();
        }

        public override void LoadContent()
        {
            Menu.LoadContent();
        }

        public override void UnloadContent()
        {
            Menu.UnloadContent();
        }

        public override void HandleInput(InputManager input)
        {
            base.HandleInput(input);

            var controls = GameServices.MenuOptions.Controls;
            if (controls.ResumeGame.Check(input))
            {
                ChooseAction("Resume");
            }
        }

        public override void Update(GameTime gameTime)
        {
            Menu.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            Menu.Draw(spriteBatch);
        }
        #endregion


        #region Protected Methods
        protected override void InitializeImplementation()
        {
            var factory = GameServices.UIFactory;
            var buttons = new List<ButtonController>
            {
                factory.MakeButton("Resume", new Vector2(200, 200), "Buttons/Resume Game button - Normal State", "Buttons/Resume Game button - Hover State", "Buttons/Resume Game button - Click State", "Buttons/Resume Game button - Disabled"),
                factory.MakeButton("Restart", new Vector2(200, 300), "Buttons/Restart Game button - Normal State", "Buttons/Restart Game button - Hover State", "Buttons/Restart Game button - Click State", "Buttons/Restart Game button - Disabled"),
                factory.MakeButton("Leave Game", new Vector2(200, 400), "Buttons/Leave Game button - Normal State", "Buttons/Leave Game button - Hover State", "Buttons/Leave Game button - Click State", "Buttons/Leave Game button - Disabled"),
            };

            var labels = new List<LabelController>
            {
                factory.MakeLabel("Pause", "Backgrounds/Transparent Background", new Vector2(200, 50), Color.White, "Menu/Menu Title Font"),
            };

            var images = new List<ImageController>();

            var model = new MenuModel("Backgrounds/Menu Background");
            model.Buttons.AddRange(buttons);
            model.Labels.AddRange(labels);
            model.Images.AddRange(images);

            model.SelectedButton.Hover();

            var view = new MenuView();
            Menu = new MenuController(model, view);
        }

        protected override void ChooseAction(string actionName)
        {
            var stateManager = GameServices.GameStateManager;

            if (actionName == "Resume")
            {
                stateManager.SwitchState("Play State");
            }
            else if (actionName == "Restart")
            {
                var level = (PlayState) stateManager.GetState("Play State");
                level.Restart();
                stateManager.SwitchState("Play State");
            }
            else if (actionName == "Leave Game")
            {
                stateManager.SwitchState("Main Menu");
            }
            else
            {
                throw new ArgumentOutOfRangeException(
                    message: "Unhandled button/action",
                    paramName: nameof(actionName),
                    actualValue: actionName);
            }
        }
        #endregion


        #region Private Methods
        private void UnloadPlayStateContent()
        {
            PlayState?.UnloadContent();
            PlayState = null;
        }
        #endregion
    }
}
