﻿using System;
using KaboomPoolGame.Source.Models.UI;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.Loaders;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Views.UI
{
    public class ImageView : UIView<ImageModel>, IView<ImageModel>
    {
        #region Properties
        private Texture2D Image { get; set; }
        #endregion

        #region Constructors
        public ImageView() : base() { }
        #endregion

        #region Public Methods
        protected override void LoadContentImplementation(ContentLoader contentLoader)
        {
            Image = contentLoader.LoadImage(Model.ImageName);

            Model.Dimensions =
                Image.Bounds.Size.ToVector2();
        }

        protected override void UnloadContentImplementation()
        {
            Image = null;

            Model.Dimensions = Vector2.Zero;
        }

        protected override void DrawImplementation(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                texture: Image,
                position: Model.Position,
                origin: Model.Center);
        }
        #endregion
    }
}
