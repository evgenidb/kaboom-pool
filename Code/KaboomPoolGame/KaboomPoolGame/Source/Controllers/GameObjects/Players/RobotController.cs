﻿using KaboomPoolGame.Source.Enums.GameObjects;
using KaboomPoolGame.Source.Models.GameObjects.Players;
using KaboomPoolGame.Source.Views.GameObjects.Players;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.GameObjects.Players
{
    public class RobotController : GameObjectController<Robot, RobotView>
    {
        #region Constants
        private const double DiagonalSpeedMultiplier = 0.7;
        #endregion

        #region Constructors
        public RobotController(Robot model, RobotView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public override void Update(GameTime gameTime)
        {
            var elapsed =
                gameTime.ElapsedGameTime.TotalSeconds;

            Model.Position +=
                (Model.Direction * (float)elapsed);

            // Update Sprite
            Model.Sprite.Update(gameTime);

            Model.CurrentState = PlayerState.Idle;
            Model.Direction = Vector2.Zero;
        }

        public void MoveUp()
        {
            var speed = (float)Model.MaxSpeed;
            Model.Direction += new Vector2(
                x: 0,
                y: -speed);

            Model.CurrentState = PlayerState.MoveUp;
        }

        public void MoveDown()
        {
            var speed = (float)Model.MaxSpeed;
            Model.Direction += new Vector2(
                x: 0,
                y: speed);

            Model.CurrentState = PlayerState.MoveDown;
        }

        public void MoveLeft()
        {
            var speed = (float)Model.MaxSpeed;
            Model.Direction += new Vector2(
                x: -speed,
                y: 0);

            Model.CurrentState = PlayerState.MoveLeft;
        }

        public void MoveRight()
        {
            var speed = (float)Model.MaxSpeed;
            Model.Direction += new Vector2(
                x: speed,
                y: 0);

            Model.CurrentState = PlayerState.MoveRight;
        }

        public void MoveUpRight()
        {
            var speed = (float)(Model.MaxSpeed * DiagonalSpeedMultiplier);
            Model.Direction += new Vector2(
                x: speed,
                y: -speed);

            Model.CurrentState = PlayerState.MoveUpRight;
        }

        public void MoveUpLeft()
        {
            var speed = (float)(Model.MaxSpeed * DiagonalSpeedMultiplier);
            Model.Direction += new Vector2(
                x: -speed,
                y: -speed);

            Model.CurrentState = PlayerState.MoveUpLeft;
        }

        public void MoveDownRight()
        {
            var speed = (float)(Model.MaxSpeed * DiagonalSpeedMultiplier);
            Model.Direction += new Vector2(
                x: speed,
                y: speed);

            Model.CurrentState = PlayerState.MoveDownRight;
        }

        public void MoveDownLeft()
        {
            var speed = (float)(Model.MaxSpeed * DiagonalSpeedMultiplier);
            Model.Direction += new Vector2(
                x: -speed,
                y: speed);

            Model.CurrentState = PlayerState.MoveDownLeft;
        }
        #endregion
    }
}
