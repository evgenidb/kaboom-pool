﻿namespace KaboomPoolGame.Source.Utils.MathUtils.Shapes
{
    public class NullShape : IShape
    {
        #region Constructors
        public NullShape() { }
        #endregion

        #region Public Methods
        public bool CollidesWith(IShape other)
        {
            return false;
        }

        public bool IsInsideOf(IShape other)
        {
            return false;
        }
        #endregion
    }
}
