﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Models.GameObjects.Players;
using KaboomPoolGame.Source.Utils;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Views.GameObjects.Players
{
    public class RobotView : IView<Robot>
    {
        #region Fields
        private Robot model;
        #endregion

        #region Properties
        public bool IsContentLoaded { get; private set; }

        private Robot Model
        {
            get
            {
                return model;
            }
            set
            {
                if (value != null)
                {
                    model = value;
                }
                else
                {
                    throw new ArgumentNullException(
                        message: "Robot Model cannot be null",
                        paramName: nameof(Model));
                }
            }
        }

        private Dictionary<string, Texture2D> Textures { get; } =
            new Dictionary<string, Texture2D>();
        #endregion

        #region Public Methods
        public void Initialize(Robot model)
        {
            Model = model;
            if (IsContentLoaded)
            {
                UnloadContent();
                LoadContent();
            }
        }

        public void LoadContent()
        {
            var content = GameServices.ContentLoader;

            foreach (var spriteName in Model.GetSpriteNames())
            {
                var texture = content.LoadImage(spriteName);
                Textures[spriteName] = texture;
            }

            IsContentLoaded = true;
        }

        public void UnloadContent()
        {
            IsContentLoaded = false;

            Textures.Clear();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsContentLoaded)
            {
                var layerDepth = GameServices.Utils
                    .GetLayerDepth(Model.Layer);

                var textureName = Model.Sprite.TextureName;
                var texture = Textures[textureName];

                spriteBatch.Draw(
                    texture: texture,
                    position: Model.Position,
                    origin: Model.Origin,
                    rotation: (float)Model.Rotation,
                    scale: Model.Scale,
                    layerDepth: (float)layerDepth);
            }
        }
        #endregion
    }
}