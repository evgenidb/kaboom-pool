﻿using KaboomPoolGame.Source.Models.GameObjects.Effects;
using KaboomPoolGame.Source.Views.GameObjects.Effects;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.GameObjects.Effects
{
    public class ExplosionController : GameObjectController<Explosion, ExplosionView>
    {
       #region Constructors
        public ExplosionController(Explosion model, ExplosionView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public override void Update(GameTime gameTime)
        {
            Model.Sprite.Update(gameTime);

            Model.MustRemove =
                Model.MustRemove || Model.Sprite.HasFinished;
        }
        #endregion
    }
}
