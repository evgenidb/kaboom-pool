﻿using KaboomPoolGame.Source.Models.GameObjects.Effects;
using KaboomPoolGame.Source.Views.GameObjects.Effects;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.GameObjects.Effects
{
    public class CraterController : GameObjectController<Crater, CraterView>
    {
        #region Constructors
        public CraterController(Crater model, CraterView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public override void Update(GameTime gameTime)
        {
            Model.Sprite.Update(gameTime);
        }
        #endregion
    }
}
