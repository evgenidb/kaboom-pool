﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Models.UI
{
    public abstract class UIComponent
    {
        #region Properties
        public Vector2 Position { get; set; }
        public Vector2 Dimensions { get; set; }
        public Vector2 Center
        {
            get
            {
                return Dimensions / 2;
            }
        }

        public Rectangle Bounds
        {
            get
            {
                return new Rectangle(
                    Position.ToPoint(), 
                    Dimensions.ToPoint());
            }
        }
        #endregion

        #region Constructors
        public UIComponent(Vector2 position)
            : this(position, dimensions: Vector2.Zero)
        { }

        public UIComponent(Vector2 position, Vector2 dimensions)
        {
            Position = position;
            Dimensions = dimensions;
        }
        #endregion
    }
}
