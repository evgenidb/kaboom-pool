﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.States;
using KaboomPoolGame.Source.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Controllers.Managers
{
    public class GameStateManager
    {
        #region Properties
        private GameState CurrentState { get; set; } = null;
        private Dictionary<string, GameState> States { get; } =
            new Dictionary<string, GameState>();
        #endregion

        #region Constructors
        public GameStateManager() { }

        public GameStateManager(IEnumerable<GameState> states)
            : this()
        {
            AddStates(states);
        }
        #endregion

        #region Public Methods
        public void Initialize(string initialStateName)
        {
            CurrentState = GetState(initialStateName);

            if (!CurrentState.IsInitialized)
            {
                CurrentState.Initialize();
            }

            CurrentState.Enter();
        }

        public GameState GetState(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException(
                    message: "The name of the state is invalid; it cannot be empty or null.",
                    paramName: nameof(name));
            }
            if (States.ContainsKey(name))
            {
                return States[name];
            }
            else
            {
                throw new ArgumentException(
                    message: $"Not found state with name {name}.",
                    paramName: nameof(name));
            }
        }

        public void AddState(GameState state)
        {
            if (state == null)
            {
                throw new ArgumentNullException(
                    message: "State cannot be null.",
                    paramName: nameof(state));
            }
            var stateName = state.Name;
            if (string.IsNullOrWhiteSpace(stateName))
            {
                throw new ArgumentException(
                    message: "The name of the state is invalid; it cannot be empty or null.",
                    paramName: nameof(state));
            }
            if (States.ContainsKey(stateName))
            {
                throw new ArgumentException(
                    message: $"Already exists game state with name {stateName}.",
                    paramName: nameof(state));
            }

            States[stateName] = state;
        }

        public void AddStates(IEnumerable<GameState> states)
        {
            foreach (var state in states)
            {
                AddState(state);
            }
        }

        public void SwitchState(string newStateName)
        {
            var newState = GetState(newStateName);
            if (!newState.IsInitialized)
            {
                newState.Initialize();
            }

            var oldState = CurrentState;
            oldState.Leave(newState);
            CurrentState = newState;
            newState.Enter(oldState);
        }

        public void ExitGame()
        {
            CurrentState.UnloadContent();
            GameServices.Game.Exit();
        }

        public void HandleInput(InputManager input)
        {
            CurrentState.HandleInput(input);
        }

        public void Update(GameTime gameTime)
        {
            CurrentState.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            CurrentState.Draw(spriteBatch);
        }
        #endregion
    }
}
