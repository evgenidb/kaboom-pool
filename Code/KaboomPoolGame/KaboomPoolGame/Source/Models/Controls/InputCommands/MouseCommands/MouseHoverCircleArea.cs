﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;
using KaboomPoolGame.Source.Utils;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.MouseCommands
{
    public class MouseHoverCircleArea : IInputCommand
    {
        #region Public Methods
        public bool Check(InputManager input, CheckInputArgs args = null)
        {
            if (args == null)
            {
                var exceptionMessage = string.Join(" ",
                    "Input args is a required parameter.");

                throw new ArgumentNullException(
                    message: exceptionMessage,
                    paramName: nameof(args));
            }
            var area = (CircleAreaArgs) args;
            var point = input.CurrentMousePosition();

            var math = GameServices.Geometry;

            return math.IsPointInsideCircle(
                point: point,
                circleCenter: area.Center,
                radius: area.Radius);
        }

        public bool Check(InputManager input, IEnumerable<CheckInputArgs> argsCollection)
        {
            foreach (var arg in argsCollection)
            {
                if (arg is CircleAreaArgs)
                {
                    return Check(input, arg);
                }
            }

            // No CircleAreaArgs was passed
            var exceptionMessage = string.Join(" ",
                $"{nameof(CircleAreaArgs)} is required",
                "but is not found in the args collection.");

            throw new ArgumentException(
                message: exceptionMessage,
                paramName: nameof(argsCollection));
        }

        public override string ToString()
        {
            return "Mouse hover";
        }
        #endregion
    }
}
