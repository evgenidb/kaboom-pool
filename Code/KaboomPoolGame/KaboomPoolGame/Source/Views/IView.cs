﻿using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Views
{
    public interface IView<TModel>
    {
        #region Properties
        bool IsContentLoaded { get; }
        #endregion

        #region Methods
        void Initialize(TModel model);
        void LoadContent();
        void UnloadContent();
        void Draw(SpriteBatch spriteBatch);
        #endregion
    }
}
