﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Models.UI
{
    public class LabelModel : UIComponent
    {
        #region Properties
        public string Text { get; set; }
        public Color TextColor { get; set; }
        public string FontName { get; set; }
        public string BackgroundName { get; set; }
        #endregion

        #region Constructors
        public LabelModel(Vector2 position, string text, string backgroundName, string font, Color color)
            : this(position, Vector2.Zero, text, backgroundName, font, color)
        { }

        public LabelModel(Vector2 position, Vector2 dimensions, string text, string backgroundName, string font, Color color)
            : base(position, dimensions)
        {
            Text = text;
            TextColor = color;
            FontName = font;
            BackgroundName = backgroundName;
        }
        #endregion
    }
}
