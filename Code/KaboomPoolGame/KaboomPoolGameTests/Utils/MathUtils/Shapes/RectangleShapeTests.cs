﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using KaboomPoolGame.Source.Utils.MathUtils.Shapes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KaboomPoolGameTests.Utils.MathUtils.Shapes
{
    [TestClass]
    public class RectangleShapeTests
    {
        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            GameServices.Geometry = new GeometryUtil();
            GameServices.Math = new MathUtil();
        }
        #endregion


        #region Test Methods

        #region Test IsInside - Non-rotated rectangle
        [TestMethod]
        public void TestIsInside_RectangleShapeInsideNullShape()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new NullShape();

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeInsidePoint()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new PointShape(0, 0);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeInsideCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new CircleShape(
                x: 10,
                y: 10,
                radius: 50);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeOutsideCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new CircleShape(
                x: 100,
                y: 100,
                radius: 50);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeCollidesCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new CircleShape(
                x: 50,
                y: 0,
                radius: 50);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeContainsCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new CircleShape(
                x: 3,
                y: 2,
                radius: 1);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeInsideRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 5,
                y: 8,
                width: 200,
                height: 100);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeOutsideRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 50,
                y: 2,
                width: 20,
                height: 10);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeCollidesRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 5,
                y: -2,
                width: 20,
                height: 10);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeContainsRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 8,
                y: 1,
                width: 1,
                height: 2);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeCrossesRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 0,
                y: 0,
                width: 12,
                height: 24);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeInsideRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 2,
                y: 1,
                width: 32,
                height: 30,
                rotation: Radians(30));

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeOutsideRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 50,
                y: 2,
                width: 20,
                height: 10,
                rotation: Radians(30));

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeCollidesRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 5,
                y: -2,
                width: 20,
                height: 10,
                rotation: Radians(5));

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeContainsRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 1,
                height: 2,
                rotation: Radians(30));

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RectangleShapeCrossesRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 0,
                y: 0,
                width: 10,
                height: 20,
                rotation: Radians(10));

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Test IsInside - Rotated rectangle
        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeInsideNullShape()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new NullShape();

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeInsidePoint()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new PointShape(0, 0);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeInsideCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new CircleShape(
                x: 10,
                y: 10,
                radius: 50);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeOutsideCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new CircleShape(
                x: 100,
                y: 100,
                radius: 50);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeCollidesCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new CircleShape(
                x: 50,
                y: 0,
                radius: 50);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeContainsCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new CircleShape(
                x: 3,
                y: 2,
                radius: 1);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeInsideRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 5,
                y: 8,
                width: 22,
                height: 40);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeOutsideRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 50,
                y: 2,
                width: 20,
                height: 10);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeCollidesRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 5,
                y: -2,
                width: 20,
                height: 10);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeContainsRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 1,
                y: 8,
                width: 1,
                height: 2);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeCrossesRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 0,
                y: 0,
                width: 24,
                height: 12);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeInsideRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 5,
                y: 8,
                width: 200,
                height: 100,
                rotation: Radians(30));

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeOutsideRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 50,
                y: 2,
                width: 94,
                height: 10,
                rotation: Radians(2));

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeCollidesRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 5,
                y: -2,
                width: 20,
                height: 10,
                rotation: Radians(5));

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeContainsRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 0,
                y: 8,
                width: 1,
                height: 2,
                rotation: Radians(30));

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsInside_RotatedRectangleShapeCrossesRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 0,
                y: 0,
                width: 30,
                height: 16,
                rotation: Radians(10));

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Test CollidesWith - Non-rotated rectangle
        [TestMethod]
        public void TestCollidesWith_RectangleShapeInsideNullShape()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new NullShape();

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeContainsPoint()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new PointShape(0, 0);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeOutsideLeftPoint()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new PointShape(-10.1, 0);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeOutsideRightPoint()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new PointShape(10.1, 0);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeOutsideTopPoint()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new PointShape(0, -10.1);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeOutsideBottomPoint()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new PointShape(0, 10.1);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeInsideCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new CircleShape(
                x: 10,
                y: 10,
                radius: 50);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeOutsideCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new CircleShape(
                x: 100,
                y: 100,
                radius: 50);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeCollidesCircleWithSide()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new CircleShape(
                x: 50,
                y: 0,
                radius: 41);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeCollidesCircleWithVertex()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new CircleShape(
                x: 50,
                y: 5,
                radius: 41);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeContainsCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new CircleShape(
                x: 3,
                y: 2,
                radius: 1);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeInsideRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 5,
                y: 8,
                width: 200,
                height: 100);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeOutsideRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 50,
                y: 2,
                width: 20,
                height: 10);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeCollidesRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 5,
                y: -2,
                width: 20,
                height: 10);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeContainsRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 8,
                y: 1,
                width: 1,
                height: 2);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeCrossesRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 0,
                y: 0,
                width: 12,
                height: 24);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeInsideRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 2,
                y: 1,
                width: 32,
                height: 30,
                rotation: Radians(30));

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeOutsideRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 50,
                y: 2,
                width: 20,
                height: 10,
                rotation: Radians(30));

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeCollidesRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 5,
                y: -2,
                width: 20,
                height: 10,
                rotation: Radians(5));

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeContainsRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 1,
                height: 2,
                rotation: Radians(30));

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RectangleShapeCrossesRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10);

            var otherShape = new RectangleShape(
                x: 0,
                y: 0,
                width: 10,
                height: 20,
                rotation: Radians(10));

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }
        #endregion

        #region Test CollidesWith - Rotated rectangle
        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeInsideNullShape()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new NullShape();

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeContainesPoint()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new PointShape(0, 0);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeInsideCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new CircleShape(
                x: 10,
                y: 10,
                radius: 50);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeOutsideCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new CircleShape(
                x: 100,
                y: 100,
                radius: 50);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeCollidesCircleWithSide()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new CircleShape(
                x: 50,
                y: 0,
                radius: 46);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeCollidesCircleWithVertex()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new CircleShape(
                x: 50,
                y: 10,
                radius: 46);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeContainsCircle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new CircleShape(
                x: 3,
                y: 2,
                radius: 1);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeInsideRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 5,
                y: 8,
                width: 22,
                height: 40);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeOutsideRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 50,
                y: 2,
                width: 20,
                height: 10);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeCollidesRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 5,
                y: -2,
                width: 20,
                height: 10);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeContainsRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 1,
                y: 8,
                width: 1,
                height: 2);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeCrossesRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 0,
                y: 0,
                width: 24,
                height: 12);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeInsideRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 5,
                y: 8,
                width: 200,
                height: 100,
                rotation: Radians(30));

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeOutsideRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 50,
                y: 2,
                width: 89,
                height: 10,
                rotation: Radians(2));

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeCollidesRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 50,
                y: 2,
                width: 89.8,
                height: 10,
                rotation: Radians(5));

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeContainsRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 0,
                y: 8,
                width: 1,
                height: 2,
                rotation: Radians(30));

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestCollidesWith_RotatedRectangleShapeCrossesRotatedRectangle()
        {
            // Assign
            var shape = new RectangleShape(
                x: 0,
                y: 0,
                width: 20,
                height: 10,
                rotation: Radians(90));

            var otherShape = new RectangleShape(
                x: 0,
                y: 0,
                width: 30,
                height: 16,
                rotation: Radians(10));

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }
        #endregion

        #endregion


        #region Helper Methods
        private double Radians(double degrees)
        {
            return GameServices.Geometry.DegreeToRadian(degrees);
        }
        #endregion
    }
}
