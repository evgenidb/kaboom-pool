﻿using System;
using System.Collections.Generic;
using System.Linq;
using KaboomPoolGame.Source.Controllers.Managers;
using KaboomPoolGame.Source.Models.Controls.InputCommands.CheckInputAguments;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Enums;
using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;
using Microsoft.Xna.Framework.Input;

namespace KaboomPoolGame.Source.Models.Controls.InputCommands.KeyboardCommands
{
    public class BasicKeyCommand : IInputCommand
    {
        #region Fields
        private RequiredCommandState requiredState;
        #endregion

        #region Properties
        private Keys Key { get; }
        private RequiredCommandState RequiredState
        {
            get { return requiredState; }
            set
            {
                var validStates = new[]
                {
                    RequiredCommandState.Press,
                    RequiredCommandState.Release,
                    RequiredCommandState.Up,
                };
                if (validStates.Contains(value))
                {
                    requiredState = value;
                }
                else
                {
                    var statesString = string.Join(", ", validStates);
                    var exceptionMessage = string.Join(" ",
                        "Invalid Key state.",
                        "Valid states are:",
                        $"{statesString}.");

                    throw new ArgumentOutOfRangeException(
                        message: exceptionMessage,
                        paramName: nameof(RequiredState),
                        actualValue: value);
                }
            }
        }
        #endregion

        #region Constructors
        public BasicKeyCommand(Keys key, RequiredCommandState requiredState = RequiredCommandState.Release)
        {
            Key = key;
            RequiredState = requiredState;
        }
        #endregion

        #region Public Methods
        public bool Check(InputManager input, CheckInputArgs args = null)
        {
            switch (RequiredState)
            {
                case RequiredCommandState.Up:
                    return input.IsKeyUp(Key);
                case RequiredCommandState.Press:
                    return input.IsKeyPressed(Key);
                case RequiredCommandState.Release:
                    return input.IsKeyReleased(Key);
                default:
                    break;
            }

            return false;
        }

        public bool Check(InputManager input, IEnumerable<CheckInputArgs> argsCollection)
        {
            return Check(input);
        }

        public override string ToString()
        {
            return $"{RequiredState.ToString()} {Key.ToString()}";
        }
        #endregion
    }
}
