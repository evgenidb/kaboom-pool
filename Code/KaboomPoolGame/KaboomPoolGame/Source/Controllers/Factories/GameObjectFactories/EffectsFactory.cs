﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Models.GameObjects.Effects;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.Factories.GameObjectFactories
{
    public class EffectsFactory
    {
        public Explosion MakeExplosion()
        {
            var frames = new List<string>();

            const int framesCount = 52;
            int digitCount = framesCount.ToString().Length;
            string frameIdFormat = new string('0', digitCount);

            for (int frameNumber = 1; frameNumber <= framesCount; frameNumber++)
            {
                var frameId = frameNumber.ToString(frameIdFormat);
                var frameName = $"Effects/Explosion/Explosion 1 - Frame {frameId}";
                frames.Add(frameName);
            }

            var sprite = new Animation(
                animationName: "Explosion",
                frameDuration: 0.03,
                frames: frames,
                isLooped: false);

            var model = new Explosion(
                position: Vector2.Zero,
                size: new Vector2(60, 60),
                sprite: sprite);

            return model;
        }

        public Crater MakeCrater()
        {
            var sprite = new SpriteData(
                spriteName: "Crater",
                textureName: "Effects/Craters/Crater");

            var model = new Crater(
                position: Vector2.Zero,
                size: new Vector2(20, 20),
                sprite: sprite);

            return model;
        }
    }
}
