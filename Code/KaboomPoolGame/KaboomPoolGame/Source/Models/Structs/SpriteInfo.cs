﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KaboomPoolGame.Source.Models.Structs
{
    public struct SpriteInfo
    {
        public string Name { get; }
        public Texture2D Spritesheet { get; }
        public Rectangle Location { get; }

        public SpriteInfo(string name, Rectangle location, Texture2D spriteSheet)
        {
            Name = name;
            Location = location;
            Spritesheet = spriteSheet;
        }
    }
}
