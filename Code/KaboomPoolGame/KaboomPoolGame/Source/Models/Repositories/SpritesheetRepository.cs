﻿using System;
using System.Collections.Generic;
using KaboomPoolGame.Source.Models.Structs;

namespace KaboomPoolGame.Source.Models.Repositories
{
    public class SpritesheetRepository
    {
        #region Properties
        private Dictionary<string, List<SpritesheetInfo>> Spritesheets { get;  } =
            new Dictionary<string, List<SpritesheetInfo>>();

        private HashSet<string> LoadedSpritesheets { get; } =
            new HashSet<string>();
        #endregion


        #region Constructors
        public SpritesheetRepository() { }
        #endregion


        #region Public Methods
        public void LoadContent(params string[] categories)
        {
            LoadContent((IEnumerable<string>) categories);
        }

        public void LoadContent(IEnumerable<string> categories)
        {
            foreach (var category in categories)
            {
                if (IsCategoryLoaded(category))
                {
                    continue;
                }

                var spritesheets = Spritesheets[category];
                foreach (var spritesheet in spritesheets)
                {
                    spritesheet.LoadContent();
                }

                LoadedSpritesheets.Add(category);
            }
        }

        public void UnloadContent(params string[] categories)
        {
            UnloadContent((IEnumerable<string>) categories);
        }

        public void UnloadContent(IEnumerable<string> categories)
        {
            foreach (var category in categories)
            {
                if (!IsCategoryLoaded(category))
                {
                    continue;
                }

                var spritesheets = Spritesheets[category];
                foreach (var spritesheet in spritesheets)
                {
                    spritesheet.UnloadContent();
                }

                LoadedSpritesheets.Remove(category);
            }
        }

        public void AddSpritesheet(SpritesheetInfo spritesheet)
        {
            var category = spritesheet.Category;

            if (!Spritesheets.ContainsKey(category))
            {
                Spritesheets[category] =
                    new List<SpritesheetInfo>();
            }

            Spritesheets[category]
                .Add(spritesheet);

            if (IsCategoryLoaded(category))
            {
                spritesheet.LoadContent();
            }
        }

        public void RemoveSpritesheet(SpritesheetInfo spritesheet)
        {
            var category = spritesheet.Category;

            if (IsCategoryLoaded(category))
            {
                spritesheet.UnloadContent();
            }
            
            Spritesheets.Remove(category);
        }

        public SpriteInfo Get(string category, string image)
        {
            if (!Spritesheets.ContainsKey(category))
            {
                throw new ArgumentOutOfRangeException(
                    message: $"Category not found.",
                    paramName: nameof(category),
                    actualValue: category);
            }

            var imageNameParts = image.Split('/');
            string imageName =
                imageNameParts[imageNameParts.Length - 1];

            foreach (var spritesheet in Spritesheets[category])
            {
                if (spritesheet.HasLocation(imageName))
                {
                    var sprite = new SpriteInfo(
                        name: imageName,
                        location: spritesheet.GetLocation(imageName),
                        spriteSheet: spritesheet.Spritesheet);

                    return sprite;
                }
            }

            // Image not found in this category
            throw new ArgumentOutOfRangeException(
                message: $"Image {image} not found in category {category}.",
                paramName: nameof(image),
                actualValue: image);
        }
        #endregion

        #region Private Methods
        private bool IsCategoryLoaded(string category)
        {
            return LoadedSpritesheets.Contains(category);
        }
        #endregion
    }
}
