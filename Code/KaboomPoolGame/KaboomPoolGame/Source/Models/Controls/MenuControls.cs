﻿using KaboomPoolGame.Source.Models.Controls.InputCommands.Interfaces;
using KaboomPoolGame.Source.Models.Controls.InputCommands.SpecialInputCommands;

namespace KaboomPoolGame.Source.Models.Controls
{
    public class MenuControls
    {
        public bool IsMouseVisible { get; set; } = false;

        public IInputCommand PreviousMenuItem { get; set; } = new NullInputCommand();
        public IInputCommand NextMenuItem { get; set; } = new NullInputCommand();
        public IInputCommand ClickMenuItem { get; set; } = new NullInputCommand();

        public IInputCommand ResumeGame { get; set; } = new NullInputCommand();
    }
}
