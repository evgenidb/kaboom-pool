﻿using System;
using KaboomPoolGame.Source.Models.GameObjects.Terrain;
using KaboomPoolGame.Source.Views.GameObjects.Terrain;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Controllers.GameObjects.Terrain
{
    public class TerrainController : GameObjectFlyweightController<TerrainType, TerrainView>
    {
        #region Constructors
        public TerrainController(TerrainType model, TerrainView view)
            : base(model, view)
        { }
        #endregion

        #region Public Methods
        public override void Update(GameTime gameTime)
        {
            // Update Sprites
            foreach (var spriteItem in Model.Sprites)
            {
                spriteItem.Value.Update(gameTime);
            }

            Clean();
        }

        public override void Clean()
        {
            Model.Clean();
        }
        #endregion
    }
}
