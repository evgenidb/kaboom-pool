﻿namespace KaboomPoolGame.Source.Enums.GameObjects
{
    public enum PlayerState
    {
        Idle,
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        MoveUpLeft,
        MoveUpRight,
        MoveDownLeft,
        MoveDownRight,
    }
}