﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.GameObjects.Effects
{
    public class Crater : GameObject, IRenderableModel
    {
        #region Properties
        public Layer Layer { get; } = Layer.GroundEffect;

        public ISpriteData Sprite { get; private set; }
        #endregion

        #region Constructors
        public Crater(Vector2 position, Vector2 size, ISpriteData sprite, double rotation = 0, Vector2? scale = default(Vector2?))
            : base("Crater", position, size, rotation, scale)
        {
            Sprite = sprite;
        }
        #endregion

        #region Public Methods
        public IEnumerable<string> GetSpriteNames()
        {
            return Sprite.TextureNames;
        }
        #endregion
    }
}
