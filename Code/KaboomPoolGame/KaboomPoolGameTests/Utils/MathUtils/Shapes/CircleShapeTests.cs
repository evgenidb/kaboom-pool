﻿using System;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Utils.MathUtils;
using KaboomPoolGame.Source.Utils.MathUtils.Shapes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KaboomPoolGameTests.Utils.MathUtils.Shapes
{
    [TestClass]
    public class CircleShapeTests
    {
        #region Initialization and Cleanup
        [ClassInitialize]
        public static void InitializeTests(TestContext context)
        {
            GameServices.Geometry = new GeometryUtil();
            GameServices.Math = new MathUtil();
        }
        #endregion


        #region Test Methods

        #region Test Method IsInsideOf
        [TestMethod]
        public void CircleShapeInsideNullShapeShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new NullShape();

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeInsidePointShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new PointShape(0, 1);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeInsideCircleShouldReturnTrue()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new CircleShape(
                x: 0,
                y: 1,
                radius: 10);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeOutsideCircleShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new CircleShape(
                x: 10,
                y: 11,
                radius: 2);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeNotInsideButCollidesCircleShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new CircleShape(
                x: 10,
                y: 4,
                radius: 3);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeOtherIsInsideCircleShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new CircleShape(
                x: 2,
                y: 2,
                radius: 1);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeInsideRectangleShouldReturnTrue()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 10,
                height: 6);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeOutsideRectangleShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 4,
                y: 11,
                width: 10,
                height: 6);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeNotInsideButCollidesRectangleShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 6.5,
                y: 1,
                width: 10,
                height: 6);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeOtherIsInsideRectangleShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 10);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 2,
                height: 3);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeInsideRotatedRectangleShouldReturnTrue()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 10,
                height: 6,
                rotation: angle);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeOutsideRotatedRectangleShouldReturnFalse()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 5,
                y: 5,
                width: 10,
                height: 6,
                rotation: angle);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeNotInsideButCollidesRotatedRectangleShouldReturnFalse()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 1,
                y: 5.5,
                width: 10,
                height: 6,
                rotation: angle);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeOtherIsInsideRotatedRectangleShouldReturnFalse()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 10);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 2,
                height: 3,
                rotation: angle);

            // Act
            var actual = shape.IsInsideOf(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }
        #endregion

        #region Test Method CollidesWith
        [TestMethod]
        public void CircleShapeCollidesNullShapeShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new NullShape();

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeCollidesPointShouldReturnTrue()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new PointShape(0, 1);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeInsideCollidesCircleShouldReturnTrue()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new CircleShape(
                x: 0,
                y: 1,
                radius: 10);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeNotCollideCircleShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new CircleShape(
                x: 10,
                y: 11,
                radius: 2);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeCollidesCircleShouldReturnTrue()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new CircleShape(
                x: 1,
                y: 6,
                radius: 3);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeCollidesOtherIsInsideCircleShouldReturnTrue()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 4);

            var otherShape = new CircleShape(
                x: 2,
                y: 2,
                radius: 1);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeInsideCollidesRectangleShouldReturnTrue()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 10,
                height: 6);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeNotCollideRectangleShouldReturnFalse()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 4,
                y: 11,
                width: 10,
                height: 6);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeCollidesRectangleShouldReturnTrue()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 6,
                y: 1,
                width: 10,
                height: 6);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeCollidesOtherIsInsideRectangleShouldReturnTrue()
        {
            // Assign
            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 10);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 2,
                height: 3);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeCollidesInsideRotatedRectangleShouldReturnTrue()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 12,
                height: 8,
                rotation: angle);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeNotCollideRotatedRectangleShouldReturnFalse()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new CircleShape(
                x: 1,
                y: 8,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 5,
                y: 5,
                width: 10,
                height: 6,
                rotation: angle);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CircleShapeCollidesRotatedRectangleShouldReturnTrue()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 1);

            var otherShape = new RectangleShape(
                x: 5.5,
                y: 2,
                width: 10,
                height: 6,
                rotation: angle);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CircleShapeCollidesOtherIsInsideRotatedRectangleShouldReturnTrue()
        {
            // Assign
            var angle = Math.PI / 6; // 30 degrees

            var shape = new CircleShape(
                x: 1,
                y: 2,
                radius: 10);

            var otherShape = new RectangleShape(
                x: 0,
                y: 1,
                width: 2,
                height: 3,
                rotation: angle);

            // Act
            var actual = shape.CollidesWith(otherShape);

            // Assert
            Assert.IsTrue(actual);
        }
        #endregion

        #endregion
    }
}