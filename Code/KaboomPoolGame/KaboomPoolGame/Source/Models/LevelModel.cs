﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Controllers.GameObjects.Balls;
using KaboomPoolGame.Source.Controllers.GameObjects.Equipments.Bombs;
using KaboomPoolGame.Source.Controllers.GameObjects.Obstacles;
using KaboomPoolGame.Source.Controllers.GameObjects.Players;
using KaboomPoolGame.Source.Controllers.GameObjects.Terrain;
using KaboomPoolGame.Source.Models.GameObjects.Terrain;
using KaboomPoolGame.Source.Models.Repositories;
using KaboomPoolGame.Source.Utils;
using KaboomPoolGame.Source.Views.GameObjects.Balls;
using KaboomPoolGame.Source.Views.GameObjects.Obstacles;
using KaboomPoolGame.Source.Views.GameObjects.Players;
using KaboomPoolGame.Source.Views.GameObjects.Targets;
using KaboomPoolGame.Source.Views.GameObjects.Terrain;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models
{
    public class LevelModel
    {
        #region Properties
        public LevelData InitialLevelData { get; private set; }

        #region Level Objects
        public ICollection<TerrainController> Terrain { get; } =
            new HashSet<TerrainController>();

        public ICollection<WallController> Walls { get; } =
            new HashSet<WallController>();

        public ICollection<BallController> Balls { get; } =
            new HashSet<BallController>();

        public ICollection<TargetController> Targets { get; } =
            new HashSet<TargetController>();

        public ICollection<ObstacleController> Obstacles { get; } =
            new HashSet<ObstacleController>();

        public ICollection<BombController> Bombs { get; } =
            new HashSet<BombController>();

        public EffectsRepository Effects { get; } =
            new EffectsRepository();

        public RobotController Player { get; private set; }
        #endregion

        #endregion

        #region Constructors
        public LevelModel(LevelData data)
        {
            InitialLevelData = data;
            Initialize();
        }
        #endregion

        #region Public Methods
        public void Initialize()
        {
            var data = InitialLevelData;

            // Terrain
            Terrain.Clear();
            var terrainFactory =
                GameServices.EntityFactories.TerrainFactory;

            foreach (var type in data.Terrain)
            {
                var terrainName = type.Key;
                
                var model = terrainFactory.Make(terrainName);

                foreach (var location in type.Value)
                {
                    var terrainData = new SpecificTerrain(
                        terrainName: terrainName,
                        position: location.ToVector2(),
                        size: new Vector2(20, 20),
                        rotation: location.Rotation);

                    model.AddData(terrainData);
                }

                var controller = new TerrainController(
                    model: model,
                    view: new TerrainView());
                
                Terrain.Add(controller);
            }

            // Walls
            Walls.Clear();
            var wallFactory =
                GameServices.EntityFactories.WallFactory;

            foreach (var type in data.Walls)
            {
                var wallName = type.Key;

                var model = wallFactory.Make(wallName);

                foreach (var location in type.Value)
                {
                    var wallData = new SpecificWall(
                        wallName: wallName,
                        position: location.ToVector2(),
                        size: new Vector2(20, 20),
                        rotation: location.Rotation);

                    model.AddData(wallData);
                }

                var controller = new WallController(
                    model: model,
                    view: new WallView());
                
                Walls.Add(controller);
            }

            // Obstacles
            Obstacles.Clear();
            var objectFactory =
                GameServices.EntityFactories.ObjectFactory;

            foreach (var type in data.Obstacles)
            {
                var objectName = type.Key;

                foreach (var location in type.Value)
                {
                    var model =
                        objectFactory.MakeObstacle(objectName);

                    model.Position = location.ToVector2();
                    model.Rotation = location.Rotation;

                    var controller = new ObstacleController(
                        model: model,
                        view: new ObstacleView());

                    Obstacles.Add(controller);
                }
            }


            // Player
            var playerFactory =
                GameServices.EntityFactories.PlayerFactory;

            var robotModel = playerFactory.MakeRobot();

            var playerLocation = data.Player;
            robotModel.Position = playerLocation.ToVector2();
            robotModel.Rotation = playerLocation.Rotation;

            Player = new RobotController(
                model: robotModel,
                view: new RobotView());

            // Balls
            Balls.Clear();

            foreach (var ballData in data.Balls)
            {
                var model = objectFactory.MakeBall();

                model.Position = ballData.ToVector2();
                model.Rotation = ballData.Rotation;

                var controller = new BallController(
                    model: model,
                    view: new BallView());

                Balls.Add(controller);
            }

            // Targets
            Targets.Clear();
            var targetFactory =
                GameServices.EntityFactories.TargetFactory;

            foreach (var targetData in data.Targets)
            {
                var model = targetFactory.MakeTarget();

                model.Position = targetData.ToVector2();
                model.Rotation = targetData.Rotation;

                var controller = new TargetController(
                    model: model,
                    view: new TargetView());

                Targets.Add(controller);
            }

            // Bombs
            Bombs.Clear();

            // Effects
            Effects.ClearAll();
        }
        #endregion
    }
}
