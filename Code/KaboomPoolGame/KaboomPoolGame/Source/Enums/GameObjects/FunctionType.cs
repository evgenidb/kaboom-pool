﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaboomPoolGame.Source.Enums.GameObjects
{
    public enum FunctionType
    {
        Constant,
        Linear,
        Quadratic,
    }
}
