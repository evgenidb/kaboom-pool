﻿using System.Collections.Generic;
using KaboomPoolGame.Source.Enums.Views;
using KaboomPoolGame.Source.Models.Sprites;
using Microsoft.Xna.Framework;

namespace KaboomPoolGame.Source.Models.GameObjects.Balls
{
    public class Ball : GameObject, IRenderableModel, IMovable
    {
        #region Properties
        public Vector2 Direction { get; set; } =
            Vector2.Zero;
        
        public double MaxSpeed { get; set; } = 0;

        public Layer Layer { get; } = Layer.Ball;

        public ISpriteData Sprite { get; private set; }
        #endregion

        #region Constructors
        public Ball(Vector2 position, Vector2 size, ISpriteData sprite, double rotation = 0, Vector2? scale = default(Vector2?))
            : base("Ball", position, size, rotation, scale)
        {
            Sprite = sprite;

            AddAllowedPublicProperties(new[]{
                nameof(Direction),
                nameof(MaxSpeed),
            });
        }
        #endregion

        #region Public Methods
        public IEnumerable<string> GetSpriteNames()
        {
            return Sprite.TextureNames;
        }
        #endregion
    }
}
